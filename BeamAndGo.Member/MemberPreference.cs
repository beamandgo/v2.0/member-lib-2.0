﻿using System.ComponentModel.DataAnnotations.Schema;
using BeamAndGo.Core.Data;

namespace BeamAndGo.Member
{
    public class MemberPreference : BaseEntityWithSid<int>
    {
        public virtual int MemberId { get; set; }

        [Column("LK_PreferenceTypeId")]
        public virtual int PreferenceTypeId { get; set; }
        public virtual string Value { get; set; }

        #region Related entities
        public virtual MemberPreferenceType PreferenceType { get; set; }
        #endregion
    }
}
