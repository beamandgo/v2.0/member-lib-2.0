﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeamAndGo.Member
{
    public interface IMemberAddressService
    {
        Task<MemberAddress> GetBySid(string sid);
        Task<MemberAddress> Create(string memberSid, string name, string street, string postalCode, string countryCode, string remarks = null);
        Task<MemberAddress> Update(string sid, string name, string street, string postalCode, string countryCode, string remarks = null);
        Task Delete(string sid);

        Task Verify(string sid, string meta);
        Task SetDefault(string sid);

        IAsyncEnumerable<MemberAddress> ListByMemberSid(string memberSid);
    }
}
