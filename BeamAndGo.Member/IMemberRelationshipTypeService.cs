﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeamAndGo.Member
{
    public interface IMemberRelationshipTypeService
    {
        Task<MemberRelationshipType> GetById(int id);
        Task<MemberRelationshipType> Create(string name, char? gender = null);
        Task<MemberRelationshipType> Update(int id, string name, char? gender = null);
        Task Delete(int id);
        Task<MemberRelationshipType> Enable(int id);
        Task<MemberRelationshipType> Disable(int id);
        Task<IEnumerable<MemberRelationshipType>> ListAll();
        Task<IEnumerable<MemberRelationshipType>> ListAllActiveSortedByName();
    }
}
