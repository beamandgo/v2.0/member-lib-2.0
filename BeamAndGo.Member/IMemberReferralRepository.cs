﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BeamAndGo.Core.Data;

namespace BeamAndGo.Member
{
    public interface IMemberReferralRepository : IRepository<MemberReferral, int>
    {
        Task<MemberReferral> GetBySid(string sid, bool includeRelated = true);
        IAsyncEnumerable<MemberReferral> ListByReferredMemberId(int memberId);
        IAsyncEnumerable<MemberReferral> ListByReferredMemberIdAndReferralType(int memberId, MemberReferralType referralType);
        IAsyncEnumerable<MemberReferral> ListByReferrerMemberId(int memberId);
        IAsyncEnumerable<MemberReferral> ListByReferrerMemberIdAndReferralType(int memberId, MemberReferralType referralType);
    }
}
