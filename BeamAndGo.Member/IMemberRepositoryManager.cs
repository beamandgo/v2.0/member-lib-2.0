﻿using BeamAndGo.Core.Data;

namespace BeamAndGo.Member
{
    public interface IMemberRepositoryManager : IRepositoryManager
    {
        IMemberRepository MemberRepository { get; set; }
        IMemberPhoneRepository MemberPhoneRepository { get; set; }
        IMemberEmailRepository MemberEmailRepository { get; set; }        
        IMemberAddressRepository MemberAddressRepository { get; set; }
        IMemberRelationshipRepository MemberRelationshipRepository { get; set; }
        IMemberPreferenceRepository MemberPreferenceRepository { get; set; }
        IMemberReferralRepository MemberReferralRepository { get; set; }

        IMemberLogRepository MemberLogRepository { get; set; }

        IMemberPreferenceTypeRepository MemberPreferenceTypeRepository { get; set; }
        IMemberRelationshipTypeRepository MemberRelationshipTypeRepository { get; set; }
    }
}
