﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Transactions;
using BeamAndGo.Core.Data;

namespace BeamAndGo.Member.Admin
{
    public class MemberAdminService : IMemberAdminService
    {
        private readonly IMemberAdminRepositoryManager _repositoryManager;
        private readonly IMemberAdminSearch _adminSearch;
        private readonly IMemberAdminLogService _adminLogService;
        private readonly IMemberService _memberService;

        public MemberAdminService(
            IMemberAdminRepositoryManager repositoryManager,
            IMemberAdminSearch adminSearch,
            IMemberAdminLogService adminLogService,
            IMemberService memberService)
        {
            _repositoryManager = repositoryManager;
            _adminLogService = adminLogService;
            _adminSearch = adminSearch;
            _memberService = memberService;
        }

        public virtual Task<int> Count() =>
            _repositoryManager.MemberAdminRepository.Count();

        public virtual async Task<MemberAdmin> GetBySid(string sid)
        {
            if (string.IsNullOrWhiteSpace(sid)) throw MemberAdminErrors.AdminSidRequired();
            var admin = await _repositoryManager.MemberAdminRepository.GetBySid(sid);
            if (admin == null) throw MemberAdminErrors.AdminNotFound(sid);
            return admin;
        }

        public virtual async Task<MemberAdmin> GetByMemberSid(string memberSid)
        {
            // GetBySid will validate Sid
            var member = await _memberService.GetBySid(memberSid);

            var admin = await _repositoryManager.MemberAdminRepository.GetByMemberId(member.Id);
            if (admin == null) throw MemberAdminErrors.AdminMemberSidNotFound(memberSid);
            return admin;
        }

        public virtual async Task<MemberAdmin> Create(string memberSid, MemberAdminAccessLevel accessLevel, DateTimeOffset? expiry)
        {
            if (!Enum.IsDefined(typeof(MemberAdminAccessLevel), accessLevel))
                throw MemberAdminErrors.AdminAccessLevelInvalid(accessLevel);

            // GetBySid will validate Sid
            var member = await _memberService.GetBySid(memberSid);

            if (null != await _repositoryManager.MemberAdminRepository.GetByMemberId(member.Id))
                throw MemberAdminErrors.AdminExists(memberSid);

            var memberAdmin = new MemberAdmin
            {
                MemberId = member.Id,
                AccessLevel = accessLevel,
                Expiry = expiry
            };

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                await _repositoryManager.MemberAdminRepository.Create(memberAdmin);
                await _repositoryManager.SaveChanges();
                await _adminLogService.Log(memberAdmin.Id, new object[] { "CreateAdmin", memberSid, accessLevel, expiry });
                scope.Complete();
            }

            return memberAdmin;
        }

        public virtual async Task<MemberAdmin> Update(string sid, MemberAdminAccessLevel accessLevel, DateTimeOffset? expiry)
        {
            if (!Enum.IsDefined(typeof(MemberAdminAccessLevel), accessLevel))
                throw MemberAdminErrors.AdminAccessLevelInvalid(accessLevel);

            // GetBySid will validate Sid
            var memberAdmin = await GetBySid(sid);
            memberAdmin.AccessLevel = accessLevel;
            memberAdmin.Expiry = expiry;

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                await _repositoryManager.MemberAdminRepository.Update(memberAdmin);
                await _repositoryManager.SaveChanges();
                await _adminLogService.Log(memberAdmin.Id, new object[] { "UpdateAdmin", sid, accessLevel, expiry });
                scope.Complete();
            }

            return memberAdmin;
        }

        public virtual async Task Delete(string sid)
        {
            // GetBySid will validate Sid
            var memberAdmin = await GetBySid(sid);

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                await _repositoryManager.MemberAdminRepository.Delete(memberAdmin);
                await _repositoryManager.SaveChanges();
                await _adminLogService.Log(memberAdmin.Id, new object[] { "DeleteAdmin", sid });
                scope.Complete();
            }
        }

        public virtual Task<IEnumerable<MemberAdmin>> ListAll(int start = 0, int count = 100) =>
            _repositoryManager.MemberAdminRepository.ListAll(start, count);

        public virtual async IAsyncEnumerable<MemberAdmin> Search(string search, int start = 0, int count = 100)
        {
            if (string.IsNullOrWhiteSpace(search)) throw MemberErrors.SearchKeywordRequired();
            await foreach (var a in _adminSearch.Search(search.Split(' ', StringSplitOptions.RemoveEmptyEntries), start, count))
                yield return a;
        }

        public virtual async Task<MemberAdmin> Enable(string sid)
        {
            // GetBySid will validate Sid
            var memberAdmin = await GetBySid(sid);
            memberAdmin.RecordStatus = RecordStatus.Active;

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                await _repositoryManager.MemberAdminRepository.Update(memberAdmin);
                await _repositoryManager.SaveChanges();
                await _adminLogService.Log(memberAdmin.Id, new object[] { "EnableAdmin", sid });
                scope.Complete();
            }

            return memberAdmin;
        }

        public virtual async Task<MemberAdmin> Disable(string sid)
        {
            // GetBySid will validate Sid
            var memberAdmin = await GetBySid(sid);
            memberAdmin.RecordStatus = RecordStatus.Inactive;

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                await _repositoryManager.MemberAdminRepository.Update(memberAdmin);
                await _repositoryManager.SaveChanges();
                await _adminLogService.Log(memberAdmin.Id, new object[] { "DisableAdmin", sid });
                scope.Complete();
            }

            return memberAdmin;
        }
    }
}
