﻿using BeamAndGo.Core.Data;

namespace BeamAndGo.Member.Admin
{
    public interface IMemberAdminRepositoryManager : IRepositoryManager
    {
        IMemberAdminRepository MemberAdminRepository { get; set; }
        IMemberAdminLogRepository MemberAdminLogRepository { get; set; }
    }
}
