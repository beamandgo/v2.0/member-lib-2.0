﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using BeamAndGo.Core.Data;

namespace BeamAndGo.Member.Admin
{
    public class MemberAdmin : BaseEntityWithSid<int>
    {
        public virtual int MemberId { get; set; }

        [Column("LK_MemberAdminAccessLevelId")]
        public virtual MemberAdminAccessLevel AccessLevel { get; set; }

        public virtual DateTimeOffset? Expiry { get; set; }

        #region Related entities
        public virtual Member Member { get; set; }
        #endregion
    }
}
