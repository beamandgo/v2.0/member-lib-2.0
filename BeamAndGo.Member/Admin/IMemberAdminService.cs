﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Admin
{
    public interface IMemberAdminService
    {
        Task<int> Count();
        Task<MemberAdmin> GetBySid(string sid);
        Task<MemberAdmin> GetByMemberSid(string memberSid);
        Task<MemberAdmin> Create(string memberSid, MemberAdminAccessLevel accessLevel, DateTimeOffset? expiry);
        Task<MemberAdmin> Update(string sid, MemberAdminAccessLevel accessLevel, DateTimeOffset? expiry);
        Task Delete(string sid);
        Task<IEnumerable<MemberAdmin>> ListAll(int start = 0, int count = 100);
        IAsyncEnumerable<MemberAdmin> Search(string search, int start = 0, int count = 100);
        Task<MemberAdmin> Enable(string sid);
        Task<MemberAdmin> Disable(string sid);
    }
}
