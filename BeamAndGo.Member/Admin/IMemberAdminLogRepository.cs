﻿using System.Collections.Generic;
using BeamAndGo.Core.Data;

namespace BeamAndGo.Member.Admin
{
    public interface IMemberAdminLogRepository : IRepository<MemberAdminLog, long>
    {
        IAsyncEnumerable<MemberAdminLog> ListByAdminId(int adminId, int start = 0, int count = 100);
    }
}
