﻿namespace BeamAndGo.Member.Admin
{
	public enum MemberAdminAccessLevel
	{
		ReadOnlyMasked = 1,
		ReadOnlyUnmasked = 2,
		FullAccess = 9,
		SuperAdmin = 100
	}
}
