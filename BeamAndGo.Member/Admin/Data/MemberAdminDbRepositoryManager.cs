﻿using BeamAndGo.Member.Data;

namespace BeamAndGo.Member.Admin.Data
{
    public class MemberAdminDbRepositoryManager : MemberDbRepositoryManager, IMemberAdminRepositoryManager
    {
        public IMemberAdminRepository MemberAdminRepository { get; set; }
        public IMemberAdminLogRepository MemberAdminLogRepository { get; set; }

        public MemberAdminDbRepositoryManager(MemberAdminDbContext context) : base(context)
        {
            MemberAdminRepository = new MemberAdminDbRepository(context);
            MemberAdminLogRepository = new MemberAdminLogDbRepository(context);
        }
    }
}
