﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using BeamAndGo.Core.Data.EntityFrameworkCore;

namespace BeamAndGo.Member.Admin.Data
{
    public class MemberAdminLogDbRepository : BaseDbRepository<MemberAdminLog, long>, IMemberAdminLogRepository
    {
        private readonly MemberAdminDbContext _context;

        public MemberAdminLogDbRepository(MemberAdminDbContext context) : base(context)
        {
            _context = context;
        }

        public IAsyncEnumerable<MemberAdminLog> ListByAdminId(int adminId, int start = 0, int count = 100)
        {
            var query = _context.MemberAdminLogs.AsNoTracking()
                .Where(l => l.AdminId == adminId)
                .OrderByDescending(l => l.Id)
                .Skip(start);

            if (count > 0) query = query.Take(count);

            return query.AsAsyncEnumerable();
        }
    }
}
