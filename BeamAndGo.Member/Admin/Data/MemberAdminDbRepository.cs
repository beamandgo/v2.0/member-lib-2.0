﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BeamAndGo.Core.Data.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace BeamAndGo.Member.Admin.Data
{
    public class MemberAdminDbRepository : BaseDbRepository<MemberAdmin, int>, IMemberAdminRepository
    {
        private readonly MemberAdminDbContext _context;

        public MemberAdminDbRepository(MemberAdminDbContext context) : base(context)
        {
            _context = context;
        }

        public Task<int> Count() =>
            _context.MemberAdmins.CountAsync();

        public Task<MemberAdmin> GetBySid(string sid) =>
            _context.MemberAdmins
                .Include(ma => ma.Member)
                .AsNoTracking()
                .Where(ma => ma.Sid == sid)
                .FirstOrDefaultAsync();

        public Task<MemberAdmin> GetByMemberId(int memberId) =>
            _context.MemberAdmins
                .AsNoTracking()
                .Where(ma => ma.MemberId == memberId)
                .FirstOrDefaultAsync();

        public override Task<IEnumerable<MemberAdmin>> ListAll(int start = 0, int count = 0)
        {
            var query = _context.MemberAdmins
                .Include(ma => ma.Member)
                .AsNoTracking()
                .AsEnumerable();

            if (start > 0) query = query.Skip(start);
            if (count > 0) query = query.Take(count);

            return Task.FromResult(query);
        }
    }
}
