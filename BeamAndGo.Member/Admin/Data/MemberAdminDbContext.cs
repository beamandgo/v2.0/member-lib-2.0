﻿using Microsoft.EntityFrameworkCore;
using BeamAndGo.Member.Data;

namespace BeamAndGo.Member.Admin.Data
{
    public class MemberAdminDbContext : MemberDbContext
    {
        public DbSet<MemberAdmin> MemberAdmins { get; set; }
        public DbSet<MemberAdminLog> MemberAdminLogs { get; set; }

        public MemberAdminDbContext(DbContextOptions<MemberDbContext> options) : base(options) { }
    }
}
