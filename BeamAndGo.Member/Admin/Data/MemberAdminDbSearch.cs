﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace BeamAndGo.Member.Admin.Data
{
    public class MemberAdminDbSearch : IMemberAdminSearch
    {
        private readonly MemberAdminDbContext _context;

        public MemberAdminDbSearch(MemberAdminDbContext context)
        {
            _context = context;
        }

        public IAsyncEnumerable<MemberAdmin> Search(string[] terms, int start = 0, int count = 100)
        {
            var query = _context.MemberAdmins
                .Include(ma => ma.Member)
                .Include(ma => ma.Member.Emails)
                .Include(ma => ma.Member.Phones)
                .AsNoTracking();

            foreach (var term in terms)
            {
                query = query.Where(ma =>
                    ma.Sid.Contains(term, StringComparison.OrdinalIgnoreCase) ||
                    ma.Member.Sid.Contains(term, StringComparison.OrdinalIgnoreCase) ||
                    ma.Member.FirstName.Contains(term, StringComparison.OrdinalIgnoreCase) ||
                    ma.Member.LastName.Contains(term, StringComparison.OrdinalIgnoreCase) ||
                    ma.Member.Emails.Any(me => me.Email.Contains(term, StringComparison.OrdinalIgnoreCase)) ||
                    ma.Member.Phones.Any(mp => mp.PhoneNumber.Contains(term, StringComparison.OrdinalIgnoreCase))
                );
            }

            query = query.Skip(start);

            if (count > 0) query = query.Take(count);

            return query.AsAsyncEnumerable();
        }
    }
}
