﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BeamAndGo.Core.Common;

namespace BeamAndGo.Member.Admin
{
    public class MemberAdminLogService : IMemberAdminLogService
    {
        private readonly IMemberAdminRepositoryManager _repositoryManager;

        public MemberAdminLogService(IMemberAdminRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
        }

        public async Task Log(int adminId, object[] meta, string notes = null)
        {
            var log = new MemberAdminLog
            {
                AdminId = adminId,
                Meta = meta.SerializeJson(),
                Notes = notes,
                CreateDate = DateTimeOffset.UtcNow
            };

            await _repositoryManager.MemberAdminLogRepository.Create(log);
            await _repositoryManager.SaveChanges();
        }

        public IAsyncEnumerable<MemberAdminLog> ListByAdminId(int adminId, int start = 0, int count = 100) =>
            _repositoryManager.MemberAdminLogRepository.ListByAdminId(adminId, start, count);
    }
}
