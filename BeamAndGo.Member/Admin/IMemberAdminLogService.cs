﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Admin
{
    public interface IMemberAdminLogService
    {
        Task Log(int adminId, object[] meta, string notes = null);
        IAsyncEnumerable<MemberAdminLog> ListByAdminId(int adminId, int start = 0, int count = 100);
    }
}
