﻿using System.Collections.Generic;

namespace BeamAndGo.Member.Admin
{
    public interface IMemberAdminSearch
    {
        IAsyncEnumerable<MemberAdmin> Search(string[] terms, int start = 0, int count = 100);
    }
}
