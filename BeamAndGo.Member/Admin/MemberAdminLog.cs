﻿using System;
using BeamAndGo.Core.Data;

namespace BeamAndGo.Member.Admin
{
    public class MemberAdminLog : IEntity<long>, IEntityWithAuditDates
    {
        public long Id { get; set; }

        public int AdminId { get; set; }

        /// <summary>
        /// System generated metadata
        /// </summary>
        public string Meta { get; set; }

        /// <summary>
        /// Notes for internal use
        /// </summary>
        public string Notes { get; set; }

        public DateTimeOffset CreateDate { get; set; }
        public DateTimeOffset? UpdateDate { get; set; }
    }
}

