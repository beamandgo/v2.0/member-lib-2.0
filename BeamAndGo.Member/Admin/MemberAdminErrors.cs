﻿using System;
namespace BeamAndGo.Member.Admin
{
    public static class MemberAdminErrors
    {
        private const string Prefix = "MEMBERADMIN";
        
        public static ArgumentException AdminSidRequired() =>
            new ArgumentException($"{Prefix}_SID_REQUIRED:Admin SID is required.");

        public static ArgumentException AdminAccessLevelInvalid(MemberAdminAccessLevel accessLevel) =>
            new ArgumentException($"{Prefix}_ACCESSLEVEL_INVALID:Admin access level ({accessLevel}) is invalid.");

        public static InvalidOperationException AdminExists(string memberSid) =>
            new InvalidOperationException($"{Prefix}_ADMIN_EXISTS:Member ({memberSid}) is already an admin, please update access level instead.");

        public static ArgumentException AdminMemberSidNotFound(string memberSid) =>
            new ArgumentException($"{Prefix}_ADMIN_NOTFOUND:Member ({memberSid}) is not an admin.");

        public static ArgumentException AdminNotFound(string adminSid) =>
            new ArgumentException($"{Prefix}_ADMIN_NOTFOUND:Admin ({adminSid}) is not found.");
    }
}
