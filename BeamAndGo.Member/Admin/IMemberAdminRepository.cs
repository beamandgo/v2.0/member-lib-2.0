﻿using System.Threading.Tasks;
using BeamAndGo.Core.Data;

namespace BeamAndGo.Member.Admin
{
    public interface IMemberAdminRepository : IRepository<MemberAdmin, int>
    {
        Task<int> Count();
        Task<MemberAdmin> GetBySid(string sid);
        Task<MemberAdmin> GetByMemberId(int memberId);
    }
}
