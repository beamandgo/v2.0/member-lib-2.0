﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeamAndGo.Member
{
    public interface IMemberPhoneService
    {
        Task<MemberPhone> GetBySid(string sid);
        Task<MemberPhone> Create(string memberSid, string callingCode, string phone, string identifier = null);
        Task<MemberPhone> Update(string sid, string callingCode, string phone, string identifier = null);
        Task Delete(string sid);

        Task Verify(string sid, string meta);
        Task SetDefault(string sid);

        IAsyncEnumerable<MemberPhone> ListByMemberSid(string memberSid);

        Task<bool> Exists(string callingCode, string phone);
    }
}
