﻿using System.Collections.Generic;
using BeamAndGo.Core.Data;

namespace BeamAndGo.Member
{
    public class Member : BaseEntityWithSid<int>
    {
        public virtual string FirstName { get; set; }
        public virtual string MiddleName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string DisplayName { get; set; }

        public virtual char? Gender { get; set; } // 'M' or 'F'

        public virtual int? BirthYear { get; set; }
        public virtual int? BirthMonth { get; set; }
        public virtual int? BirthDay { get; set; }

        public virtual string CountryCode { get; set; }

        public virtual string ReferralCode { get; set; }

        public virtual string Source { get; set; }
        public virtual string IPAddress { get; set; }

        #region Related entities
        public virtual IEnumerable<MemberPhone> Phones { get; set; }
        public virtual IEnumerable<MemberEmail> Emails { get; set; }
        public virtual IEnumerable<MemberAddress> Addresses { get; set; }
        public virtual IEnumerable<MemberRelationship> Relationships { get; set; }
        #endregion
    }
}
