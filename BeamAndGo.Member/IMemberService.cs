﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace BeamAndGo.Member
{
    public interface IMemberService
    {
        Task<int> Count();

        Task<Member> GetById(int id);
        Task<Member> GetBySid(string sid, bool includeRelated = false);
        Task<Member> GetByPhone(string phone);
        Task<Member> GetByPhone(string callingCode, string phone);
        Task<Member> GetByEmail(string email);

        Task<Member> Create(CreateMemberData create);
        Task<Member> Update(string sid, UpdateMemberData update);

        Task<Member> Disable(string memberSid);
        Task<Member> Enable(string memberSid);

        Task<IEnumerable<Member>> ListAll(int start = 0, int count = 100);
        IAsyncEnumerable<Member> Search(string search, int start = 0, int count = 100);
        Task<Member> SearchExact(string search);

        event EventHandler MemberCreated;
        event EventHandler MemberUpdated;
    }
}
