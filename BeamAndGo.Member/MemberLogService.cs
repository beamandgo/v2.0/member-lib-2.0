﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BeamAndGo.Core.Common;

namespace BeamAndGo.Member
{
    public class MemberLogService : IMemberLogService
    {
        private readonly IMemberRepositoryManager _repositoryManager;

        public MemberLogService(IMemberRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
        }

        public async Task Log(int memberId, object[] meta, string notes = null)
        {
            var log = new MemberLog
            {
                MemberId = memberId,
                Meta = meta.SerializeJson(),
                Notes = notes,
                CreateDate = DateTimeOffset.UtcNow
            };

            await _repositoryManager.MemberLogRepository.Create(log);
            await _repositoryManager.SaveChanges();
        }

        public IAsyncEnumerable<MemberLog> ListByMemberId(int memberId, int start = 0, int count = 100) =>
            _repositoryManager.MemberLogRepository.ListByMemberId(memberId, start, count);
    }
}
