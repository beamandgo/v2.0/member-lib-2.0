﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeamAndGo.Member
{
    public interface IMemberLogService
    {
        Task Log(int memberId, object[] meta, string notes = null);
        IAsyncEnumerable<MemberLog> ListByMemberId(int memberId, int start = 0, int count = 100);
    }
}
