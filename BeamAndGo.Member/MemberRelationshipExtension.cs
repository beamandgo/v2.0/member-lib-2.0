﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeamAndGo.Member
{
    public static class MemberRelationshipExtension
    {
        public static string FullName(this MemberRelationship relationship) =>
            string.IsNullOrWhiteSpace(relationship.MiddleName) ?
                $"{relationship.FirstName} {relationship.LastName}" : // Jonathan Chua
                $"{relationship.FirstName} {relationship.MiddleName[0]}. {relationship.LastName}"; // Jonathan E. Chua

        public static string AbbreviatedName(this MemberRelationship relationship) =>
            string.IsNullOrWhiteSpace(relationship.MiddleName) ?
                $"{relationship.FirstName[0]}{relationship.LastName[0]}" : // JC
                $"{relationship.FirstName[0]}{relationship.MiddleName[0]}{relationship.LastName[0]}"; // JEC
      
        public static DateTime? BirthDate(this MemberRelationship relationship)
        {
            if (relationship.BirthYear == null || relationship.BirthMonth == null || relationship.BirthDay == null)
                return null;

            return new DateTime(relationship.BirthYear.Value, relationship.BirthMonth.Value, relationship.BirthDay.Value);
        }

        public static int? AgeAt(this MemberRelationship relationship, DateTime date) =>
            AgeAt(relationship, date.Year, date.Month, date.Day);

        public static int? AgeAt(this MemberRelationship relationship, int year, int month, int day)
        {
            if (relationship.BirthYear == null || relationship.BirthMonth == null || relationship.BirthDay == null)
                return null;

            return MemberHelper.AgeAt(relationship.BirthYear.Value, relationship.BirthMonth.Value, relationship.BirthDay.Value,
                year, month, day);
        }
    }
}
