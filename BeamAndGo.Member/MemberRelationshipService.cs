﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.Extensions.Options;
using BeamAndGo.Core.Common;

namespace BeamAndGo.Member
{
    public class MemberRelationshipService : IMemberRelationshipService
    {
        private readonly MemberServiceOptions _options;
        private readonly IMemberRepositoryManager _repositoryManager;
        private readonly IMemberRelationshipTypeService _relationshipTypeService;
        private readonly IMemberService _memberService;
        private readonly IMemberLogService _logService;

        public MemberRelationshipService(
            IOptions<MemberServiceOptions> options,
            IMemberRepositoryManager repositoryManager,
            IMemberRelationshipTypeService relationshipTypeService,
            IMemberService memberService,
            IMemberLogService logService)
        {
            _options = options.Value;
            _repositoryManager = repositoryManager;
            _relationshipTypeService = relationshipTypeService;
            _memberService = memberService;
            _logService = logService;
        }

        public virtual async Task<MemberRelationship> GetBySid(string sid)
        {
            if (string.IsNullOrWhiteSpace(sid)) throw MemberErrors.RelationshipSidRequired();
            var relationship = await _repositoryManager.MemberRelationshipRepository.GetBySid(sid);
            if (relationship == null) throw MemberErrors.RelationshipNotFound(sid);
            return relationship;
        }

        public virtual async Task<MemberRelationship> Create(CreateMemberRelationshipData create)
        {
            if (string.IsNullOrWhiteSpace(create.FirstName)) throw MemberErrors.FirstNameRequired();
            if (string.IsNullOrWhiteSpace(create.LastName)) throw MemberErrors.LastNameRequired();
            if (string.IsNullOrWhiteSpace(create.CallingCode)) throw MemberErrors.CallingCodeRequired();
            if (string.IsNullOrWhiteSpace(create.PhoneNumber)) throw MemberErrors.PhoneRequired();

            if (!create.PhoneNumber.IsNumeric())
                throw MemberErrors.PhoneInvalid(create.PhoneNumber);

            if (!string.IsNullOrWhiteSpace(create.Email) && !create.Email.IsValidEmail()) 
                throw MemberErrors.EmailInvalid(create.Email);

            if (create.BirthYear != null &&
                create.BirthMonth != null &&
                create.BirthDay != null &&
                !_ValidateAge(create.BirthYear.Value, create.BirthMonth.Value, create.BirthDay.Value))
                throw MemberErrors.BirthDateInvalid(create.BirthYear.Value, create.BirthMonth.Value, create.BirthDay.Value);

            var member = await _memberService.GetBySid(create.MemberSid);
            var relationshipType = await _relationshipTypeService.GetById(create.RelationshipTypeId);

            var memberRelationship = new MemberRelationship
            {
                MemberId = member.Id,
                RelationshipTypeId = relationshipType.Id,

                FirstName = create.FirstName,
                MiddleName = create.MiddleName,
                LastName = create.LastName,

                Gender = create.Gender,

                BirthYear = create.BirthYear,
                BirthMonth = create.BirthMonth,
                BirthDay = create.BirthDay,

                CallingCode = create.CallingCode,
                PhoneNumber = create.PhoneNumber,

                Email = create.Email,

                Street = create.Street,
                PostalCode = create.PostalCode,
                CountryCode = create.CountryCode
            };

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                await _repositoryManager.MemberRelationshipRepository.Create(memberRelationship);
                await _repositoryManager.SaveChanges();
                await _logService.Log(member.Id, new object[] { "CreateRelationship", create });
                scope.Complete();
            }

            return memberRelationship;
        }

        public virtual async Task<MemberRelationship> Update(string sid, UpdateMemberRelationshipData update)
        {
            if (string.IsNullOrWhiteSpace(update.FirstName)) throw MemberErrors.FirstNameRequired();
            if (string.IsNullOrWhiteSpace(update.LastName)) throw MemberErrors.LastNameRequired();
            if (string.IsNullOrWhiteSpace(update.CallingCode)) throw MemberErrors.CallingCodeRequired();
            if (string.IsNullOrWhiteSpace(update.PhoneNumber)) throw MemberErrors.PhoneRequired();

            if (!update.PhoneNumber.IsNumeric())
                throw MemberErrors.PhoneInvalid(update.PhoneNumber);

            if (!string.IsNullOrWhiteSpace(update.Email) && !update.Email.IsValidEmail())
                throw MemberErrors.EmailInvalid(update.Email);

            if (update.BirthYear != null &&
                update.BirthMonth != null &&
                update.BirthDay != null &&
                !_ValidateAge(update.BirthYear.Value, update.BirthMonth.Value, update.BirthDay.Value))
                throw MemberErrors.BirthDateInvalid(update.BirthYear.Value, update.BirthMonth.Value, update.BirthDay.Value);

            var memberRelationship = await GetBySid(sid);
            var relationshipType = await _relationshipTypeService.GetById(update.RelationshipTypeId);

            memberRelationship.RelationshipTypeId = relationshipType.Id;

            memberRelationship.FirstName = update.FirstName;
            memberRelationship.MiddleName = update.MiddleName;
            memberRelationship.LastName = update.LastName;

            memberRelationship.Gender = update.Gender;

            memberRelationship.BirthYear = update.BirthYear;
            memberRelationship.BirthMonth = update.BirthMonth;
            memberRelationship.BirthDay = update.BirthDay;

            memberRelationship.CallingCode = update.CallingCode;
            memberRelationship.PhoneNumber = update.PhoneNumber;

            memberRelationship.Email = update.Email;

            memberRelationship.Street = update.Street;
            memberRelationship.PostalCode = update.PostalCode;
            memberRelationship.CountryCode = update.CountryCode;

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {                
                await _repositoryManager.MemberRelationshipRepository.Update(memberRelationship);
                await _repositoryManager.SaveChanges();
                await _logService.Log(memberRelationship.MemberId, new object[] { "UpdateRelationship", sid, update });
                scope.Complete();
            }

            return memberRelationship;
        }

        public virtual async Task Delete(string sid)
        {
            var memberRelationship = await GetBySid(sid);

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                await _repositoryManager.MemberRelationshipRepository.Delete(memberRelationship);
                await _repositoryManager.SaveChanges();
                await _logService.Log(memberRelationship.MemberId, new object[] { "DeleteRelationship", sid });
                scope.Complete();
            }
        }

        public virtual async IAsyncEnumerable<MemberRelationship> ListByMemberSid(string memberSid)
        {
            var member = await _memberService.GetBySid(memberSid);
            await foreach (var r in _repositoryManager.MemberRelationshipRepository.ListByMemberId(member.Id))
                yield return r;
        }

        private bool _ValidateAge(int birthYear, int birthMonth, int birthDay)
        {
            var now = DateTime.Now;
            var age = MemberHelper.AgeAt(
                birthYear, birthMonth, birthDay,
                now.Year, now.Month, now.Day);

            if (age < _options.MinimumAge || age > _options.MaximumAge) return false;

            return true;
        }
    }
}
