﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeamAndGo.Member
{
    public interface IMemberRelationshipService
    {
        Task<MemberRelationship> GetBySid(string sid);

        Task<MemberRelationship> Create(CreateMemberRelationshipData create);
        Task<MemberRelationship> Update(string sid, UpdateMemberRelationshipData update);

        Task Delete(string sid);

        IAsyncEnumerable<MemberRelationship> ListByMemberSid(string memberSid);
    }
}
