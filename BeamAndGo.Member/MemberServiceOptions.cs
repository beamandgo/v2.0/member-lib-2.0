﻿namespace BeamAndGo.Member
{
    public class MemberServiceOptions
    {
        public int MinimumAge { get; set; } = 0;
        public int MaximumAge { get; set; } = 100;
    }
}
