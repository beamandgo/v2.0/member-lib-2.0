﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BeamAndGo.Core.Data.EntityFrameworkCore;

namespace BeamAndGo.Member.Data
{
    public class MemberRelationshipTypeDbRepository : BaseDbRepository<MemberRelationshipType, int>, IMemberRelationshipTypeRepository
    {
        private readonly MemberDbContext _context;

        public MemberRelationshipTypeDbRepository(MemberDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<MemberRelationshipType>> ListAllActiveSortedByName() =>
            await _context.MemberRelationshipTypes.AsNoTracking()
               .Where(mrt => mrt.RecordStatus == Core.Data.RecordStatus.Active)
               .OrderBy(mrt => mrt.Name)
               .ToListAsync();
    }
}
