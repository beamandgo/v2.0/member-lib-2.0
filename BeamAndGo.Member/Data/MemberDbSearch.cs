﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace BeamAndGo.Member.Data
{
    public class MemberDbSearch : IMemberSearch
    {
        private readonly MemberDbContext _context;

        public MemberDbSearch(MemberDbContext context)
        {
            _context = context;
        }

        public IAsyncEnumerable<Member> Search(string[] terms, int start = 0, int count = 100)
        {
            var query = _context.Members
                .Include(m => m.Emails)
                .Include(m => m.Phones)
                .Include(m => m.Addresses)
                .AsNoTracking();

            foreach (var term in terms)
            {
                query = query.Where(m =>
                    m.Sid.Contains(term, StringComparison.OrdinalIgnoreCase) ||
                    m.FirstName.Contains(term, StringComparison.OrdinalIgnoreCase) ||
                    m.LastName.Contains(term, StringComparison.OrdinalIgnoreCase) ||
                    m.Emails.Any(me => me.Email.Contains(term, StringComparison.OrdinalIgnoreCase)) ||
                    m.Phones.Any(mp => mp.PhoneNumber.Contains(term, StringComparison.OrdinalIgnoreCase))
                );
            }

            query = query.Skip(start);

            if (count > 0) query = query.Take(count);

            return query.AsAsyncEnumerable();
        }
    }
}
