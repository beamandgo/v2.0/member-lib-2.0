﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BeamAndGo.Core.Data.EntityFrameworkCore;

namespace BeamAndGo.Member.Data
{
    public class MemberPreferenceTypeDbRepository : BaseDbRepository<MemberPreferenceType, int>, IMemberPreferenceTypeRepository
    {
        private readonly MemberDbContext _context;

        public MemberPreferenceTypeDbRepository(MemberDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<MemberPreferenceType>> ListAllActiveSorted() =>
            await _context.MemberPreferenceTypes.AsNoTracking()
                .Where(mpt => mpt.RecordStatus == Core.Data.RecordStatus.Active)
                .OrderBy(mpt => mpt.SortOrder)
                .ToListAsync();
    }
}
