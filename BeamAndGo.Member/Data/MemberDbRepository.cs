﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BeamAndGo.Core.Data.EntityFrameworkCore;
using System.Collections.Generic;

namespace BeamAndGo.Member.Data
{
    public class MemberDbRepository : BaseDbRepository<Member, int>, IMemberRepository
    {
        private readonly MemberDbContext _context;

        public MemberDbRepository(MemberDbContext context) : base(context)
        {
            _context = context;
        }

        public Task<int> Count() =>
            _context.Members.CountAsync();

        public Task<Member> GetByPhone(string phone) =>
            _context.Members.AsNoTracking()
                .Where(m => m.Phones.Any(mp => mp.PhoneNumber == phone))
                .FirstOrDefaultAsync();

        public Task<Member> GetByPhone(string callingCode, string phone) =>
            _context.Members.AsNoTracking()
                .Where(m => m.Phones.Any(mp =>
                    mp.CallingCode == callingCode &&
                    mp.PhoneNumber == phone))
                .FirstOrDefaultAsync();

        public Task<Member> GetByEmail(string email) =>
            _context.Members.AsNoTracking()
                .Where(m => m.Emails.Any(me => me.Email == email))
                .FirstOrDefaultAsync();

        public Task<Member> GetBySid(string sid, bool includeRelated)
        {
            if (includeRelated)
            {
                return _context.Members
                    .Include(m => m.Phones)
                    .Include(m => m.Emails)
                    .Include(m => m.Addresses)
                    .Include(m => m.Relationships)
                    .AsNoTracking()
                    .Where(m => m.Sid == sid)
                    .FirstOrDefaultAsync();
            }

            return _context.Members.AsNoTracking()
                .Where(m => m.Sid == sid)
                .FirstOrDefaultAsync();
        }

        public Task<Member> GetByReferralCode(string referralCode) =>
            _context.Members.AsNoTracking()
                .Where(m => m.ReferralCode == referralCode)
                .FirstOrDefaultAsync();

        public override Task<IEnumerable<Member>> ListAll(int start = 0, int count = 0)
        {
            var query = _context.Members
                .Include(m => m.Phones)
                .Include(m => m.Emails)
                .Include(m => m.Addresses)
                .AsNoTracking()
                .AsEnumerable();

            if (start > 0) query = query.Skip(start);
            if (count > 0) query = query.Take(count);

            return Task.FromResult(query);
        }
    }
}
