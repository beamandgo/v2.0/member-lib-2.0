﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BeamAndGo.Core.Data.EntityFrameworkCore;

namespace BeamAndGo.Member.Data
{
    public class MemberEmailDbRepository : BaseDbRepository<MemberEmail, int>, IMemberEmailRepository
    {
        private readonly MemberDbContext _context;

        public MemberEmailDbRepository(MemberDbContext context) : base(context)
        {
            _context = context;
        }

        public Task<MemberEmail> GetBySid(string sid) =>
            _context.MemberEmails.AsNoTracking()
                .Where(me => me.Sid == sid)
                    .FirstOrDefaultAsync();

        public Task<MemberEmail> GetByEmail(string email) =>
            _context.MemberEmails.AsNoTracking()
                .Where(me => me.Email == email)
                .FirstOrDefaultAsync();

        public Task<MemberEmail> GetDefaultByMemberId(int memberId) =>
            _context.MemberEmails.AsNoTracking()
                .Where(me =>
                    me.MemberId == memberId &&
                    me.IsDefault)
                .FirstOrDefaultAsync();

        public IAsyncEnumerable<MemberEmail> ListByMemberId(int memberId) =>
            _context.MemberEmails.AsNoTracking()
                .Where(me =>
                    me.MemberId == memberId &&
                    me.RecordStatus == Core.Data.RecordStatus.Active)
                .OrderByDescending(me => me.IsDefault)
                .ThenBy(me => me.Email)
                .AsAsyncEnumerable();
    }
}
