﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BeamAndGo.Core.Data.EntityFrameworkCore;

namespace BeamAndGo.Member.Data
{
    public class MemberRelationshipDbRepository : BaseDbRepository<MemberRelationship, int>, IMemberRelationshipRepository
    {
        private readonly MemberDbContext _context;

        public MemberRelationshipDbRepository(MemberDbContext context) : base(context)
        {
            _context = context;
        }
        public Task<MemberRelationship> GetBySid(string sid) =>
            _context.MemberRelationships
                .Include(mr => mr.RelationshipType)
                .AsNoTracking()
                .Where(mr => mr.Sid == sid)
                .FirstOrDefaultAsync();

        public IAsyncEnumerable<MemberRelationship> ListByMemberId(int memberId) =>
            _context.MemberRelationships
                .Include(mr => mr.RelationshipType)
                .AsNoTracking()
                .Where(mr => mr.MemberId == memberId)
                .OrderBy(mr => mr.FirstName)
                .ThenBy(mr => mr.LastName)
                .AsAsyncEnumerable();

        public IAsyncEnumerable<MemberRelationship> ListByRelationshipTypeId(int relationshipTypeId) =>
            _context.MemberRelationships.AsNoTracking()
                .Where(mr => mr.RelationshipTypeId == relationshipTypeId)
                .AsAsyncEnumerable();
    }
}
