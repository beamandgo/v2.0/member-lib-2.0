﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BeamAndGo.Core.Data.EntityFrameworkCore;

namespace BeamAndGo.Member.Data
{
    public class MemberPhoneDbRepository : BaseDbRepository<MemberPhone, int>, IMemberPhoneRepository
    {
        private readonly MemberDbContext _context;
        public MemberPhoneDbRepository(MemberDbContext context) : base(context)
        {
            _context = context;
        }

        public Task<MemberPhone> GetBySid(string sid) =>
            _context.MemberPhones.AsNoTracking()
                .Where(mp => mp.Sid == sid)
                .FirstOrDefaultAsync();

        public Task<MemberPhone> GetByCallingCodeAndPhone(string callingCode, string phone) =>
            _context.MemberPhones.AsNoTracking()
                .Where(mp =>
                    mp.CallingCode == callingCode &&
                    mp.PhoneNumber == phone)
            .FirstOrDefaultAsync();

        public Task<MemberPhone> GetDefaultByMemberId(int memberId) =>
            _context.MemberPhones.AsNoTracking()
                .Where(mp =>
                    mp.MemberId == memberId &&
                    mp.IsDefault)
                .FirstOrDefaultAsync();

        public IAsyncEnumerable<MemberPhone> ListByMemberId(int memberId) =>
            _context.MemberPhones.AsNoTracking()
                .Where(mp =>
                    mp.MemberId == memberId &&
                    mp.RecordStatus == Core.Data.RecordStatus.Active)
                .OrderByDescending(mp => mp.IsDefault)
                .ThenBy(mp => mp.CallingCode)
                .ThenBy(mp => mp.PhoneNumber)
                .AsAsyncEnumerable();
    }
}
