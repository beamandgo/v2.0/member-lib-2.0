﻿using BeamAndGo.Core.Data.EntityFrameworkCore;

namespace BeamAndGo.Member.Data
{
    public class MemberDbRepositoryManager : BaseDbRepositoryManager, IMemberRepositoryManager
    {
        public IMemberRepository MemberRepository { get; set; }
        public IMemberPhoneRepository MemberPhoneRepository { get; set; }
        public IMemberEmailRepository MemberEmailRepository { get; set; }
        public IMemberAddressRepository MemberAddressRepository { get; set; }
        public IMemberRelationshipRepository MemberRelationshipRepository { get; set; }
        public IMemberPreferenceRepository MemberPreferenceRepository { get; set; }
        public IMemberReferralRepository MemberReferralRepository { get; set; }
        public IMemberLogRepository MemberLogRepository { get; set; }

        public IMemberPreferenceTypeRepository MemberPreferenceTypeRepository { get; set; }
        public IMemberRelationshipTypeRepository MemberRelationshipTypeRepository { get; set; }

        public MemberDbRepositoryManager(MemberDbContext context) : base(context)
        {
            MemberRepository = new MemberDbRepository(context);
            MemberPhoneRepository = new MemberPhoneDbRepository(context);
            MemberEmailRepository = new MemberEmailDbRepository(context);            
            MemberAddressRepository = new MemberAddressDbRepository(context);
            MemberRelationshipRepository = new MemberRelationshipDbRepository(context);
            MemberPreferenceRepository = new MemberPreferenceDbRepository(context);
            MemberReferralRepository = new MemberReferralDbRepository(context);

            MemberLogRepository = new MemberLogDbRepository(context);

            MemberPreferenceTypeRepository = new MemberPreferenceTypeDbRepository(context);
            MemberRelationshipTypeRepository = new MemberRelationshipTypeDbRepository(context);
        }
    }
}
