﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using BeamAndGo.Core.Data.EntityFrameworkCore;

namespace BeamAndGo.Member.Data
{
    public class MemberLogDbRepository : BaseDbRepository<MemberLog, long>, IMemberLogRepository
    {
        private readonly MemberDbContext _context;

        public MemberLogDbRepository(MemberDbContext context) : base(context)
        {
            _context = context;
        }

        public IAsyncEnumerable<MemberLog> ListByMemberId(int memberId, int start = 0, int count = 100)
        {
            var query = _context.MemberLogs.AsNoTracking()
                .Where(l => l.MemberId == memberId)
                .OrderByDescending(l => l.Id)
                .Skip(start);

            if (count > 0) query = query.Take(count);

            return query.AsAsyncEnumerable();
        }
    }
}
