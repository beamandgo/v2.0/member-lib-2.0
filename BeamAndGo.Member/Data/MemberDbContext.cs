﻿using Microsoft.EntityFrameworkCore;
using BeamAndGo.Core.Data.EntityFrameworkCore;

namespace BeamAndGo.Member.Data
{
    public class MemberDbContext: BaseDbContext
    {
        public DbSet<Member> Members { get; set; }
        public DbSet<MemberPhone> MemberPhones { get; set; }
        public DbSet<MemberEmail> MemberEmails { get; set; }        
        public DbSet<MemberAddress> MemberAddresses { get; set; }
        public DbSet<MemberRelationship> MemberRelationships { get; set; }
        public DbSet<MemberPreference> MemberPreferences { get; set; }
        public DbSet<MemberReferral> MemberReferrals { get; set; }

        public DbSet<MemberLog> MemberLogs { get; set; }

        public DbSet<MemberPreferenceType> MemberPreferenceTypes { get; set; }
        public DbSet<MemberRelationshipType> MemberRelationshipTypes { get; set; }

        public MemberDbContext(DbContextOptions<MemberDbContext> options) : base(options) { }

    }
}
