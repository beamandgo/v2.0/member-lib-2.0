﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BeamAndGo.Core.Data.EntityFrameworkCore;

namespace BeamAndGo.Member.Data
{
    public class MemberAddressDbRepository: BaseDbRepository<MemberAddress, int>, IMemberAddressRepository
    {
        private readonly MemberDbContext _context;

        public MemberAddressDbRepository(MemberDbContext context) : base(context)
        {
            _context = context;
        }
        public Task<MemberAddress> GetBySid(string sid) =>
            _context.MemberAddresses.AsNoTracking()
                .Where(ma => ma.Sid == sid)
                .FirstOrDefaultAsync();

        public Task<MemberAddress> GetDefaultByMemberId(int memberId) =>
            _context.MemberAddresses.AsNoTracking()
                .Where(ma =>
                    ma.MemberId == memberId &&
                    ma.IsDefault)
                .FirstOrDefaultAsync();

        public IAsyncEnumerable<MemberAddress> ListByMemberId(int memberId) =>
            _context.MemberAddresses.AsNoTracking()
                .Where(ma =>
                    ma.MemberId == memberId &&
                    ma.RecordStatus == Core.Data.RecordStatus.Active)
                .OrderByDescending(ma => ma.IsDefault)
                .ThenBy(ma => ma.Id)
                .AsAsyncEnumerable();
    }
}
