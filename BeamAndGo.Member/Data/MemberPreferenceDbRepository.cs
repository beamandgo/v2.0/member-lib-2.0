﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BeamAndGo.Core.Data.EntityFrameworkCore;

namespace BeamAndGo.Member.Data
{
    public class MemberPreferenceDbRepository : BaseDbRepository<MemberPreference, int>, IMemberPreferenceRepository
    {
        private readonly MemberDbContext _context;
        public MemberPreferenceDbRepository(MemberDbContext context) : base(context)
        {
            _context = context;
        }

        public Task<MemberPreference> GetBySid(string sid) =>
            _context.MemberPreferences
                .Include(mp => mp.PreferenceType)
                .AsNoTracking()
                .Where(mp => mp.Sid == sid)
                .FirstOrDefaultAsync();

        public Task<MemberPreference> GetByMemberIdAndPreferenceTypeId(int memberId, int preferenceTypeId) =>
            _context.MemberPreferences
                .Include(mp => mp.PreferenceType)
                .AsNoTracking()
                .Where(mp =>
                    mp.MemberId == memberId &&
                    mp.PreferenceTypeId == preferenceTypeId &&
                    mp.RecordStatus == Core.Data.RecordStatus.Active)
                .FirstOrDefaultAsync();

        public IAsyncEnumerable<MemberPreference> ListByMemberId(int memberId) =>
            _context.MemberPreferences
                .Include(mp => mp.PreferenceType)
                .AsNoTracking()
                .Where(mp => mp.MemberId == memberId)
                .AsAsyncEnumerable();

        public IAsyncEnumerable<MemberPreference> ListByPreferenceTypeId(int preferenceTypeId) =>
            _context.MemberPreferences.AsNoTracking()
                .Where(mp => mp.PreferenceTypeId == preferenceTypeId)
                .AsAsyncEnumerable();
    }
}
