﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BeamAndGo.Core.Data.EntityFrameworkCore;

namespace BeamAndGo.Member.Data
{
    public class MemberReferralDbRepository : BaseDbRepository<MemberReferral, int>, IMemberReferralRepository
    {
        private readonly MemberDbContext _context;

        public MemberReferralDbRepository(MemberDbContext context) : base(context)
        {
            _context = context;
        }

        public Task<MemberReferral> GetBySid(string sid, bool includeRelated = true) =>
            _context.MemberReferrals.AsNoTracking()
                .Where(mr => mr.Sid == sid)
                .FirstOrDefaultAsync();


        public IAsyncEnumerable<MemberReferral> ListByReferredMemberId(int memberId) =>
            _context.MemberReferrals
                .Include(mr => mr.Referrer)
                .Include(mr => mr.Referred)
                .AsNoTracking()
                .Where(mr => mr.ReferredMemberId == memberId)
                .AsAsyncEnumerable();

        public IAsyncEnumerable<MemberReferral> ListByReferredMemberIdAndReferralType(int memberId, MemberReferralType referralType) =>
            _context.MemberReferrals
                .Include(mr => mr.Referrer)
                .Include(mr => mr.Referred)
                .AsNoTracking()
                .Where(mr =>
                    mr.ReferredMemberId == memberId &&
                    mr.ReferralType == referralType)
                .AsAsyncEnumerable();

        public IAsyncEnumerable<MemberReferral> ListByReferrerMemberId(int memberId) =>
            _context.MemberReferrals
                .Include(mr => mr.Referrer)
                .Include(mr => mr.Referred)
                .AsNoTracking()
                .Where(mr => mr.ReferrerMemberId == memberId)
                .AsAsyncEnumerable();

        public IAsyncEnumerable<MemberReferral> ListByReferrerMemberIdAndReferralType(int memberId, MemberReferralType referralType) =>
            _context.MemberReferrals
                .Include(mr => mr.Referrer)
                .Include(mr => mr.Referred)
                .AsNoTracking()
                .Where(mr =>
                    mr.ReferrerMemberId == memberId &&
                    mr.ReferralType == referralType)
                .AsAsyncEnumerable();
    }
}
