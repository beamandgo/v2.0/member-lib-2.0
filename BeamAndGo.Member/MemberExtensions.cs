﻿using System;

namespace BeamAndGo.Member
{
    public static class MemberExtensions
    {
        public static string FullName(this Member member) =>
            string.IsNullOrWhiteSpace(member.MiddleName) ?
                $"{member.FirstName} {member.LastName}" : // Jonathan Chua
                $"{member.FirstName} {member.MiddleName[0]}. {member.LastName}"; // Jonathan E. Chua

        public static string AbbreviatedName(this Member member) =>
            string.IsNullOrWhiteSpace(member.MiddleName) ?
                $"{member.FirstName[0]}{member.LastName[0]}" : // JC
                $"{member.FirstName[0]}{member.MiddleName[0]}{member.LastName[0]}"; // JEC

        public static string DisplayNameOrDefault(this Member member) =>
            string.IsNullOrWhiteSpace(member.DisplayName) ?
                FullName(member) :
                member.DisplayName;

        public static DateTime? BirthDate(this Member member)
        {
            if (member.BirthYear == null || member.BirthMonth == null || member.BirthDay == null)
                return null;

            return new DateTime(member.BirthYear.Value, member.BirthMonth.Value, member.BirthDay.Value);
        }

        public static int? AgeAt(this Member member, DateTime date) =>
            AgeAt(member, date.Year, date.Month, date.Day);

        public static int? AgeAt(this Member member, int year, int month, int day)
        {
            if (member.BirthYear == null || member.BirthMonth == null || member.BirthDay == null)
                return null;

            return MemberHelper.AgeAt(member.BirthYear.Value, member.BirthMonth.Value, member.BirthDay.Value,
                year, month, day);
        }
    }
}
