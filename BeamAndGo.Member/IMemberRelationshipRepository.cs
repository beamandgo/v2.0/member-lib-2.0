﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BeamAndGo.Core.Data;

namespace BeamAndGo.Member
{
    public interface IMemberRelationshipRepository : IRepository<MemberRelationship, int>
    {
        Task<MemberRelationship> GetBySid(string sid);
        IAsyncEnumerable<MemberRelationship> ListByMemberId(int memberId);
        IAsyncEnumerable<MemberRelationship> ListByRelationshipTypeId(int relationshipTypeId);
    }
}
