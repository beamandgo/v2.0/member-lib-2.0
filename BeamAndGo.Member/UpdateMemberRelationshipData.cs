﻿namespace BeamAndGo.Member
{
    public class UpdateMemberRelationshipData
    {
        public virtual int RelationshipTypeId { get; set; }

        public virtual string FirstName { get; set; }
        public virtual string MiddleName { get; set; }
        public virtual string LastName { get; set; }

        public virtual char? Gender { get; set; }

        public virtual int? BirthYear { get; set; }
        public virtual int? BirthMonth { get; set; }
        public virtual int? BirthDay { get; set; }

        public virtual string CallingCode { get; set; }
        public virtual string PhoneNumber { get; set; }

        public virtual string Email { get; set; }

        public virtual string Street { get; set; }
        public virtual string PostalCode { get; set; }
        public virtual string CountryCode { get; set; }
    }
}
