﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BeamAndGo.Core.Data;

namespace BeamAndGo.Member
{
    public interface IMemberRelationshipTypeRepository : IRepository<MemberRelationshipType, int>
    {
        Task<IEnumerable<MemberRelationshipType>> ListAllActiveSortedByName();
    }
}
