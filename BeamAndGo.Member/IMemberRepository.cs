﻿using System.Threading.Tasks;
using BeamAndGo.Core.Data;

namespace BeamAndGo.Member
{
    public interface IMemberRepository : IRepository<Member, int>
    {
        Task<int> Count();
        Task<Member> GetBySid(string sid, bool includeRelated);
        Task<Member> GetByPhone(string phone);
        Task<Member> GetByPhone(string callingCode, string phone);
        Task<Member> GetByEmail(string email);
        Task<Member> GetByReferralCode(string referralCode);
    }
}