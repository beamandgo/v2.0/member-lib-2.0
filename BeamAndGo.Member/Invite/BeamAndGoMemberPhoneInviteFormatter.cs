﻿using System;

namespace BeamAndGo.Member.Invite
{
    public class BeamAndGoMemberPhoneInviteFormatter : IMemberPhoneInviteFormatter
    {
        public string Format(string firstName, string lastName, string referralLink)
        {
            return String.Join(Environment.NewLine, 
                $"{firstName} {lastName} has invited you to join Beam&Go.",
                $"Join now at {referralLink}");
        }
    }
}
