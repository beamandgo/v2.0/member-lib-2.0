﻿namespace BeamAndGo.Member.Invite
{
    public interface IMemberPhoneInviteFormatter
    {
        string Format(string firstName, string lastName, string referralLink);
    }
}
