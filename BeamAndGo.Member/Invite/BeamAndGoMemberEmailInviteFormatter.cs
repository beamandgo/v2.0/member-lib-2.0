﻿using System;

namespace BeamAndGo.Member.Invite
{
    public class BeamAndGoMemberEmailInviteFormatter : IMemberEmailInviteFormatter
    {
        public (string subject, string body) Format(string firstName, string lastName, string referralLink)
        {
            string subject = $"{firstName} {lastName} has invited you to join Beam&Go.";

            string body = String.Join(Environment.NewLine,
                $"{firstName} {lastName} has invited you to join Beam&Go.",
                $"Join now at {referralLink}");

            return (subject, body);
        }
    }
}
