﻿using System;
using System.Threading.Tasks;
using BeamAndGo.Member.Messaging;
using BeamAndGo.Core.Common;

namespace BeamAndGo.Member.Invite
{
    public class MemberEmailInviteService : IMemberEmailInviteService
    {
        private readonly IMemberEmailInviteFormatter _formatter;
        private readonly IMemberMailMessagingProvider _messagingProvider;
        private readonly IMemberEmailService _emailService;
        private readonly IMemberService _memberService;

        public MemberEmailInviteService(
            IMemberEmailInviteFormatter formatter,
            IMemberMailMessagingProvider messagingProvider,
            IMemberEmailService emailService,
            IMemberService memberService
            )
        {
            _formatter = formatter;
            _messagingProvider = messagingProvider;
            _emailService = emailService;
            _memberService = memberService;
        }

        public virtual async Task Send(string fromMemberSid, string toEmail, string referralLink)
        {
            if (string.IsNullOrWhiteSpace(toEmail)) throw MemberErrors.EmailRequired();
            if (string.IsNullOrWhiteSpace(referralLink)) throw MemberInviteErrors.ReferralLinkRequired();
            if (!toEmail.IsValidEmail()) throw MemberErrors.EmailInvalid(toEmail);

            if (await _emailService.Exists(toEmail)) throw MemberInviteErrors.EmailExists(toEmail);

            // GetBySid validates Sid
            var member = await _memberService.GetBySid(fromMemberSid);

            (string subject, string body) = _formatter.Format(member.FirstName, member.LastName, referralLink);
            await _messagingProvider.Send(toEmail, subject, body, true);
        }
    }
}
