﻿using System.Threading.Tasks;

namespace BeamAndGo.Member.Invite
{
    public interface IMemberPhoneInviteService
    {
        Task Send(string fromMemberSid, string toCallingCode, string toPhone, string referralLink);
    }
}
