﻿using System;
namespace BeamAndGo.Member.Invite
{
    public static class MemberInviteErrors
    {
        private const string Prefix = "MEMBERINVITE";
        
        public static ArgumentException ReferralLinkRequired() =>
          new ArgumentException($"{Prefix}_REFERRALLINK_REQUIRED: Referral link is required.");

        public static InvalidOperationException EmailExists(string email) =>
            new InvalidOperationException($"{Prefix}_EMAIL_EXISTS: Email ({email}) is already registered.");

        public static InvalidOperationException PhoneExists(string callingCode, string phone) =>
            new InvalidOperationException($"{Prefix}_PHONE_EXISTS: Phone ({callingCode} {phone}) is already registered.");
    }
}
