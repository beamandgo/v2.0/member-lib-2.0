﻿using System.Threading.Tasks;

namespace BeamAndGo.Member.Invite
{
    public interface IMemberEmailInviteService
    {
        Task Send(string fromMemberSid, string toEmail, string referralLink);
    }
}
