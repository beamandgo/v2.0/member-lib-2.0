﻿using System;
using System.Threading.Tasks;
using BeamAndGo.Member.Messaging;
using BeamAndGo.Core.Common;

namespace BeamAndGo.Member.Invite
{
    public class MemberPhoneInviteService : IMemberPhoneInviteService
    {
        private readonly IMemberPhoneInviteFormatter _formatter;
        private readonly IMemberTextMessagingProvider _messagingProvider;
        private readonly IMemberPhoneService _phoneService;
        private readonly IMemberService _memberService;

        public MemberPhoneInviteService(
            IMemberPhoneInviteFormatter formatter,
            IMemberTextMessagingProvider messagingProvider,
            IMemberPhoneService phoneService,
            IMemberService memberService
            )
        {
            _formatter = formatter;
            _messagingProvider = messagingProvider;
            _phoneService = phoneService;
            _memberService = memberService;
        }

        public virtual async Task Send(string fromMemberSid, string toCallingCode, string toPhone, string referralLink)
        {
            if (string.IsNullOrWhiteSpace(toCallingCode)) throw MemberErrors.CallingCodeRequired();
            if (string.IsNullOrWhiteSpace(toPhone)) throw MemberErrors.PhoneRequired();
            if (string.IsNullOrWhiteSpace(referralLink)) throw MemberInviteErrors.ReferralLinkRequired();
            if (!toCallingCode.IsNumeric()) throw MemberErrors.CallingCodeInvalid(toPhone);
            if (!toPhone.IsNumeric()) throw MemberErrors.PhoneInvalid(toPhone);

            if (await _phoneService.Exists(toCallingCode, toPhone)) throw MemberInviteErrors.PhoneExists(toCallingCode, toPhone);

            // GetBySid validates Sid
            var member = await _memberService.GetBySid(fromMemberSid);

            string message = _formatter.Format(member.FirstName, member.LastName, referralLink);
            await _messagingProvider.Send(toCallingCode, toPhone, message);
        }
    }
}
