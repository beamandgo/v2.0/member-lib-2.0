﻿namespace BeamAndGo.Member.Invite
{
    public interface IMemberEmailInviteFormatter
    {
        (string subject, string body) Format(string firstName, string lastName, string referralLink);
    }
}
