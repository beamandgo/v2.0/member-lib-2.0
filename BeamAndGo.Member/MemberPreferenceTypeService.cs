﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BeamAndGo.Core.Data;
using System.Linq;
using System.Transactions;

namespace BeamAndGo.Member
{
    public class MemberPreferenceTypeService : IMemberPreferenceTypeService
    {
        private readonly IMemberRepositoryManager _repositoryManager;

        public MemberPreferenceTypeService(IMemberRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
        }

        public virtual async Task<MemberPreferenceType> GetById(int preferenceTypeId)
        {
            var preferenceType = await _repositoryManager.MemberPreferenceTypeRepository.GetById(preferenceTypeId);
            if (preferenceType == null) throw MemberErrors.PreferenceTypeNotFound(preferenceTypeId);
            return preferenceType;
        }

        public virtual async Task<MemberPreferenceType> Create(string name, string description, string defaultValue)
        {
            if (string.IsNullOrWhiteSpace(name)) throw MemberErrors.PreferenceTypeNameRequired();
            if (string.IsNullOrWhiteSpace(description)) throw MemberErrors.PreferenceTypeDescriptionRequired();
            if (string.IsNullOrWhiteSpace(defaultValue)) throw MemberErrors.PreferenceTypeDefaultValueRequired();

            var preferenceType = new MemberPreferenceType
            {
                Name = name,
                Description = description,
                DefaultValue = defaultValue
            };

            await _repositoryManager.MemberPreferenceTypeRepository.Create(preferenceType);
            await _repositoryManager.SaveChanges();

            return preferenceType;
        }

        public virtual async Task<MemberPreferenceType> Update(int id, string name, string description, string defaultValue)
        {
            if (string.IsNullOrWhiteSpace(name)) throw MemberErrors.PreferenceTypeNameRequired();
            if (string.IsNullOrWhiteSpace(description)) throw MemberErrors.PreferenceTypeDescriptionRequired();
            if (string.IsNullOrWhiteSpace(defaultValue)) throw MemberErrors.PreferenceTypeDefaultValueRequired();

            var preferenceType = await GetById(id);
            preferenceType.Name = name;
            preferenceType.Description = description;
            preferenceType.DefaultValue = defaultValue;

            await _repositoryManager.MemberPreferenceTypeRepository.Update(preferenceType);
            await _repositoryManager.SaveChanges();

            return preferenceType;
        }

        public virtual async Task Delete(int id)
        {
            var preferenceType = await GetById(id);

            var enumerator = _repositoryManager.MemberPreferenceRepository.ListByPreferenceTypeId(id).GetAsyncEnumerator();

            if (await enumerator.MoveNextAsync())
            {
                await enumerator.DisposeAsync();
                throw MemberErrors.DeletePreferenceTypeNotAllowed(preferenceType.Name);
            }

            await enumerator.DisposeAsync();

            await _repositoryManager.MemberPreferenceTypeRepository.Delete(preferenceType);
            await _repositoryManager.SaveChanges();                           
        }

        public virtual async Task<MemberPreferenceType> Enable(int id)
        {
            var preferenceType = await GetById(id);
            preferenceType.RecordStatus = RecordStatus.Active;

            await _repositoryManager.MemberPreferenceTypeRepository.Update(preferenceType);
            await _repositoryManager.SaveChanges();

            return preferenceType;
        }

        public virtual async Task<MemberPreferenceType> Disable(int id)
        {
            var preferenceType = await GetById(id);

            preferenceType.RecordStatus = RecordStatus.Inactive;

            await _repositoryManager.MemberPreferenceTypeRepository.Update(preferenceType);
            await _repositoryManager.SaveChanges();

            return preferenceType;
        }

        public virtual Task<IEnumerable<MemberPreferenceType>> ListAll() =>
            _repositoryManager.MemberPreferenceTypeRepository.ListAll();

        public virtual Task<IEnumerable<MemberPreferenceType>> ListAllActiveSorted() =>
            _repositoryManager.MemberPreferenceTypeRepository.ListAllActiveSorted();
    }
}
