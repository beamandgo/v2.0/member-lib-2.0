﻿using System.Collections.Generic;

namespace BeamAndGo.Member
{
    public interface IMemberSearch
    {
        IAsyncEnumerable<Member> Search(string[] terms, int start = 0, int count = 100);
    }
}
