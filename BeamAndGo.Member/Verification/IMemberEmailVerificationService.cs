﻿using System.Threading.Tasks;

namespace BeamAndGo.Member.Verification
{
    public interface IMemberEmailVerificationService
    {
        Task Send(string emailSid, string verificationLink);
        Task<bool> Verify(string verificationCode);
    }
}
