﻿using System;
using System.Threading.Tasks;
using BeamAndGo.Member.Messaging;

namespace BeamAndGo.Member.Verification
{
    public class MemberPhoneVerificationService : IMemberPhoneVerificationService
    {
        private readonly IMemberOtpProvider _otpProvider;
        private readonly IMemberPhoneService _phoneService;

        public MemberPhoneVerificationService(
            IMemberOtpProvider otpProvider,
            IMemberPhoneService phoneService)
        {
            _otpProvider = otpProvider;
            _phoneService = phoneService;
        }

        public virtual async Task<string> SendVerification(string phoneSid)
        {
            var memberPhone = await _phoneService.GetBySid(phoneSid);

            // TODO: Check if MemberPhone data is valid - due to migration issues the data may be null
            return await _otpProvider.Generate(memberPhone.CallingCode, memberPhone.PhoneNumber);
        }

        public virtual async Task<bool> Verify(string phoneSid, string prefix, string otp)
        {
            if (string.IsNullOrWhiteSpace(prefix)) throw MemberVerificationErrors.OtpPrefixRequired();
            if (string.IsNullOrWhiteSpace(otp)) throw MemberVerificationErrors.OtpRequired();

            var memberPhone = await _phoneService.GetBySid(phoneSid);

            var verified = await _otpProvider.Verify(memberPhone.CallingCode, memberPhone.PhoneNumber, prefix, otp);
            if (!verified) throw MemberVerificationErrors.OtpInvalid(phoneSid, prefix, otp);

            await _phoneService.Verify(memberPhone.Sid, $"Verified with OTP");

            return true;
        }
    }
}
