﻿using System;

namespace BeamAndGo.Member.Verification
{
    public class MemberEmailVerificationToken
    {
        public virtual string Sid { get; set; } = Guid.NewGuid().ToString("N");
        public virtual string MemberEmailSid { get; set; }
        public virtual DateTimeOffset Expiry { get; set; }
    }
}
