﻿namespace BeamAndGo.Member.Verification
{
    public interface IMemberEmailVerificationFormatter
    {
        (string subject, string body) Format(string firstName, string lastName, string toEmail, string verificationLink);
    }
}
