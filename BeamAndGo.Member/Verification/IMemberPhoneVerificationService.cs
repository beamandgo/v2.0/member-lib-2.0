﻿using System.Threading.Tasks;

namespace BeamAndGo.Member.Verification
{
    public interface IMemberPhoneVerificationService
    {
        Task<string> SendVerification(string phoneSid);
        Task<bool> Verify(string phoneSid, string prefix, string otp);
    }
}
