﻿using System;

namespace BeamAndGo.Member.Verification
{
    public static class MemberVerificationErrors
    {
        private const string Prefix = "MEMBERVERIFICATION";
        
        #region Required
        public static ArgumentException OtpPrefixRequired() =>
            new ArgumentException($"{Prefix}_OTPPREFIX_REQUIRED:Prefix is required.");
        
        public static ArgumentException OtpRequired() =>
            new ArgumentException($"{Prefix}_OTP_REQUIRED:OTP is required.");

        public static ArgumentException VerificationCodeRequired() =>
            new ArgumentException($"{Prefix}_VERIFICATIONCODE_REQUIRED:Email verification code is required.");

        public static ArgumentException VerificationLinkRequired() =>
            new ArgumentException($"{Prefix}_VERIFICATIONLINK_REQUIRED:Email verification link is required.");
        #endregion
        
        #region Invalid
        public static InvalidOperationException OtpInvalid(string phoneSid, string prefix, string otp) =>
            new InvalidOperationException($"{Prefix}_OTP_INVALID:One time pin ({prefix}-{otp}) for phone ({phoneSid}) is invalid.");

        public static InvalidOperationException VerificationCodeInvalid(string verificationCode) =>
            new InvalidOperationException($"{Prefix}_VERIFICATIONCODE_INVALID:Email verification code ({verificationCode}) is invalid.");
        #endregion
    }
}
