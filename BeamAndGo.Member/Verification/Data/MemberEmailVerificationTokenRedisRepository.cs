﻿using System;
using System.Threading.Tasks;
using StackExchange.Redis;
using BeamAndGo.Core.Common;

namespace BeamAndGo.Member.Verification.Data
{
    public class MemberEmailVerificationTokenRedisRepository : IMemberEmailVerificationTokenRepository
    {
        private readonly IConnectionMultiplexer _redis;

        public MemberEmailVerificationTokenRedisRepository(IConnectionMultiplexer redis)
        {
            _redis = redis;
        }

        private const string _keyPrefix = "member.verify.email";
        private string _GetKey(string sid) => $"{_keyPrefix}:{sid}";

        public async Task<MemberEmailVerificationToken> GetBySid(string sid)
        {
            var db = _redis.GetDatabase();
            var token = await db.StringGetAsync(_GetKey(sid));
            if (!token.HasValue) return null;
            return ((string)token).DeserializeJson<MemberEmailVerificationToken>();
        }

        public async Task<MemberEmailVerificationToken> Create(MemberEmailVerificationToken entity)
        {
            var db = _redis.GetDatabase();
            await db.StringSetAsync(_GetKey(entity.Sid), entity.SerializeJson(),
                expiry: entity.Expiry - DateTimeOffset.UtcNow);
            return entity;
        }

        public async Task DeleteBySid(string sid)
        {
            var db = _redis.GetDatabase();
            await db.KeyDeleteAsync(_GetKey(sid));
        }
    }
}
