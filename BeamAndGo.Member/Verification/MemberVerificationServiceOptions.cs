﻿namespace BeamAndGo.Member.Verification
{
    public class MemberVerificationServiceOptions
    {
        /// <summary>
        /// Email verification code expiry, in seconds. Default is 24 hours.
        /// </summary>
        public int EmailVerificationExpirySeconds { get; set; } = 86400;

        /// <summary>
        /// OTP expiry, in seconds. Default is 2 minutes.
        /// </summary>
        public int PhoneVerificationExpirySeconds { get; set; } = 120;
    }
}
