﻿using System;

namespace BeamAndGo.Member.Verification
{
    public class DefaultEmailVerificationFormatter : IMemberEmailVerificationFormatter
    {
        public (string, string) Format(string firstName, string lastName, string toEmail, string verificationLink)
        {
            var subject = "Verify your email";

            var body = String.Join(Environment.NewLine,
                          $"Dear {firstName} {lastName},",
                          $"You have added a new e-mail address {toEmail}.",
                          $"Please click on the link below to verify your e-mail address: {verificationLink}");

            return (subject, body);
        }
    }
}
