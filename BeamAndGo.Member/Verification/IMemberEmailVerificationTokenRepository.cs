﻿using System.Threading.Tasks;

namespace BeamAndGo.Member.Verification
{
    public interface IMemberEmailVerificationTokenRepository
    {
        Task<MemberEmailVerificationToken> Create(MemberEmailVerificationToken entity);
        Task<MemberEmailVerificationToken> GetBySid(string sid);
        Task DeleteBySid(string sid);
    }
}
