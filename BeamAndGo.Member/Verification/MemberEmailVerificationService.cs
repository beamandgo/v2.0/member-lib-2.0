﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using BeamAndGo.Core.Common;
using BeamAndGo.Member.Messaging;

namespace BeamAndGo.Member.Verification
{
    public class MemberEmailVerificationService : IMemberEmailVerificationService
    {
        private readonly MemberVerificationServiceOptions _options;
        private readonly IMemberEmailVerificationTokenRepository _tokenRepository;
        private readonly IMemberEmailVerificationFormatter _emailFormatter;
        private readonly IMemberMailMessagingProvider _messagingProvider;
        private readonly IMemberEmailService _emailService;
        private readonly IMemberService _memberService;

        public MemberEmailVerificationService(
            IOptions<MemberVerificationServiceOptions> options,
            IMemberEmailVerificationTokenRepository tokenRepository,
            IMemberEmailVerificationFormatter emailFormatter,
            IMemberMailMessagingProvider messagingProvider,
            IMemberEmailService emailService,
            IMemberService memberService)
        {
            _options = options.Value;
            _tokenRepository = tokenRepository;
            _emailFormatter = emailFormatter;
            _messagingProvider = messagingProvider;
            _emailService = emailService;
            _memberService = memberService;
        }

        public virtual async Task Send(string emailSid, string verificationLink)
        {
            if (string.IsNullOrWhiteSpace(verificationLink)) throw MemberVerificationErrors.VerificationLinkRequired();

            var memberEmail = await _emailService.GetBySid(emailSid);
            if (memberEmail.IsVerified) throw MemberErrors.EmailVerificationExists(emailSid);

            var token = new MemberEmailVerificationToken
            {
                MemberEmailSid = emailSid,
                Expiry = DateTimeOffset.UtcNow.AddSeconds(_options.EmailVerificationExpirySeconds)
            };

            await _tokenRepository.Create(token);

            // Format the verification link and add VerificationCode
            verificationLink = verificationLink.Format(new { VerificationCode = token.Sid });

            var member = await _memberService.GetById(memberEmail.MemberId);

            (string subject, string body) = _emailFormatter.Format(member.FirstName, member.LastName, memberEmail.Email, verificationLink);

            await _messagingProvider.Send(memberEmail.Email, subject, body, true);
        }

        public virtual async Task<bool> Verify(string verificationCode)
        {
            if (string.IsNullOrWhiteSpace(verificationCode)) throw MemberVerificationErrors.VerificationCodeRequired();

            var token = await _tokenRepository.GetBySid(verificationCode);
            if (token == null) throw MemberVerificationErrors.VerificationCodeInvalid(verificationCode);

            await _emailService.Verify(token.MemberEmailSid, $"Verified with verification code/link");
            await _tokenRepository.DeleteBySid(verificationCode);
            return true;
        }
    }
}
