﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Transactions;

namespace BeamAndGo.Member
{
    public class MemberAddressService: IMemberAddressService
    {
        private readonly IMemberRepositoryManager _repositoryManager;
        private readonly IMemberService _memberService;
        private readonly IMemberLogService _logService;

        public MemberAddressService(
            IMemberRepositoryManager repositoryManager,
            IMemberService memberService,
            IMemberLogService logService)
        {
            _repositoryManager = repositoryManager;
            _memberService = memberService;
            _logService = logService;
        }

        public virtual async Task<MemberAddress> GetBySid(string sid)
        {
            if (string.IsNullOrWhiteSpace(sid)) throw MemberErrors.AddressSidRequired();
            var address = await _repositoryManager.MemberAddressRepository.GetBySid(sid);
            if (address == null) throw MemberErrors.AddressNotFound(sid);
            return address;
        }

        public virtual async Task<MemberAddress> Create(string memberSid, string name, string street, string postalCode, string countryCode, string remarks = null)
        {
            if (string.IsNullOrWhiteSpace(name)) throw MemberErrors.AddressNameRequired();
            if (string.IsNullOrWhiteSpace(street)) throw MemberErrors.StreetRequired();
            if (string.IsNullOrWhiteSpace(countryCode)) throw MemberErrors.CountryCodeRequired();

            // GetBySid will validate Sid
            var member = await _memberService.GetBySid(memberSid, false);

            var memberAddress = new MemberAddress()
            {
                MemberId = member.Id,
                Name = name,
                Street = street,
                PostalCode = postalCode,
                CountryCode = countryCode,
                Remarks = remarks
            };

            // Check if a default exists
            var defaultAddress = await _repositoryManager.MemberAddressRepository.GetDefaultByMemberId(member.Id);
            memberAddress.IsDefault = (defaultAddress == null);

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                await _repositoryManager.MemberAddressRepository.Create(memberAddress);
                await _repositoryManager.SaveChanges();
                await _logService.Log(member.Id, new object[] { "CreateAddress", memberSid, name, street, postalCode, countryCode, remarks });
                scope.Complete();
            }

            return memberAddress;
        }

        public virtual async Task<MemberAddress> Update(string sid, string name, string street, string postalCode, string countryCode, string remarks = null)
        {
            if (string.IsNullOrWhiteSpace(name)) throw MemberErrors.AddressNameRequired();
            if (string.IsNullOrWhiteSpace(street)) throw MemberErrors.StreetRequired();
            if (string.IsNullOrWhiteSpace(countryCode)) throw MemberErrors.CountryCodeRequired();

            // GetBySid will validate Sid
            var memberAddress = await GetBySid(sid);
            memberAddress.Name = name;
            memberAddress.Street = street;
            memberAddress.PostalCode = postalCode;
            memberAddress.CountryCode = countryCode;
            memberAddress.Remarks = remarks;

            // When an address is updated, it loses its verified status
            memberAddress.IsVerified = false;
            memberAddress.VerifiedAt = null;
            memberAddress.VerificationMeta = null;

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                await _repositoryManager.MemberAddressRepository.Update(memberAddress);
                await _repositoryManager.SaveChanges();
                await _logService.Log(memberAddress.MemberId, new object[] { "UpdateAddress", name, street, postalCode, countryCode, remarks });
                scope.Complete();
            }

            return memberAddress;
        }

        public virtual async Task Delete(string sid)
        {
            // GetBySid will validate Sid
            var memberAddress = await GetBySid(sid);

            // Can't delete the default address
            if (memberAddress.IsDefault) throw MemberErrors.DeleteDefaultAddressNotAllowed(memberAddress.Name, memberAddress.Street, memberAddress.PostalCode, memberAddress.CountryCode);

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                await _repositoryManager.MemberAddressRepository.Delete(memberAddress);
                await _repositoryManager.SaveChanges();
                await _logService.Log(memberAddress.MemberId, new object[] { "DeleteAddress", sid });
                scope.Complete();
            }
        }

        public virtual async Task Verify(string sid, string verificationMeta)
        {
            // GetBySid will validate Sid
            var memberAddress = await GetBySid(sid);
            memberAddress.IsVerified = true;
            memberAddress.VerifiedAt = DateTimeOffset.UtcNow;
            memberAddress.VerificationMeta = verificationMeta;

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                await _repositoryManager.MemberAddressRepository.Update(memberAddress);
                await _repositoryManager.SaveChanges();
                await _logService.Log(memberAddress.MemberId, new object[] { "VerifyAddress", sid, verificationMeta });
                scope.Complete();
            }
        }

        public virtual async Task SetDefault(string sid)
        {
            // GetBySid will validate Sid
            var memberAddress = await GetBySid(sid);
            memberAddress.IsDefault = true;

            // Do not allow making an unverified address as default
            // if current default address is already verified
            if (!memberAddress.IsVerified)
            {
                var defaultAddress = await _repositoryManager.MemberAddressRepository.GetDefaultByMemberId(memberAddress.MemberId);
                if (defaultAddress.IsVerified) throw MemberErrors.UnverifiedDefaultAddressNotAllowed(memberAddress.Name, memberAddress.Street, memberAddress.PostalCode, memberAddress.CountryCode);
            }

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                await _repositoryManager.MemberAddressRepository.Update(memberAddress);

                // Loop through all other addresses and remove default
                var addresses = _repositoryManager.MemberAddressRepository.ListByMemberId(memberAddress.MemberId);
                await foreach (var address in addresses)
                {
                    if (memberAddress.Id != address.Id)
                    {
                        address.IsDefault = false;
                        await _repositoryManager.MemberAddressRepository.Update(address);
                    }
                }

                await _repositoryManager.SaveChanges();
                await _logService.Log(memberAddress.MemberId, new object[] { "SetDefaultAddress", sid });
                scope.Complete();
            }
        }

        public virtual async IAsyncEnumerable<MemberAddress> ListByMemberSid(string memberSid)
        {
            // GetBySid will validate Sid
            var member = await _memberService.GetBySid(memberSid);
            await foreach (var a in _repositoryManager.MemberAddressRepository.ListByMemberId(member.Id))
                yield return a;
        }
    }
}
