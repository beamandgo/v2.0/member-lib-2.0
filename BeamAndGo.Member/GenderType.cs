﻿namespace BeamAndGo.Member
{
    public static class GenderType
    {
        public const char Male = 'M';
        public const char Female = 'F';

        public static bool IsValid(char gender)
        {
            switch (gender)
            {
                case Male:
                case Female: return true;
                default: return false;
            }
        }
    }
}
