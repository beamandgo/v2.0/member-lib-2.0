﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace BeamAndGo.Member
{
    [ExcludeFromCodeCoverage]
    public class MemberServiceException : Exception
    {
        public MemberServiceException() : base() { }
        public MemberServiceException(string message) : base(message) { }
        public MemberServiceException(string message, Exception innerException) : base(message, innerException) { }
    }
}
