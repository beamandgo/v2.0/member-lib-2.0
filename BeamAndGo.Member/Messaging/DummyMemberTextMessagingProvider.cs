﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace BeamAndGo.Member.Messaging
{
    public class DummyMemberTextMessagingProvider : IMemberTextMessagingProvider
    {
        private readonly ILogger _logger;

        public DummyMemberTextMessagingProvider(ILogger<DummyMemberTextMessagingProvider> logger)            
        {
            _logger = logger;
        }

        public Task Send(string callingCode, string phone, string message)
        {
            _logger.LogInformation(string.Join('\n',
                $"To: +{callingCode} {phone}",
                message));

            return Task.CompletedTask;
        }
    }
}