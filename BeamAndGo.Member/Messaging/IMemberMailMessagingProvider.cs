﻿using System.Threading.Tasks;

namespace BeamAndGo.Member.Messaging
{
    public interface IMemberMailMessagingProvider
    {
        Task Send(string to, string subject, string body, bool isBodyHtml = true);
    }
}
