﻿using System.Threading.Tasks;

namespace BeamAndGo.Member.Messaging
{
    public interface IMemberOtpProvider
    {
        public Task<string> Generate(string callingCode, string phone);
        public Task<bool> Verify(string callingCode, string phone, string prefix, string pin);
    }
}
