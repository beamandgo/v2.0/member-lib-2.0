﻿using System.Threading.Tasks;

namespace BeamAndGo.Member.Messaging
{
    public interface IMemberTextMessagingProvider
    {
        Task Send(string callingCode, string phone, string message);
    }
}
