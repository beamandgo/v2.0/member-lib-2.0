﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace BeamAndGo.Member.Messaging
{
    public class DummyMemberOtpProvider : IMemberOtpProvider
    {
        private readonly ILogger _logger;

        public DummyMemberOtpProvider(ILogger<DummyMemberOtpProvider> logger)
        {
            _logger = logger;
        }

        public Task<string> Generate(string callingCode, string phone)
        {
            _logger.LogInformation($"OTP sent to: +{callingCode} {phone}");
            return Task.FromResult("xxx");
        }

        public Task<bool> Verify(string callingCode, string phone, string prefix, string pin)
        {
            if (pin == "123456") return Task.FromResult(true);
            return Task.FromResult(false);
        }
    }
}
