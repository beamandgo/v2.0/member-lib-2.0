﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace BeamAndGo.Member.Messaging
{
    public class DummyMemberMailMessagingProvider : IMemberMailMessagingProvider
    {
        private readonly ILogger _logger;

        public DummyMemberMailMessagingProvider(ILogger<DummyMemberMailMessagingProvider> logger)
        {
            _logger = logger;
        }

        public Task Send(string to, string subject, string body, bool isBodyHtml = true)
        {
            _logger.LogInformation(string.Join('\n',
                $"To: {to}",
                $"Subject: {subject}",
                body));

            return Task.CompletedTask;
        }
    }
}
