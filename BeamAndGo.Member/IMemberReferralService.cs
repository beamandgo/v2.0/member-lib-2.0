﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeamAndGo.Member
{
    public interface IMemberReferralService
    {
        Task<MemberReferral> Register(string referredMemberSid, string referralCode, MemberReferralType referralType);

        IAsyncEnumerable<MemberReferral> ListByReferredMemberSidAndReferralType(string memberSid, MemberReferralType referralType);
        IAsyncEnumerable<MemberReferral> ListByReferredMemberSid(string memberSid);

        IAsyncEnumerable<MemberReferral> ListByReferrerMemberSidAndReferralType(string memberSid, MemberReferralType referralType);
        IAsyncEnumerable<MemberReferral> ListByReferrerMemberSid(string memberSid);

        //event EventHandler ReferrerRegistered;
    }
}
