﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeamAndGo.Member
{
    public interface IMemberPreferenceTypeService
    {
        Task<MemberPreferenceType> GetById(int id);
        Task<MemberPreferenceType> Create(string name, string description, string defaultValue);
        Task<MemberPreferenceType> Update(int id, string name, string description, string defaultValue);
        Task Delete(int id);
        Task<MemberPreferenceType> Enable(int id);
        Task<MemberPreferenceType> Disable(int id);
        Task<IEnumerable<MemberPreferenceType>> ListAll();
        Task<IEnumerable<MemberPreferenceType>> ListAllActiveSorted();
    }
}
