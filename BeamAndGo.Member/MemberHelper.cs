﻿using System;

namespace BeamAndGo.Member
{
    public static class MemberHelper
    {
        public static int AgeAt(int birthYear, int birthMonth, int birthDay, DateTime at) =>
            AgeAt(birthYear, birthMonth, birthDay, at.Year, at.Month, at.Day);

        public static int AgeAt(int birthYear, int birthMonth, int birthDay, int atYear, int atMonth, int atDay)
        {
            int y = atYear - birthYear;
            int m = atMonth - birthMonth;
            int d = atDay - birthDay;

            if (d < 0) m--;
            if (m < 0) y--;

            return y;
        }
    }
}
