﻿using System.ComponentModel.DataAnnotations.Schema;
using BeamAndGo.Core.Data;

namespace BeamAndGo.Member
{
    [Table("LK_PreferenceTypes")]
    public class MemberPreferenceType : BaseLookup
    {
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual string DefaultValue { get; set; }
        public virtual int SortOrder { get; set; } 
    }
}
