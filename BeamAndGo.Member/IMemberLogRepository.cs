﻿using System.Collections.Generic;
using BeamAndGo.Core.Data;

namespace BeamAndGo.Member
{
    public interface IMemberLogRepository : IRepository<MemberLog, long>
    {
        IAsyncEnumerable<MemberLog> ListByMemberId(int memberId, int start = 0, int count = 100);
    }
}
