﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Transactions;
using BeamAndGo.Core.Common;

namespace BeamAndGo.Member
{
    public class MemberPhoneService : IMemberPhoneService
    {
        private readonly IMemberRepositoryManager _repositoryManager;
        private readonly IMemberService _memberService;
        private readonly IMemberLogService _logService;

        public MemberPhoneService(
            IMemberRepositoryManager repositoryManager,
            IMemberService memberService,
            IMemberLogService logService)
        {
            _repositoryManager = repositoryManager;
            _memberService = memberService;
            _logService = logService;
        }

        public virtual async Task<MemberPhone> GetBySid(string sid)
        {
            if (string.IsNullOrWhiteSpace(sid)) throw MemberErrors.PhoneSidRequired();
            var phone = await _repositoryManager.MemberPhoneRepository.GetBySid(sid);
            if (phone == null) throw MemberErrors.PhoneNotFound(sid);
            return phone;
        }

        public virtual async Task<MemberPhone> Create(string memberSid, string callingCode, string phone, string identifier = null)
        {
            if (string.IsNullOrWhiteSpace(callingCode)) throw MemberErrors.CallingCodeRequired();
            if (string.IsNullOrWhiteSpace(phone)) throw MemberErrors.PhoneRequired();
            if (!callingCode.IsNumeric()) throw MemberErrors.CallingCodeInvalid(callingCode);
            if (!phone.IsNumeric()) throw MemberErrors.PhoneInvalid(phone);

            // GetBySid will validate Sid
            var member = await _memberService.GetBySid(memberSid);

            if (await Exists(callingCode, phone)) throw MemberErrors.PhoneExists(callingCode, phone);

            var memberPhone = new MemberPhone()
            {
                MemberId = member.Id,
                CallingCode = callingCode,
                PhoneNumber = phone,
                Identifier = identifier,
                IsVerified = false,
            };

            // Check if a default exists
            var defaultPhone = await _repositoryManager.MemberPhoneRepository.GetDefaultByMemberId(member.Id);
            memberPhone.IsDefault = (defaultPhone == null);

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                await _repositoryManager.MemberPhoneRepository.Create(memberPhone);
                await _repositoryManager.SaveChanges();
                await _logService.Log(member.Id, new object[] { "CreatePhone", memberSid, callingCode, phone, identifier });
                scope.Complete();
            }

            return memberPhone;
        }

        public virtual async Task<MemberPhone> Update(string sid, string callingCode, string phone, string identifier = null)
        {
            if (string.IsNullOrWhiteSpace(callingCode)) throw MemberErrors.CallingCodeRequired();
            if (string.IsNullOrWhiteSpace(phone)) throw MemberErrors.PhoneRequired();
            if (!callingCode.IsNumeric()) throw MemberErrors.CallingCodeInvalid(callingCode);
            if (!phone.IsNumeric()) throw MemberErrors.PhoneInvalid(phone);

            // GetBySid will validate Sid
            var memberPhone = await GetBySid(sid);

            // Check if phone in use if it was changed
            if (!memberPhone.CallingCode.Equals(callingCode, StringComparison.OrdinalIgnoreCase) ||
                !memberPhone.PhoneNumber.Equals(phone, StringComparison.OrdinalIgnoreCase) &&
                await Exists(callingCode, phone))
                throw MemberErrors.PhoneExists(callingCode, phone);

            memberPhone.CallingCode = callingCode;
            memberPhone.PhoneNumber = phone;

            // When an phone is updated, it loses its verified status
            memberPhone.IsVerified = false;
            memberPhone.VerifiedAt = null;
            memberPhone.VerificationMeta = null;

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                await _repositoryManager.MemberPhoneRepository.Update(memberPhone);
                await _repositoryManager.SaveChanges();
                await _logService.Log(memberPhone.MemberId, new object[] { "UpdatePhone", sid, callingCode, phone, identifier });
                scope.Complete();
            }

            return memberPhone;
        }

        public virtual async Task Delete(string sid)
        {
            // GetBySid will validate Sid
            var memberPhone = await GetBySid(sid);

            // Can't delete the default phone
            if (memberPhone.IsDefault) throw MemberErrors.DeleteDefaultPhoneNotAllowed(memberPhone.CallingCode, memberPhone.PhoneNumber);

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                await _repositoryManager.MemberPhoneRepository.Delete(memberPhone);
                await _repositoryManager.SaveChanges();
                await _logService.Log(memberPhone.MemberId, new object[] { "DeletePhone", sid });
                scope.Complete();
            }
        }

        public virtual async Task Verify(string sid, string meta)
        {
            // GetBySid will validate Sid
            var memberPhone = await GetBySid(sid);

            if (memberPhone.IsVerified) throw MemberErrors.PhoneVerificationExists(sid);

            memberPhone.IsVerified = true;
            memberPhone.VerifiedAt = DateTimeOffset.UtcNow;
            memberPhone.VerificationMeta = meta;

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                await _repositoryManager.MemberPhoneRepository.Update(memberPhone);
                await _repositoryManager.SaveChanges();
                await _logService.Log(memberPhone.MemberId, new object[] { "VerifyPhone", sid, meta });
                scope.Complete();
            }
        }

        public virtual async Task SetDefault(string sid)
        {
            // GetBySid will validate Sid
            var memberPhone = await GetBySid(sid);            

            // Do not allow making an unverified phone as default
            // if current default phone is already verified
            if (!memberPhone.IsVerified)
            {
                var defaultPhone = await _repositoryManager.MemberPhoneRepository.GetDefaultByMemberId(memberPhone.MemberId);
                if (defaultPhone.IsVerified) throw MemberErrors.UnverifiedDefaultPhoneNotAllowed(memberPhone.CallingCode, memberPhone.PhoneNumber);
            }

            memberPhone.IsDefault = true;

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                await _repositoryManager.MemberPhoneRepository.Update(memberPhone);

                // Loop through all other phones and remove default
                var phones = _repositoryManager.MemberPhoneRepository.ListByMemberId(memberPhone.MemberId);
                await foreach (var phone in phones)
                {
                    if (memberPhone.Id != phone.Id && phone.IsDefault)
                    {
                        phone.IsDefault = false;
                        await _repositoryManager.MemberPhoneRepository.Update(phone);
                    }
                }

                await _repositoryManager.SaveChanges();
                await _logService.Log(memberPhone.MemberId, new object[] { "SetDefaultPhone", sid });
                scope.Complete();
            }
        }

        public virtual async IAsyncEnumerable<MemberPhone> ListByMemberSid(string memberSid)
        {
            // GetBySid will validate Sid
            var member = await _memberService.GetBySid(memberSid, false);
            await foreach (var p in _repositoryManager.MemberPhoneRepository.ListByMemberId(member.Id))
                yield return p;
        }

        public async Task<bool> Exists(string callingCode, string phone)
        {
            return null != await _repositoryManager.MemberPhoneRepository.GetByCallingCodeAndPhone(callingCode, phone);
        }
    }
}
