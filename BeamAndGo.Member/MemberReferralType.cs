﻿namespace BeamAndGo.Member
{
    public enum MemberReferralType
    {
        Referral = 0,
        SalesPerson = 1,
        Delegate = 2
    }
}
