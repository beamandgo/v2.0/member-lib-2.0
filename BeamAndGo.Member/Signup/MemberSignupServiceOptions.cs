﻿namespace BeamAndGo.Member.Signup
{
    public class MemberSignupServiceOptions
    {
        public bool PhoneRequired { get; set; } = true;
        public bool EmailRequired { get; set; } = false;
        public bool AddressRequired { get; set; } = false;
    }
}
