﻿using System;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Signup
{
    public interface IMemberSignupService
    {
        Task<Member> Signup(SignupData signup, string notes = null);

        event EventHandler MemberSignup;
    }
}
