﻿using System.Collections.Generic;

namespace BeamAndGo.Member.Signup
{
    public class SignupData
    {
        public virtual string FirstName { get; set; } // Required
        public virtual string MiddleName { get; set; }
        public virtual string LastName { get; set; } // Required
        public virtual string DisplayName { get; set; }

        public virtual char? Gender { get; set; } // 'M' or 'F'

        public virtual int? BirthDay { get; set; }
        public virtual int? BirthMonth { get; set; }
        public virtual int? BirthYear { get; set; }

        public virtual string CallingCode { get; set; } // Derived from MemberPhone
        public virtual string PhoneNumber { get; set; } // Derived from MemberPhone

        public virtual string Email { get; set; } // Derived from MemberEmail

        public virtual string Street { get; set; } // Derived from MemberAddress
        public virtual string PostalCode { get; set; } // Derived from MemberAddress
        public virtual string CountryCode { get; set; } // Will be applied to both Member and MemberAddress

        public virtual string ReferralCode { get; set; }
        public virtual MemberReferralType ReferralType { get; set; } = MemberReferralType.Referral;

        public virtual string Source { get; set; }
        public virtual string IPAddress { get; set; }

        public virtual IDictionary<int, string> Preferences { get; set; } = new Dictionary<int, string>();
    }
}
