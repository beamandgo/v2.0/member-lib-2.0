﻿using System;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.Extensions.Options;

namespace BeamAndGo.Member.Signup
{
    public class MemberSignupService : IMemberSignupService
    {
        private readonly MemberSignupServiceOptions _options;
        private readonly IMemberService _memberService;
        private readonly IMemberPhoneService _memberPhoneService;
        private readonly IMemberEmailService _memberEmailService;
        private readonly IMemberAddressService _memberAddressService;
        private readonly IMemberReferralService _memberReferralService;
        private readonly IMemberPreferenceService _memberPreferenceService;
        private readonly IMemberLogService _memberLogService;

        public event EventHandler MemberSignup;

        public MemberSignupService(
            IOptions<MemberSignupServiceOptions> options,
            IMemberService memberService,
            IMemberPhoneService memberPhoneService,
            IMemberEmailService memberEmailService,
            IMemberAddressService memberAddressService,
            IMemberReferralService memberReferralService,
            IMemberPreferenceService memberPreferenceService,
            IMemberLogService memberLogService)
        {
            _options = options.Value;
            _memberService = memberService;
            _memberPhoneService = memberPhoneService;
            _memberEmailService = memberEmailService;
            _memberAddressService = memberAddressService;
            _memberReferralService = memberReferralService;
            _memberPreferenceService = memberPreferenceService;
            _memberLogService = memberLogService;
        }

        public async Task<Member> Signup(SignupData signup, string notes = null)
        {
            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                // Create Member
                var member = await _memberService.Create(new CreateMemberData
                {
                    FirstName = signup.FirstName,
                    MiddleName = signup.MiddleName,
                    LastName = signup.LastName,
                    DisplayName = signup.DisplayName,
                    Gender = signup.Gender,
                    BirthYear = signup.BirthYear,
                    BirthMonth = signup.BirthMonth,
                    BirthDay = signup.BirthDay,
                    CountryCode = signup.CountryCode,
                    Source = signup.Source,
                    IPAddress = signup.IPAddress
                });

                // Add phone
                if (_options.PhoneRequired ||
                    !string.IsNullOrWhiteSpace(signup.CallingCode) ||
                    !string.IsNullOrWhiteSpace(signup.PhoneNumber))
                    await _memberPhoneService.Create(member.Sid, signup.CallingCode, signup.PhoneNumber);

                // Add email
                if (_options.EmailRequired ||
                    !string.IsNullOrWhiteSpace(signup.Email))
                    await _memberEmailService.Create(member.Sid, signup.Email);

                // Add address
                if (_options.AddressRequired ||
                    !string.IsNullOrWhiteSpace(signup.Street) ||
                    !string.IsNullOrWhiteSpace(signup.PostalCode))
                    await _memberAddressService.Create(
                        member.Sid,
                        "Default",
                        signup.Street,
                        signup.PostalCode,
                        signup.CountryCode);

                // Add preferences
                foreach (var p in signup.Preferences)
                {
                    await _memberPreferenceService.CreateOrUpdate(
                        member.Sid,
                        p.Key,
                        p.Value);
                }

                if (!string.IsNullOrWhiteSpace(signup.ReferralCode))
                    await _memberReferralService.Register(member.Sid, signup.ReferralCode, signup.ReferralType);

                await _memberLogService.Log(member.Id, new object[] { "Signup", signup }, notes);

                scope.Complete();

                return member;
            }
        }
    }
}
