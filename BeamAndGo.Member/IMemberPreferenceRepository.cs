﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BeamAndGo.Core.Data;

namespace BeamAndGo.Member
{
    public interface IMemberPreferenceRepository : IRepository<MemberPreference, int>
    {
        Task<MemberPreference> GetBySid(string sid);
        Task<MemberPreference> GetByMemberIdAndPreferenceTypeId(int memberId, int preferenceTypeId);
        IAsyncEnumerable<MemberPreference> ListByMemberId(int memberId);
        IAsyncEnumerable<MemberPreference> ListByPreferenceTypeId(int preferenceTypeId);
    }
}
