﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BeamAndGo.Core.Data;

namespace BeamAndGo.Member
{
    public interface IMemberAddressRepository: IRepository<MemberAddress, int>
    {
        Task<MemberAddress> GetBySid(string sid);
        Task<MemberAddress> GetDefaultByMemberId(int memberId);
        IAsyncEnumerable<MemberAddress> ListByMemberId(int memberId);
    }
}
