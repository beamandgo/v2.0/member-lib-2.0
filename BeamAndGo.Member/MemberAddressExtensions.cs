﻿namespace BeamAndGo.Member
{
    public static class MemberAddressExtensions
    {
        /// <summary>
        /// Split a single address string by newline into multiple lines
        /// </summary>
        public static string[] StreetLines(this MemberAddress memberAddress)
        {
            return memberAddress.Street.Split('\n', System.StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
