﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeamAndGo.Member
{    
    public interface IMemberPreferenceService
    {
        Task<MemberPreference> GetBySid(string sid);
        Task<MemberPreference> CreateOrUpdate(string memberSid, int preferenceTypeId, string value);
        Task Delete(string sid);

        IAsyncEnumerable<MemberPreference> ListByMemberSid(string memberSid);
        IAsyncEnumerable<MemberPreference> ListWithDefaultByMemberSid(string memberSid);
    }
}
