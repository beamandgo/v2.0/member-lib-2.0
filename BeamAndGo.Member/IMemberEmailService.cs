﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeamAndGo.Member
{
    public interface IMemberEmailService
    {
        Task<MemberEmail> GetBySid(string sid);
        Task<MemberEmail> Create(string memberSid, string email, string identifier = null);
        Task<MemberEmail> Update(string sid, string email, string identifier = null);
        Task Delete(string sid);

        Task Verify(string sid, string meta);
        Task SetDefault(string sid);

        IAsyncEnumerable<MemberEmail> ListByMemberSid(string memberSid);

        Task<bool> Exists(string email);
    }
}
