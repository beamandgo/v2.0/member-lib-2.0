﻿namespace BeamAndGo.Member
{
    public class CreateMemberData
    {
        public virtual string FirstName { get; set; } // Required
        public virtual string MiddleName { get; set; }
        public virtual string LastName { get; set; } // Required
        public virtual string DisplayName { get; set; }

        public virtual char? Gender { get; set; } // 'M' or 'F'

        public virtual int? BirthDay { get; set; }
        public virtual int? BirthMonth { get; set; }
        public virtual int? BirthYear { get; set; }

        public virtual string CountryCode { get; set; }

        public virtual string Source { get; set; }
        public virtual string IPAddress { get; set; }
    }
}
