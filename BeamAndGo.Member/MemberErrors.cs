﻿using System;

namespace BeamAndGo.Member
{
    public static class MemberErrors
    {
        private const string Prefix = "MEMBER";
        
        #region Required
        public static ArgumentException AddressNameRequired() =>
            new ArgumentException($"{Prefix}_ADDRESSNAME_REQUIRED:Address name is required.");

        public static ArgumentException AddressSidRequired() =>
            new ArgumentException($"{Prefix}_ADDRESSSID_REQUIRED:Address SID is required.");

        public static ArgumentException CallingCodeRequired() =>
            new ArgumentException($"{Prefix}_CALLINGCODE_REQUIRED:Calling code is required.");

        public static ArgumentException CountryCodeRequired() =>
            new ArgumentException($"{Prefix}_COUNTRYCODE_REQUIRED:Country is required.");

        public static ArgumentException EmailRequired() =>
            new ArgumentException($"{Prefix}_EMAIL_REQUIRED:Email is required.");

        public static ArgumentException EmailSidRequired() =>
            new ArgumentException($"{Prefix}_EMAILSID_REQUIRED:Email SID is required.");

        public static ArgumentException FirstNameRequired() =>
            new ArgumentException($"{Prefix}_FIRSTNAME_REQUIRED:First Name is required.");

        public static ArgumentException LastNameRequired() =>
            new ArgumentException($"{Prefix}_LASTNAME_REQUIRED:Last Name is required.");

        public static ArgumentException MemberSidRequired() =>
            new ArgumentException($"{Prefix}_SID_REQUIRED:Member SID is required.");

        public static ArgumentException PhoneRequired() =>
            new ArgumentException($"{Prefix}_PHONE_REQUIRED:Phone number is required.");

        public static ArgumentException PhoneSidRequired() =>
            new ArgumentException($"{Prefix}_PHONESID_REQUIRED:Phone SID is required.");

        public static ArgumentException PreferenceSidRequired() =>
            new ArgumentException($"{Prefix}_PREFERENCESID_REQUIRED:Preference SID is required.");

        public static ArgumentException PreferenceTypeNameRequired() =>
            new ArgumentException($"{Prefix}_PREFERENCETYPENAME_REQUIRED:Name is required.");

        public static ArgumentException PreferenceTypeDefaultValueRequired() =>
            new ArgumentException($"{Prefix}_PREFERENCETYPEDEFAULTVALUE_REQUIRED:Default value is required.");

        public static ArgumentException PreferenceTypeDescriptionRequired() =>
            new ArgumentException($"{Prefix}_PREFERENCETYPEDESCRIPTION_REQUIRED:Description is required.");

        public static ArgumentException PreferenceValueRequired() =>
            new ArgumentException($"{Prefix}_PREFERENCEVALUE_REQUIRED:Preference value is required.");

        public static ArgumentException ReferralCodeRequired() =>
            new ArgumentException($"{Prefix}_REFERRALCODE_REQUIRED:Referral code is required.");

        public static ArgumentException RelationshipSidRequired() =>
            new ArgumentException($"{Prefix}_RELATIONSHIPSID_REQUIRED:Relationship SID is required.");

        public static ArgumentException RelationshipTypeNameRequired() =>
            new ArgumentException($"{Prefix}_RELATIONSHIPTYPENAME_REQUIRED:Relationship type name is required.");

        public static ArgumentException SearchKeywordRequired() =>
            new ArgumentException($"{Prefix}_SEARCHKEYWORD_REQUIRED:Search keyword is required.");

        public static ArgumentException StreetRequired() =>
            new ArgumentException($"{Prefix}_STREET_REQUIRED:Street is required.");
        #endregion

        #region Invalid
        public static ArgumentException BirthDateInvalid(int year, int month, int day) =>
            new ArgumentException($"{Prefix}_BIRTHDATE_INVALID:Birth date ({year}/{month}/{day}) is invalid.");

        public static ArgumentException CallingCodeInvalid(string callingCode) =>
            new ArgumentException($"{Prefix}_CALLINGCODE_INVALID:Calling code ({callingCode}) is invalid.");

        public static ArgumentException EmailInvalid(string email) =>
            new ArgumentException($"{Prefix}_EMAIL_INVALID:Email ({email}) is invalid.");

        public static ArgumentException GenderInvalid(char gender) =>
            new ArgumentException($"{Prefix}_GENDER_INVALID:Gender ({gender}) is invalid.");

        public static ArgumentException PhoneInvalid(string phone) =>
            new ArgumentException($"{Prefix}_PHONE_INVALID:Phone number ({phone}) is invalid.");

        public static ArgumentException PreferenceTypeInvalid(int preferencetypeId) =>
            new ArgumentException($"{Prefix}_PREFERENCETYPE_INVALID:Preference type ({preferencetypeId}) is invalid.");

        public static ArgumentException ReferralCodeInvalid(string referralCode) =>
            new ArgumentException($"{Prefix}_REFERRALCODE_INVALID:Referral code ({referralCode}) is invalid.");
        #endregion

        #region Exists
        public static InvalidOperationException EmailExists(string email) =>
            new InvalidOperationException($"{Prefix}_EMAIL_EXISTS:Email ({email}) is already in use.");

        public static InvalidOperationException EmailVerificationExists(string emailSid) =>
            new InvalidOperationException($"{Prefix}_EMAILVERIFICATION_EXISTS:Email ({emailSid}) is already verified.");

        public static InvalidOperationException PhoneExists(string callingCode, string phone) =>
            new InvalidOperationException($"{Prefix}_PHONE_EXISTS:Phone ({callingCode} {phone}) is already in use.");

        public static InvalidOperationException PhoneVerificationExists(string phoneSid) =>
            new InvalidOperationException($"{Prefix}_PHONEVERIFICATION_EXISTS:Phone ({phoneSid}) is already verified.");

        public static InvalidOperationException ReferrerExists(string referredMemberSid, MemberReferralType referralType) =>
            new InvalidOperationException($"{Prefix}_REFERRER_EXISTS:Member ({referredMemberSid}) is already registered with a {referralType}.");
        #endregion

        #region Not Found
        public static ArgumentException AddressNotFound(string addressSid) =>
            new ArgumentException($"{Prefix}_ADDRESS_NOTFOUND:Address ({addressSid}) is not found.");

        public static ArgumentException EmailNotFound(string emailSid) =>
            new ArgumentException($"{Prefix}_EMAIL_NOTFOUND:Email ({emailSid}) is not found.");

        public static ArgumentException MemberNotFound(int id) =>
            new ArgumentException($"{Prefix}_MEMBER_NOTFOUND:Member ({id}) is not found.");

        public static ArgumentException MemberNotFound(string term) =>
            new ArgumentException($"{Prefix}_MEMBER_NOTFOUND:Member ({term}) is not found.");

        public static ArgumentException PhoneNotFound(string phoneSid) =>
            new ArgumentException($"{Prefix}_PHONE_NOTFOUND:Phone ({phoneSid}) is not found.");

        public static ArgumentException PreferenceNotFound(string preferenceSid) =>
            new ArgumentException($"{Prefix}_PREFERENCE_NOTFOUND:Preference ({preferenceSid}) is not found.");

        public static ArgumentException PreferenceTypeNotFound(int preferenceTypeId) =>
            new ArgumentException($"{Prefix}_PREFERENCETYPE_NOTFOUND:Preference type ({preferenceTypeId}) is not found.");

        public static ArgumentException RelationshipNotFound(string relationshipSid) =>
            new ArgumentException($"{Prefix}_RELATIONSHIP_NOTFOUND:Relationship ({relationshipSid}) is not found.");

        public static ArgumentException RelationshipTypeNotFound(int relationshipTypeId) =>
            new ArgumentException($"{Prefix}_RELATIONSHIPTYPE_NOTFOUND:Relationship type ({relationshipTypeId}) is not found.");
        #endregion

        #region Not Allowed
        public static InvalidOperationException DeleteDefaultAddressNotAllowed(string name, string street, string postalCode, string country) =>
            new InvalidOperationException($"{Prefix}_DELETEDEFAULTADDRESS_NOTALLOWED:Deleting primary address ({name}, {street}, {postalCode}, {country}) is not allowed.");

        public static InvalidOperationException DeleteDefaultEmailNotAllowed(string email) =>
            new InvalidOperationException($"{Prefix}_DELETEDEFAULTEMAIL_NOTALLOWED:Deleting default email ({email}) is not allowed.");

        public static InvalidOperationException DeleteDefaultPhoneNotAllowed(string callingCode, string phone) =>
            new InvalidOperationException($"{Prefix}_DELETEDEFAULTPHONE_NOTALLOWED:Deleting default phone ({callingCode} {phone}) is not allowed.");

        public static InvalidOperationException DeleteRelationshipTypeNotAllowed(string name) =>
            new InvalidOperationException($"{Prefix}_DELETERELATIONSHIPTYPE_NOTALLOWED:Deleting relationship type ({name}) is not allowed.");

        public static InvalidOperationException DeletePreferenceTypeNotAllowed(string name) =>
            new InvalidOperationException($"{Prefix}_DELETEPREFERENCETYPE_NOTALLOWED:Deleting preference type ({name}) is not allowed.");

        public static InvalidOperationException UnverifiedDefaultAddressNotAllowed(string name, string street, string postalCode, string country) =>
            new InvalidOperationException($"{Prefix}_UNVERIFIEDDEFAULTADDRESS_NOTALLOWED:Setting an unverified address ({name}, {street}, {postalCode}, {country}) as primary address is not allowed.");

        public static InvalidOperationException UnverifiedDefaultEmailNotAllowed(string email) =>
            new InvalidOperationException($"{Prefix}_UNVERIFIEDDEFAULTEMAIL_NOTALLOWED:Setting an unverified email ({email}) as primary email is not allowed.");

        public static InvalidOperationException UnverifiedDefaultPhoneNotAllowed(string callingCode, string phone) =>
            new InvalidOperationException($"{Prefix}_UNVERIFIEDDEFAULTPHONE_NOTALLOWED:Setting an unverified phone ({callingCode} {phone}) as primary phone is not allowed.");
        #endregion
    }
}
