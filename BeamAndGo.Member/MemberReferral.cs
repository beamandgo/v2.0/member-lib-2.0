﻿using System.ComponentModel.DataAnnotations.Schema;
using BeamAndGo.Core.Data;

namespace BeamAndGo.Member
{
    public class MemberReferral : BaseEntityWithSid<int>
    {
        /// <summary>
        /// Member whom is referred (receiver)
        /// </summary>
        public virtual int  ReferredMemberId { get; set; }

        /// <summary>
        /// Member whom referred this member (sender)
        /// </summary>
        public virtual int? ReferrerMemberId { get; set; }

        [Column("LK_ReferralTypeId")]
        public MemberReferralType ReferralType { get; set; }

        [ForeignKey("ReferredMemberId")]
        public virtual Member Referred { get; set; }

        [ForeignKey("ReferrerMemberId")]
        public virtual Member Referrer { get; set; }
    }
}
