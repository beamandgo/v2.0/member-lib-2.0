﻿using System;
using BeamAndGo.Core.Data;

namespace BeamAndGo.Member
{
    public class MemberPhone : BaseEntityWithSid<int>
    {
        public virtual int MemberId { get; set; }

        public virtual string CallingCode { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string Identifier { get; set; }

        public virtual bool IsDefault { get; set; }

        public virtual bool IsVerified { get; set; }
        public virtual DateTimeOffset? VerifiedAt { get; set; }
        public virtual string VerificationMeta { get; set; }
    }
}
