﻿using System;
using BeamAndGo.Core.Data;

namespace BeamAndGo.Member
{
    public class MemberEmail : BaseEntityWithSid<int>
    {
        public virtual int MemberId { get; set; }
 
        public virtual string Email { get; set; }
        public virtual string Identifier { get; set; }

        public virtual bool IsDefault { get; set; }

        public virtual bool IsVerified { get; set; }
        public virtual DateTimeOffset? VerifiedAt { get; set; }
        public virtual string VerificationMeta { get; set; }
    }
}
