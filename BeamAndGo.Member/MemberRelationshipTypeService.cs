﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeamAndGo.Core.Data;

namespace BeamAndGo.Member
{
    public class MemberRelationshipTypeService : IMemberRelationshipTypeService
    {
        private readonly IMemberRepositoryManager _repositoryManager;

        public MemberRelationshipTypeService(IMemberRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
        }

        public virtual async Task<MemberRelationshipType> GetById(int id)
        {
            var relationshipType = await _repositoryManager.MemberRelationshipTypeRepository.GetById(id);
            if (relationshipType == null) throw MemberErrors.RelationshipTypeNotFound(id);
            return relationshipType;
        }

        public virtual async Task<MemberRelationshipType> Create(string name, char? gender = null)
        {
            if (string.IsNullOrWhiteSpace(name)) throw MemberErrors.RelationshipTypeNameRequired();

            var memberRelationshipType = new MemberRelationshipType
            {
                Name = name,
                Gender = gender
            };

            await _repositoryManager.MemberRelationshipTypeRepository.Create(memberRelationshipType);
            await _repositoryManager.SaveChanges();

            return memberRelationshipType;
        }

        public virtual async Task<MemberRelationshipType> Update(int id, string name, char? gender = null)
        {
            if (string.IsNullOrEmpty(name)) throw MemberErrors.RelationshipTypeNameRequired();

            var relationshipType = await GetById(id);
            relationshipType.Name = name;
            relationshipType.Gender = gender;

            await _repositoryManager.MemberRelationshipTypeRepository.Update(relationshipType);
            await _repositoryManager.SaveChanges();

            return relationshipType;
        }

        public virtual async Task Delete(int id)
        {
            var relationshipType = await GetById(id);
            var enumerator = _repositoryManager.MemberRelationshipRepository.ListByRelationshipTypeId(id).GetAsyncEnumerator();

            if (await enumerator.MoveNextAsync())
            {
                await enumerator.DisposeAsync();
                throw MemberErrors.DeleteRelationshipTypeNotAllowed(relationshipType.Name);
            }

            await enumerator.DisposeAsync();

            await _repositoryManager.MemberRelationshipTypeRepository.Delete(relationshipType);
            await _repositoryManager.SaveChanges();
        }

        public virtual async Task<MemberRelationshipType> Enable(int id)
        {
            var relationshipType = await GetById(id);
            relationshipType.RecordStatus = RecordStatus.Active;

            await _repositoryManager.MemberRelationshipTypeRepository.Update(relationshipType);
            await _repositoryManager.SaveChanges();

            return relationshipType;
        }

        public virtual async Task<MemberRelationshipType> Disable(int id)
        {
            var relationshipType = await GetById(id);

            relationshipType.RecordStatus = RecordStatus.Inactive;

            await _repositoryManager.MemberRelationshipTypeRepository.Update(relationshipType);
            await _repositoryManager.SaveChanges();

            return relationshipType;
        }

        public virtual Task<IEnumerable<MemberRelationshipType>> ListAll() =>
            _repositoryManager.MemberRelationshipTypeRepository.ListAll();

        public virtual Task<IEnumerable<MemberRelationshipType>> ListAllActiveSortedByName() =>
            _repositoryManager.MemberRelationshipTypeRepository.ListAllActiveSortedByName();
    }
}
