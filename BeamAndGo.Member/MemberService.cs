﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.Extensions.Options;
using BeamAndGo.Core.Data;
using BeamAndGo.Core.Common;

namespace BeamAndGo.Member
{
    public class MemberService : IMemberService
    {
        private readonly MemberServiceOptions _options;
        private readonly IMemberRepositoryManager _repositoryManager;
        private readonly IMemberSearch _memberSearch;
        private readonly IMemberLogService _memberLogService;

        public event EventHandler MemberCreated;
        public event EventHandler MemberUpdated;

        public MemberService(
            IOptions<MemberServiceOptions> options,
            IMemberRepositoryManager repositoryManager,
            IMemberSearch memberSearch,
            IMemberLogService memberLogService)
        {
            _options = options.Value;
            _repositoryManager = repositoryManager;
            _memberSearch = memberSearch;
            _memberLogService = memberLogService;
        }

        public virtual Task<int> Count() =>
            _repositoryManager.MemberRepository.Count();

        public virtual async Task<Member> GetById(int id)
        {
            var member = await _repositoryManager.MemberRepository.GetById(id);
            if (member == null) throw MemberErrors.MemberNotFound(id);
            return member;
        }

        public virtual async Task<Member> GetBySid(string sid, bool includeRelated = false)
        {
            if (string.IsNullOrWhiteSpace(sid)) throw MemberErrors.MemberSidRequired();
            var member = await _repositoryManager.MemberRepository.GetBySid(sid, includeRelated);
            if (member == null) throw MemberErrors.MemberNotFound(sid);
            return member;
        }

        public virtual async Task<Member> GetByPhone(string phone)
        {
            if (string.IsNullOrWhiteSpace(phone)) throw MemberErrors.PhoneRequired();
            if (!phone.IsNumeric()) throw MemberErrors.PhoneInvalid(phone);

            var member = await _repositoryManager.MemberRepository.GetByPhone(phone);
            if (member == null) throw MemberErrors.MemberNotFound(phone);
            
            return member;
        }

        public virtual async Task<Member> GetByPhone(string callingCode, string phone)
        {
            if (string.IsNullOrWhiteSpace(callingCode)) throw MemberErrors.CallingCodeRequired();
            if (string.IsNullOrWhiteSpace(phone)) throw MemberErrors.PhoneRequired();
            if (!callingCode.IsNumeric()) throw MemberErrors.CallingCodeInvalid(callingCode);
            if (!phone.IsNumeric()) throw MemberErrors.PhoneInvalid(phone);

            var member = await _repositoryManager.MemberRepository.GetByPhone(callingCode, phone);
            if (member == null) throw MemberErrors.MemberNotFound(callingCode + phone);
            
            return member;
        }

        public virtual async Task<Member> GetByEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email)) throw MemberErrors.EmailRequired();
            if (!email.IsValidEmail()) throw MemberErrors.EmailInvalid(email);

            var member = await _repositoryManager.MemberRepository.GetByEmail(email);
            if (member == null) throw MemberErrors.MemberNotFound(email);
            
            return member;
        }

        public virtual async Task<Member> Create(CreateMemberData create)
        {
            if (string.IsNullOrWhiteSpace(create.FirstName)) throw MemberErrors.FirstNameRequired();
            if (string.IsNullOrWhiteSpace(create.LastName)) throw MemberErrors.LastNameRequired();
            if (string.IsNullOrWhiteSpace(create.CountryCode)) throw MemberErrors.CountryCodeRequired();

            // Default display name
            if (string.IsNullOrWhiteSpace(create.DisplayName))
                    create.DisplayName = $"{create.FirstName} {create.LastName}";

            var member = new Member
            {
                FirstName = create.FirstName,
                MiddleName = create.MiddleName,
                LastName = create.LastName,
                DisplayName = create.DisplayName,
                Gender = create.Gender,
                BirthYear = create.BirthYear,
                BirthMonth = create.BirthMonth,
                BirthDay = create.BirthDay,
                CountryCode = create.CountryCode,
                Source = create.Source,
                IPAddress = create.IPAddress
            };

            if (!_ValidateBirthDate(member))
                throw MemberErrors.BirthDateInvalid(member.BirthYear.Value, member.BirthMonth.Value, member.BirthDay.Value);

            if (!_ValidateGender(member))
                throw MemberErrors.GenderInvalid(member.Gender.Value);

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                await _repositoryManager.MemberRepository.Create(member);
                await _repositoryManager.SaveChanges();

                // We can only generate a referral code after creating as it depends on member ID
                await _repositoryManager.MemberRepository.Update(_GenerateReferralCode(member));
                await _repositoryManager.SaveChanges();

                await _memberLogService.Log(member.Id, new object[] { "CreateMember", create });
                scope.Complete();
            }

            return member;
        }

        public virtual async Task<Member> Update(string sid, UpdateMemberData update)
        {
            if (string.IsNullOrWhiteSpace(update.FirstName)) throw MemberErrors.FirstNameRequired();
            if (string.IsNullOrWhiteSpace(update.LastName)) throw MemberErrors.LastNameRequired();
            if (string.IsNullOrWhiteSpace(update.CountryCode)) throw MemberErrors.CountryCodeRequired();

            if (string.IsNullOrWhiteSpace(update.DisplayName))
                update.DisplayName = $"{update.FirstName} {update.LastName}";

            // GetBySid validates MemberSid as well
            var member = await GetBySid(sid, false);

            member.FirstName = update.FirstName;
            member.MiddleName = update.MiddleName;
            member.LastName = update.LastName;
            member.DisplayName = update.DisplayName;
            member.Gender = update.Gender;
            member.BirthYear = update.BirthYear;
            member.BirthMonth = update.BirthMonth;
            member.BirthDay = update.BirthDay;
            member.CountryCode = update.CountryCode;

            if (!_ValidateBirthDate(member))
                throw MemberErrors.BirthDateInvalid(member.BirthYear.Value, member.BirthMonth.Value, member.BirthDay.Value);

            if (!_ValidateGender(member))
                throw MemberErrors.GenderInvalid(member.Gender.Value);

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                await _repositoryManager.MemberRepository.Update(member);
                await _repositoryManager.SaveChanges();
                await _memberLogService.Log(member.Id, new object[] { "UpdateMember", update });
                scope.Complete();
            }

            return member;
        }

        public virtual async Task<Member> Disable(string memberSid)
        {
            var member = await GetBySid(memberSid);

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                member.RecordStatus = RecordStatus.Inactive;
                await _repositoryManager.MemberRepository.Update(member);
                await _repositoryManager.SaveChanges();
                await _memberLogService.Log(member.Id, new object[] { "DisableMember", memberSid });
                scope.Complete();
            }

            return member;
        }

        public virtual async Task<Member> Enable(string memberSid)
        {
            var member = await GetBySid(memberSid);

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                member.RecordStatus = RecordStatus.Active;
                await _repositoryManager.MemberRepository.Update(member);
                await _repositoryManager.SaveChanges();
                await _memberLogService.Log(member.Id, new object[] { "EnableMember", memberSid });
                scope.Complete();
            }

            return member;
        }

        public virtual Task<IEnumerable<Member>> ListAll(int start = 0, int count = 100) =>
            _repositoryManager.MemberRepository.ListAll(start, count);

        public virtual async IAsyncEnumerable<Member> Search(string search, int start = 0, int count = 100)
        {
            if (string.IsNullOrWhiteSpace(search)) throw MemberErrors.SearchKeywordRequired();
            await foreach (var m in _memberSearch.Search(search.Split(' ', StringSplitOptions.RemoveEmptyEntries), start, count))
                yield return m;
        }
       
        public virtual async Task<Member> SearchExact(string search)
        {
            if (string.IsNullOrWhiteSpace(search)) throw MemberErrors.SearchKeywordRequired();

            search = search.Trim();

            if (search.Contains("@")) return await GetByEmail(search);

            else if (search.IsNumeric()) return await GetByPhone(search);

            return null;
        }

        #region Private methods
        private Member _GenerateReferralCode(Member member)
        {
            if (string.IsNullOrWhiteSpace(member.ReferralCode))
            {
                var random = new Random().Next(10, 99);
                member.ReferralCode = $"{member.Id}{random}{member.FirstName[0]}{member.LastName[0]}".ToUpper();
            }

            return member;
        }

        private bool _ValidateBirthDate(Member member)
        {
            if (member.BirthYear == null ||
                member.BirthMonth == null ||
                member.BirthDay == null)
                return true;

            var age = member.AgeAt(DateTime.Now);

            if (age < _options.MinimumAge ||
                age > _options.MaximumAge)
                return false;

            return true;
        }

        private bool _ValidateGender(Member member)
        {
            if (member.Gender == null) return true;
            return GenderType.IsValid(member.Gender.Value);
        }
        #endregion
    }
}
