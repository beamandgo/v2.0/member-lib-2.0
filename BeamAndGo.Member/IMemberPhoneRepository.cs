﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BeamAndGo.Core.Data;

namespace BeamAndGo.Member
{
    public interface IMemberPhoneRepository : IRepository<MemberPhone, int>
    {
        Task<MemberPhone> GetBySid(string sid);
        Task<MemberPhone> GetByCallingCodeAndPhone(string callingCode, string phone);
        Task<MemberPhone> GetDefaultByMemberId(int memberId);
        IAsyncEnumerable<MemberPhone> ListByMemberId(int memberId);
    }
}
