﻿using System;
using BeamAndGo.Core.Data;

namespace BeamAndGo.Member
{
    public class MemberAddress : BaseEntityWithSid<int>
    {
        public virtual int MemberId { get; set; }

        public virtual string Name { get; set; }
        public virtual string Street { get; set; }
        public virtual string PostalCode { get; set; }
        public virtual string CountryCode { get; set; }
        public virtual string Remarks { get; set; }

        public virtual bool IsDefault { get; set; }

        public virtual bool IsVerified { get; set; }
        public virtual DateTimeOffset? VerifiedAt { get; set; }
        public virtual string VerificationMeta { get; set; }
    }
}
