﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BeamAndGo.Core.Data;

namespace BeamAndGo.Member
{
    public interface IMemberPreferenceTypeRepository : IRepository<MemberPreferenceType, int>
    {
        Task<IEnumerable<MemberPreferenceType>> ListAllActiveSorted();
    }
}
