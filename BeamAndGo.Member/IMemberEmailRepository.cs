﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BeamAndGo.Core.Data;

namespace BeamAndGo.Member
{
    public interface IMemberEmailRepository : IRepository<MemberEmail, int>
    {
        Task<MemberEmail> GetBySid(string sid);
        Task<MemberEmail> GetByEmail(string email);
        Task<MemberEmail> GetDefaultByMemberId(int memberId);
        IAsyncEnumerable<MemberEmail> ListByMemberId(int memberId);
    }
}
