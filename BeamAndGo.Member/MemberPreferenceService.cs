﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Transactions;

namespace BeamAndGo.Member
{
    public class MemberPreferenceService : IMemberPreferenceService
    {
        private readonly IMemberRepositoryManager _repositoryManager;
        private readonly IMemberPreferenceTypeService _preferenceTypeService;
        private readonly IMemberService _memberService;
        private readonly IMemberLogService _logService;

        public MemberPreferenceService(
            IMemberRepositoryManager repositoryManager,
            IMemberPreferenceTypeService preferenceTypeService,
            IMemberService memberService,
            IMemberLogService logService)
        {
            _repositoryManager = repositoryManager;
            _preferenceTypeService = preferenceTypeService;
            _memberService = memberService;
            _logService = logService;
        }

        public virtual async Task<MemberPreference> GetBySid(string sid)
        {
            if (string.IsNullOrWhiteSpace(sid)) throw MemberErrors.PreferenceSidRequired();
            var memberPreference = await _repositoryManager.MemberPreferenceRepository.GetBySid(sid);
            if (memberPreference == null) throw MemberErrors.PreferenceNotFound(sid);
            return memberPreference;
        }

        public virtual async Task<MemberPreference> CreateOrUpdate(string memberSid, int preferenceTypeId, string value)
        {
            if (string.IsNullOrEmpty(value)) throw MemberErrors.PreferenceValueRequired();

            // GetBySid will validate Sid
            var member = await _memberService.GetBySid(memberSid);

            var memberPreferenceType = await _repositoryManager.MemberPreferenceTypeRepository.GetById(preferenceTypeId);
            if (memberPreferenceType == null) throw MemberErrors.PreferenceTypeInvalid(preferenceTypeId);

            var memberPreference = await _repositoryManager.MemberPreferenceRepository.GetByMemberIdAndPreferenceTypeId(member.Id, preferenceTypeId);

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                // Create if it doesn't exist
                if (memberPreference == null)
                {
                    memberPreference = new MemberPreference()
                    {
                        MemberId = member.Id,
                        PreferenceTypeId = preferenceTypeId,
                        Value = value
                    };

                    await _repositoryManager.MemberPreferenceRepository.Create(memberPreference);
                }

                // Otherwise update
                else
                {
                    memberPreference.Value = value;
                    await _repositoryManager.MemberPreferenceRepository.Update(memberPreference);
                }

                await _logService.Log(member.Id, new object[] { "CreateOrUpdatePreference", memberSid, preferenceTypeId, value });
                await _repositoryManager.SaveChanges();
                scope.Complete();
            }

            return memberPreference;
        }

        public virtual async Task Delete(string sid)
        {
            // GetBySid will validate Sid
            var memberPreference = await GetBySid(sid);

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                await _repositoryManager.MemberPreferenceRepository.Delete(memberPreference);
                await _repositoryManager.SaveChanges();
                await _logService.Log(memberPreference.MemberId, new object[] { "DeletePreference", sid });
                scope.Complete();
            }
        }

        public virtual async IAsyncEnumerable<MemberPreference> ListByMemberSid(string memberSid)
        {
            // GetBySid will validate Sid
            var member = await _memberService.GetBySid(memberSid);
            var memberPreferences = _repositoryManager.MemberPreferenceRepository.ListByMemberId(member.Id);
            await foreach (var p in memberPreferences) yield return p;
        }

        public virtual async IAsyncEnumerable<MemberPreference> ListWithDefaultByMemberSid(string memberSid)
        {
            // GetBySid will validate Sid
            var member = await _memberService.GetBySid(memberSid);

            var memberPreferences = new Dictionary<int, MemberPreference>();
            await foreach (var p in _repositoryManager.MemberPreferenceRepository.ListByMemberId(member.Id))
                memberPreferences.Add(p.PreferenceTypeId, p);

            foreach (var p in await _preferenceTypeService.ListAllActiveSorted())
            {
                if (memberPreferences.ContainsKey(p.Id))
                {
                    yield return memberPreferences[p.Id];
                }
                else
                {
                    yield return new MemberPreference
                    {
                        MemberId = member.Id,
                        PreferenceTypeId = p.Id,
                        Value = p.DefaultValue,
                        PreferenceType = p,
                    };
                }
            }
        }
    }
}
