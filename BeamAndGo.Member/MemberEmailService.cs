﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Transactions;
using BeamAndGo.Core.Common;

namespace BeamAndGo.Member
{
    public class MemberEmailService : IMemberEmailService
    {
        private readonly IMemberRepositoryManager _repositoryManager;
        private readonly IMemberService _memberService;
        private readonly IMemberLogService _logService;

        public MemberEmailService(
            IMemberRepositoryManager repositoryManager,
            IMemberService memberService,
            IMemberLogService logService)
        {
            _repositoryManager = repositoryManager;
            _memberService = memberService;
            _logService = logService;
        }

        public virtual async Task<MemberEmail> GetBySid(string sid)
        {
            if (string.IsNullOrWhiteSpace(sid)) throw MemberErrors.EmailSidRequired();
            var email = await _repositoryManager.MemberEmailRepository.GetBySid(sid);
            if (email == null) throw MemberErrors.EmailNotFound(sid);
            return email;
        }

        public virtual async Task<MemberEmail> Create(string memberSid, string email, string identifier = null)
        {
            if (string.IsNullOrWhiteSpace(email)) throw MemberErrors.EmailRequired();
            if (!email.IsValidEmail()) throw MemberErrors.EmailInvalid(email);

            // GetBySid will validate Sid
            var member = await _memberService.GetBySid(memberSid);

            if (await Exists(email)) throw MemberErrors.EmailExists(email);

            var memberEmail = new MemberEmail
            {
                MemberId = member.Id,
                Email = email,
                Identifier = identifier,
                IsVerified = false
            };

            // Check if a default exists
            var defaultEmail = await _repositoryManager.MemberEmailRepository.GetDefaultByMemberId(member.Id);
            memberEmail.IsDefault = (defaultEmail == null);

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                await _repositoryManager.MemberEmailRepository.Create(memberEmail);
                await _repositoryManager.SaveChanges();
                await _logService.Log(member.Id, new object[] { "CreateEmail", memberSid, email, identifier });
                scope.Complete();
            }

            return memberEmail;
        }

        public virtual async Task<MemberEmail> Update(string sid, string email, string identifier = null)
        {
            if (string.IsNullOrWhiteSpace(email)) throw MemberErrors.EmailRequired();
            if (!email.IsValidEmail()) throw MemberErrors.EmailInvalid(email);

            // GetBySid will validate Sid
            var memberEmail = await GetBySid(sid);

            // Check if email address in use if it was changed
            if (!memberEmail.Email.Equals(email, StringComparison.OrdinalIgnoreCase) && await Exists(email))
                throw MemberErrors.EmailExists(email);

            memberEmail.Email = email;
            memberEmail.Identifier = identifier;

            // When an email is updated, it loses its verified status
            memberEmail.IsVerified = false;
            memberEmail.VerifiedAt = null;
            memberEmail.VerificationMeta = null;

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                await _repositoryManager.MemberEmailRepository.Update(memberEmail);
                await _repositoryManager.SaveChanges();
                await _logService.Log(memberEmail.MemberId, new object[] { "UpdateEmail", sid, email, identifier });
                scope.Complete();
            }

            return memberEmail;
        }

        public virtual async Task Delete(string sid)
        {
            // GetBySid will validate Sid
            var memberEmail = await GetBySid(sid);

            // Can't delete the default email
            if (memberEmail.IsDefault) throw MemberErrors.DeleteDefaultEmailNotAllowed(memberEmail.Email);

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                await _repositoryManager.MemberEmailRepository.Delete(memberEmail);
                await _repositoryManager.SaveChanges();
                await _logService.Log(memberEmail.MemberId, new object[] { "DeleteEmail", sid });
                scope.Complete();
            }
        }

        public virtual async Task Verify(string sid, string meta)
        {
            // GetBySid will validate Sid
            var memberEmail = await GetBySid(sid);

            if (memberEmail.IsVerified) throw MemberErrors.EmailVerificationExists(sid);

            memberEmail.IsVerified = true;
            memberEmail.VerifiedAt = DateTimeOffset.UtcNow;
            memberEmail.VerificationMeta = meta;

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                await _repositoryManager.MemberEmailRepository.Update(memberEmail);
                await _repositoryManager.SaveChanges();
                await _logService.Log(memberEmail.MemberId, new object[] { "VerifyEmail", sid, meta });
                scope.Complete();
            }
        }

        public virtual async Task SetDefault(string sid)
        {
            // GetBySid will validate Sid
            var memberEmail = await GetBySid(sid);            

            // Do not allow making an unverified email as default
            // if current default email is already verified
            if (!memberEmail.IsVerified)
            {
                var defaultEmail = await _repositoryManager.MemberEmailRepository.GetDefaultByMemberId(memberEmail.MemberId);
                if (defaultEmail.IsVerified) throw MemberErrors.UnverifiedDefaultEmailNotAllowed(memberEmail.Email);
            }

            memberEmail.IsDefault = true;

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                await _repositoryManager.MemberEmailRepository.Update(memberEmail);

                // Loop through all other emails and remove default
                var emails = _repositoryManager.MemberEmailRepository.ListByMemberId(memberEmail.MemberId);
                await foreach (var email in emails)
                {
                    if (memberEmail.Id != email.Id && email.IsDefault)
                    {
                        email.IsDefault = false;
                        await _repositoryManager.MemberEmailRepository.Update(email);
                    }
                }

                await _repositoryManager.SaveChanges();
                await _logService.Log(memberEmail.MemberId, new object[] { "SetDefaultEmail", sid });
                scope.Complete();
            }
        }

        public virtual async IAsyncEnumerable<MemberEmail> ListByMemberSid(string memberSid)
        {
            // GetBySid will validate Sid
            var member = await _memberService.GetBySid(memberSid, false);
            await foreach (var e in _repositoryManager.MemberEmailRepository.ListByMemberId(member.Id))
                yield return e;
        }

        public async Task<bool> Exists(string email)
        {
            return null != await _repositoryManager.MemberEmailRepository.GetByEmail(email);
        }
    }
}
