﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Transactions;

namespace BeamAndGo.Member
{
    public class MemberReferralService : IMemberReferralService
    {
        private readonly IMemberRepositoryManager _repositoryManager;
        private readonly IMemberService _memberService;
        private readonly IMemberLogService _logService;

        public MemberReferralService(
            IMemberRepositoryManager repositoryManager,
            IMemberService memberService,
            IMemberLogService logService)
        {
            _repositoryManager = repositoryManager;
            _memberService = memberService;
            _logService = logService;
        }

        public virtual async Task<MemberReferral> Register(string referredMemberSid, string referralCode, MemberReferralType referralType)
        {
            if (string.IsNullOrWhiteSpace(referralCode)) throw MemberErrors.ReferralCodeRequired();

            var referred = await _memberService.GetBySid(referredMemberSid);

            var referrer = await _repositoryManager.MemberRepository.GetByReferralCode(referralCode);
            if (referrer == null) throw MemberErrors.ReferralCodeInvalid(referralCode);

            await foreach (var _ in _repositoryManager.MemberReferralRepository.ListByReferredMemberIdAndReferralType(referred.Id, referralType))
                throw MemberErrors.ReferrerExists(referred.Sid, referralType);

            var referral = new MemberReferral
            {
                ReferredMemberId = referred.Id,
                ReferrerMemberId = referrer.Id,
                ReferralType = referralType,
            };

            using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                await _repositoryManager.MemberReferralRepository.Create(referral);
                await _repositoryManager.SaveChanges();
                await _logService.Log(referred.Id, new object[] { "RegisterReferrer", referredMemberSid, referralCode, referralType });
                scope.Complete();
            }

            return referral;
        }

        public async IAsyncEnumerable<MemberReferral> ListByReferredMemberSid(string memberSid)
        {
            var member = await _memberService.GetBySid(memberSid);
            await foreach (var r in _repositoryManager.MemberReferralRepository.ListByReferredMemberId(member.Id))
                yield return r;
        }

        public async IAsyncEnumerable<MemberReferral> ListByReferredMemberSidAndReferralType(string memberSid, MemberReferralType referralType)
        {
            var member = await _memberService.GetBySid(memberSid);
            await foreach (var r in _repositoryManager.MemberReferralRepository.ListByReferredMemberIdAndReferralType(member.Id, referralType))
                yield return r;
        }

        public async IAsyncEnumerable<MemberReferral> ListByReferrerMemberSid(string memberSid)
        {
            var member = await _memberService.GetBySid(memberSid);
            await foreach (var r in _repositoryManager.MemberReferralRepository.ListByReferrerMemberId(member.Id))
                yield return r;
        }

        public async IAsyncEnumerable<MemberReferral> ListByReferrerMemberSidAndReferralType(string memberSid, MemberReferralType referralType)
        {
            var member = await _memberService.GetBySid(memberSid);
            await foreach (var r in _repositoryManager.MemberReferralRepository.ListByReferrerMemberIdAndReferralType(member.Id, referralType))
                yield return r;
        }
    }
}
