﻿using System.ComponentModel.DataAnnotations.Schema;
using BeamAndGo.Core.Data;

namespace BeamAndGo.Member
{
    [Table("LK_RelationshipTypes")]
    public class MemberRelationshipType : BaseLookup
    {
        public virtual string Name { get; set; }
        public virtual char? Gender { get; set; }
    }
}
