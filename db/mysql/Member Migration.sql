BEGIN;

DROP TEMPORARY TABLE IF EXISTS RemitroMain.tmpMembers;

CREATE TEMPORARY TABLE RemitroMain.tmpMembers (
	`Sid` VARCHAR(36) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	UNIQUE INDEX `Sid` (`Sid`) USING BTREE
)
COLLATE='utf8mb4_unicode_ci'
ENGINE=INNODB;

INSERT INTO RemitroMain.tmpMembers 
SELECT DISTINCT
m.Sid
FROM RemitroMain.Members m
JOIN RemitroMain.LK_Countries c ON m.LK_CountryId = c.Id
WHERE m.Id > 0
AND m.LK_MemberStatusId IN (300, 301)
AND m.LK_RecordStatusId = 1
AND NOT IFNULL(m.MiddleName,'') = 'QA-TEST-DATA-ONLY'
AND NOT (m.Email NOT REGEXP '^[^@]+@[^@]+\.[^@]{2,}$' AND m.Mobile NOT REGEXP '^[0-9]+$')
AND NOT EXISTS(SELECT mm.Sid FROM Member.Members mm WHERE mm.Sid = m.Sid LIMIT 1);

INSERT INTO Member.Members
SELECT DISTINCT
NULL AS Id,
m.Sid,
m.FirstName,
m.MiddleName,
m.LastName,
CONCAT (m.FirstName, ' ', m.LastName) AS DisplayName,
null AS LK_GenderTypeId,
IF(m.BirthDate IS NULL OR YEAR(m.BirthDate) < 1900, null, YEAR(m.BirthDate)) AS BirthYear,
IF(m.BirthDate IS NULL,null,Month(m.BirthDate)) AS BirthMonth,
IF(m.BirthDate IS NULL,null,Day(m.BirthDate)) AS BirthDay,
c.ISOCode AS CountryCode,
concat(m.Id,'01',LEFT(m.firstName, 1),LEFT(m.lastName, 1)) AS ReferralCode,
NULL AS `Source`,
NULL AS IPAddress,
m.CreateDate,
m.UpdateDate,
m.LK_RecordStatusId
FROM RemitroMain.Members m
JOIN RemitroMain.tmpMembers tm ON m.Sid = tm.Sid
JOIN RemitroMain.LK_Countries c ON m.LK_CountryId = c.Id;


INSERT INTO Member.MemberEmails
SELECT
NULL AS ID,
UUID() AS Sid,
m.Id AS MemberId,
rm.Email,
NULL AS Identifier,
1 AS IsDefault,
0 AS IsVerified,
NULL AS VerifiedAt,
NULL AS VerificationMeta,
m.CreateDate,
m.UpdateDate,
m.LK_RecordStatusId
FROM Member.Members m
INNER JOIN RemitroMain.Members rm ON rm.Sid = m.Sid
INNER JOIN RemitroMain.tmpMembers tm ON m.Sid = tm.Sid
WHERE NOT (rm.Email is null or rm.Email = '');

INSERT INTO Member.MemberPhones
SELECT
NULL AS Id,
UUID() AS Sid,
m.Id AS MemberId,
c.Code AS CallingCode,
rm.Mobile AS PhoneNumber,
NULL AS Identifier,
1 AS IsDefault,
rm.VerifiedMobile AS IsVerified,
NULL VerifiedAt,
NULL VerificationMeta,
m.CreateDate,
m.UpdateDate,
m.LK_RecordStatusId
FROM Member.Members m
INNER JOIN RemitroMain.Members rm ON rm.Sid = m.Sid
INNER JOIN RemitroMain.tmpMembers tm ON m.Sid = tm.Sid
INNER JOIN RemitroMain.LK_Countries c ON rm.LK_CountryId = c.Id
WHERE NOT (rm.Mobile is null or rm.Mobile = '');


INSERT INTO Member.MemberAddresses
SELECT
NULL AS Id,
rma.Sid,
m.Id AS MemberId,
rma.Name As Name,
CONCAT(rm.Address1,
IF((lkc.Id IS NULL OR lkc.Id = -1),'',CONCAT(', ' ,lkc.Name)),  
IF((lkp.Id IS NULL OR lkp.Id = -1),'',CONCAT(', ' ,lkp.Name))
) AS Street,
IFNULL(rm.PostalCode,'') PostalCode,
c.ISOCode AS CountryCode,
rma.IsDefault,
0 AS IsVerified,
NULL AS VerifiedAt,
NULL AS VerificationMeta,
NULL AS Remarks,
rma.CreateDate,
rma.UpdateDate,
rma.LK_RecordStatusId
FROM Member.Members m
INNER JOIN RemitroMain.Members rm ON rm.Sid = m.Sid
INNER JOIN RemitroMain.tmpMembers tm ON m.Sid = tm.Sid
INNER JOIN RemitroMain.MemberAddresses rma ON rma.MemberId = rm.Id
LEFT JOIN RemitroMain.LK_Countries c ON rma.LK_CountryId = c.Id
LEFT JOIN RemitroMain.LK_Cities lkc ON rma.LK_CityId = lkc.Id
LEFT JOIN RemitroMain.LK_Provinces lkp ON rma.LK_ProvinceId = lkp.Id
WHERE NOT rma.Address1 = ""
AND rma.Address1 is not NULL;

INSERT INTO Member.MemberReferrals
SELECT
NULL AS ID,
UUID() AS Sid,
m.Id AS ReferredMemberId,
replace(mc.Code,'MR','') AS ReferrerMemberId,
1 AS LK_ReferralTypeId,
m.CreateDate,
m.UpdateDate,
m.LK_RecordStatusID
FROM Member.Members m
INNER JOIN RemitroMain.Members rm ON rm.Sid = m.Sid
INNER JOIN RemitroMain.tmpMembers tm ON m.Sid = tm.Sid
INNER JOIN RemitroMain.MemberCodes mc ON mc.MemberId = rm.Id
WHERE mc.Code LIKE 'MR%';

DROP TEMPORARY TABLE IF EXISTS RemitroMain.tmpMembers;

COMMIT;



