USE Member;

-- Table structure for table `Members`
-- Master table for all Member records

DROP TABLE IF EXISTS `LK_PreferenceTypes`;
CREATE TABLE `LK_PreferenceTypes` (
	`Id` INT(11) NOT NULL AUTO_INCREMENT,
	`Name` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`Description` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`DefaultValue` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`SortOrder` INT(11) NOT NULL DEFAULT '0',
	`LK_RecordStatusId` TINYINT(1) NOT NULL,
	PRIMARY KEY (`Id`) USING BTREE
)
ENGINE=INNODB
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `LK_RelationshipTypes`;
CREATE TABLE `LK_RelationshipTypes` (
	`Id` INT(11) NOT NULL AUTO_INCREMENT,
	`Name` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`Gender` CHAR(1) NULL COLLATE 'utf8mb4_unicode_ci',
	`LK_RecordStatusId` TINYINT(1) NOT NULL,
	PRIMARY KEY (`Id`) USING BTREE
)
ENGINE=INNODB
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `Members`;
CREATE TABLE `Members` (
	`Id` INT(11) NOT NULL AUTO_INCREMENT,
	`Sid` VARCHAR(36) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`FirstName` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`MiddleName` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`LastName` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`DisplayName` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`Gender` VARCHAR(1) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`BirthYear` INT(11) NULL DEFAULT NULL,
	`BirthMonth` INT(11) NULL DEFAULT NULL,
	`BirthDay` INT(11) NULL DEFAULT NULL,
	`CountryCode` CHAR(2) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`ReferralCode` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`Source` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`IPAddress` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`CreateDate` DATETIME NOT NULL,
	`UpdateDate` DATETIME NULL DEFAULT NULL,
	`LK_RecordStatusId` TINYINT(1) NOT NULL,
	PRIMARY KEY (`Id`) USING BTREE,
	UNIQUE INDEX `Sid` (`Sid`) USING BTREE,
	INDEX `FirstName` (`FirstName`) USING BTREE,
	INDEX `LastName` (`LastName`) USING BTREE,
	INDEX `CountryCode` (`CountryCode`) USING BTREE
)
ENGINE=INNODB
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `MemberAddresses`;
CREATE TABLE `MemberAddresses` (
	`Id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`Sid` VARCHAR(36) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`MemberId` INT(11) NOT NULL,
	`Name` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`Street` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`PostalCode` VARCHAR(16) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`CountryCode` CHAR(2) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`IsDefault` TINYINT(1) NULL DEFAULT '0',
	`IsVerified` TINYINT(1) NULL DEFAULT '0',
	`VerifiedAt` DATETIME NULL DEFAULT NULL,
	`VerificationMeta` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`Remarks` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`CreateDate` DATETIME NOT NULL,
	`UpdateDate` DATETIME NULL DEFAULT NULL,
	`LK_RecordStatusId` TINYINT(1) NOT NULL,
	PRIMARY KEY (`Id`) USING BTREE,
	UNIQUE INDEX `Sid` (`Sid`) USING BTREE,
	INDEX `MemberId` (`MemberId`) USING BTREE,
	CONSTRAINT `MemberAddresses_FK_MemberId` FOREIGN KEY (`MemberId`) REFERENCES `Members` (`Id`) ON UPDATE RESTRICT ON DELETE RESTRICT
)
ENGINE=INNODB
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `MemberEmails`;
CREATE TABLE `MemberEmails` (
	`Id` INT(11) NOT NULL AUTO_INCREMENT,
	`Sid` VARCHAR(36) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`MemberId` INT(11) NOT NULL,
	`Email` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`Identifier` VARCHAR(20) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`IsDefault` TINYINT(1) NULL DEFAULT '0',
	`IsVerified` TINYINT(1) NULL DEFAULT '0',
	`VerifiedAt` DATETIME NULL DEFAULT NULL,
	`VerificationMeta` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`CreateDate` DATETIME NOT NULL,
	`UpdateDate` DATETIME NULL DEFAULT NULL,
	`LK_RecordStatusId` TINYINT(1) NOT NULL,
	PRIMARY KEY (`Id`) USING BTREE,
	UNIQUE INDEX `Sid` (`Sid`) USING BTREE,
	INDEX `MemberId` (`MemberId`) USING BTREE,
	INDEX `Email` (`Email`) USING BTREE,
	CONSTRAINT `MemberEmails_FK_MemberId` FOREIGN KEY (`MemberId`) REFERENCES `Members` (`Id`) ON UPDATE RESTRICT ON DELETE RESTRICT
)
ENGINE=INNODB
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `MemberLogs`;
CREATE TABLE `MemberLogs` (
	`Id` INT(11) NOT NULL AUTO_INCREMENT,
	`MemberId` INT(11) NOT NULL,
	`Meta` TEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`Notes` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`CreateDate` DATETIME NOT NULL,
	`UpdateDate` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`Id`) USING BTREE,
	UNIQUE INDEX `Id` (`Id`, `MemberId`) USING BTREE,
	INDEX `MemberId` (`MemberId`) USING BTREE
)
ENGINE=INNODB
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `MemberPhones`;
CREATE TABLE `MemberPhones` (
	`Id` INT(11) NOT NULL AUTO_INCREMENT,
	`Sid` VARCHAR(36) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`MemberId` INT(11) NOT NULL,
	`CallingCode` VARCHAR(3) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`PhoneNumber` VARCHAR(32) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`Identifier` VARCHAR(20) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`IsDefault` TINYINT(1) NULL DEFAULT '0',
	`IsVerified` TINYINT(1) NULL DEFAULT '0',
	`VerifiedAt` DATETIME NULL DEFAULT NULL,
	`VerificationMeta` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`CreateDate` DATETIME NOT NULL,
	`UpdateDate` DATETIME NULL DEFAULT NULL,
	`LK_RecordStatusId` TINYINT(1) NOT NULL,
	PRIMARY KEY (`Id`) USING BTREE,
	UNIQUE INDEX `Sid` (`Sid`) USING BTREE,
	INDEX `MemberId` (`MemberId`) USING BTREE,
	INDEX `CallingCode` (`CallingCode`) USING BTREE,
	INDEX `PhoneNumber` (`PhoneNumber`) USING BTREE,
	CONSTRAINT `MemberPhones_FK_MemberId` FOREIGN KEY (`MemberId`) REFERENCES `Members` (`Id`) ON UPDATE RESTRICT ON DELETE RESTRICT
)
ENGINE=INNODB
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `MemberPreferences`;
CREATE TABLE `MemberPreferences` (
	`Id` INT(11) NOT NULL AUTO_INCREMENT,
	`Sid` VARCHAR(36) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`MemberId` INT(11) NOT NULL,
	`LK_PreferenceTypeId` INT(11) NOT NULL,
	`Value` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`CreateDate` DATETIME NOT NULL,
	`UpdateDate` DATETIME NULL DEFAULT NULL,
	`LK_RecordStatusId` TINYINT(1) NOT NULL,
	PRIMARY KEY (`Id`) USING BTREE,
	UNIQUE INDEX `Sid` (`Sid`) USING BTREE,
	UNIQUE INDEX `Id` (`MemberId`, `LK_PreferenceTypeId`) USING BTREE,
	INDEX `MemberId` (`MemberId`) USING BTREE,
	INDEX `LK_PreferenceTypeId` (`LK_PreferenceTypeId`) USING BTREE,
	CONSTRAINT `MemberPreferences_FK_MemberId` FOREIGN KEY (`MemberId`) REFERENCES `Members` (`Id`) ON UPDATE RESTRICT ON DELETE RESTRICT
)
ENGINE=INNODB
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `MemberReferrals`;
CREATE TABLE `MemberReferrals` (
	`Id` INT(11) NOT NULL AUTO_INCREMENT,
	`Sid` VARCHAR(36) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`ReferredMemberId` INT(11) NOT NULL,
	`ReferrerMemberId` INT(11) NULL DEFAULT NULL,
	`LK_ReferralTypeId` INT(11) NOT NULL,
	`CreateDate` DATETIME NOT NULL,
	`UpdateDate` DATETIME NULL DEFAULT NULL,
	`LK_RecordStatusId` TINYINT(1) NOT NULL,
	PRIMARY KEY (`Id`) USING BTREE,
	UNIQUE INDEX `Sid` (`Sid`) USING BTREE,
	INDEX `ReferredMemberId` (`ReferredMemberId`) USING BTREE,
	INDEX `ReferrerMemberId` (`ReferrerMemberId`) USING BTREE,
	CONSTRAINT `MemberReferrals_FK_ReferredMemberId` FOREIGN KEY (`ReferredMemberId`) REFERENCES `Members` (`Id`) ON UPDATE RESTRICT ON DELETE RESTRICT,
	CONSTRAINT `MemberReferrals_FK_ReferrerMemberId` FOREIGN KEY (`ReferrerMemberId`) REFERENCES `Members` (`Id`) ON UPDATE RESTRICT ON DELETE RESTRICT
)
ENGINE=INNODB
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `MemberRelationships`;
CREATE TABLE `MemberRelationships` (
	`Id` INT(11) NOT NULL AUTO_INCREMENT,
	`Sid` VARCHAR(36) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`MemberId` INT(11) NOT NULL,
	`LK_RelationshipTypeId` INT(11) NOT NULL,
	`FirstName` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`MiddleName` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`LastName` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`Gender` CHAR(1) NULL COLLATE 'utf8mb4_unicode_ci',
	`BirthYear` INT(11) NULL DEFAULT NULL,
	`BirthMonth` INT(11) NULL DEFAULT NULL,
	`BirthDay` INT(11) NULL DEFAULT NULL,
	`CallingCode` VARCHAR(3) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`PhoneNumber` VARCHAR(32) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`Email` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`Street` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`PostalCode` VARCHAR(10) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`CountryCode` CHAR(2) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`CreateDate` DATETIME NOT NULL,
	`UpdateDate` DATETIME NULL DEFAULT NULL,
	`LK_RecordStatusId` TINYINT(1) NOT NULL,
	PRIMARY KEY (`Id`) USING BTREE,
	UNIQUE INDEX `Sid` (`Sid`) USING BTREE,
	INDEX `MemberId` (`MemberId`) USING BTREE,
	INDEX `LK_RelationshipTypeId` (`LK_RelationshipTypeId`) USING BTREE,
	CONSTRAINT `MemberRelationships_FK_MemberId` FOREIGN KEY (`MemberId`) REFERENCES `Members` (`Id`) ON UPDATE RESTRICT ON DELETE RESTRICT,
	CONSTRAINT `MemberRelationships_FK_RelationshipId` FOREIGN KEY (`LK_RelationshipTypeId`) REFERENCES `Member`.`LK_RelationshipTypes` (`Id`) ON UPDATE RESTRICT ON DELETE RESTRICT	
)
ENGINE=INNODB
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- Member Admin -------

DROP TABLE IF EXISTS `MemberAdmins`;
CREATE TABLE `MemberAdmins` (
	`Id` INT(11) NOT NULL AUTO_INCREMENT,
	`Sid` VARCHAR(36) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`MemberId` INT(11) NOT NULL,
	`LK_MemberAdminAccessLevelId` INT(11) NOT NULL,
	`Expiry` DATETIME DEFAULT NULL,
	`CreateDate` DATETIME NOT NULL,
	`UpdateDate` DATETIME NULL DEFAULT NULL,
	`LK_RecordStatusId` TINYINT(1) NOT NULL,
	PRIMARY KEY (`Id`) USING BTREE,
	UNIQUE INDEX `Sid` (`Sid`) USING BTREE,
	INDEX `MemberId` (`MemberId`) USING BTREE,
	INDEX `LK_MemberAdminAccessLevelId` (`LK_MemberAdminAccessLevelId`) USING BTREE
)
ENGINE=INNODB
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `MemberAdminLogs`;
CREATE TABLE `MemberAdminLogs` (
	`Id` INT(11) NOT NULL AUTO_INCREMENT,
	`AdminId` INT(11) NOT NULL,
	`Meta` TEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`Notes` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`CreateDate` DATETIME NOT NULL,
	`UpdateDate` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`Id`) USING BTREE,
	UNIQUE INDEX `Id` (`Id`, `AdminId`) USING BTREE,
	INDEX `AdminId` (`AdminId`) USING BTREE
)
ENGINE=INNODB
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
