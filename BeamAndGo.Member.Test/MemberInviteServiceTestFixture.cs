﻿using BeamAndGo.Member.Data;
using BeamAndGo.Member.Invite;
using BeamAndGo.Member.Messaging;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeamAndGo.Member.Test
{
    public class MemberInviteServiceTestFixture : IDisposable
    {
        public IMemberRepositoryManager repositoryManager;
        public IMemberLogService memberLogService;
        public IMemberSearch memberSearch;
        public IMemberService memberService;

        public IMemberEmailInviteFormatter emailInviteFormatter;
        public IMemberMailMessagingProvider mailMessagingProvider;
        public IMemberEmailService emailService;
        public IMemberEmailInviteService emailInviteService;

        public IMemberPhoneInviteFormatter phoneInviteFormatter;
        public IMemberTextMessagingProvider textMessagingProvider;
        public IMemberPhoneService phoneService;
        public IMemberPhoneInviteService phoneInviteService;

        public MemberInviteServiceTestFixture()
        {
            var loggerFactory = LoggerFactory.Create(builder => builder.AddConsole());

            var options = new DbContextOptionsBuilder<MemberDbContext>().UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;
            var context = new MemberDbContext(options);

            var serviceOptions = Options.Create(new MemberServiceOptions());

            repositoryManager = new MemberDbRepositoryManager(context);
            memberSearch = new MemberDbSearch(context);
            memberLogService = new MemberLogService(repositoryManager);
            memberService = new MemberService(serviceOptions, repositoryManager, memberSearch, memberLogService);

            emailService = new MemberEmailService(repositoryManager, memberService, memberLogService);
            phoneService = new MemberPhoneService(repositoryManager, memberService, memberLogService);

            mailMessagingProvider = new DummyMemberMailMessagingProvider(loggerFactory.CreateLogger<DummyMemberMailMessagingProvider>());
            textMessagingProvider = new DummyMemberTextMessagingProvider(loggerFactory.CreateLogger<DummyMemberTextMessagingProvider>());

            emailInviteFormatter = new BeamAndGoMemberEmailInviteFormatter();
            phoneInviteFormatter = new BeamAndGoMemberPhoneInviteFormatter();

            emailInviteService = new MemberEmailInviteService(emailInviteFormatter, mailMessagingProvider, emailService, memberService);
            phoneInviteService = new MemberPhoneInviteService(phoneInviteFormatter, textMessagingProvider, phoneService, memberService);
        }

        public MemberInviteServiceTestFixture WithTestData()
        {
            repositoryManager.MemberRepository.Create(new Member()
            {
                Id = 1,
                Sid = "TESTMEMBER",
                FirstName = "Beam",
                LastName = "Go"
            });

            repositoryManager.SaveChanges();

            return this;
        }

        public void Dispose()
        {
            memberService = null;
            emailInviteService = null;
            phoneInviteService = null;
            repositoryManager.Dispose();
        }
    }
}
