﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BeamAndGo.Member.Test
{
    public class MemberRelationshipServiceTest
    {
        [Theory]
        [InlineData("TESTRELATIONSHIP1")]
        public async Task GetBySid_WithValidRelationshipSid_ShouldReturnMemberRelationship(string sid)
        {
            using var fixture = new MemberRelationshipServiceTestFixture().WithTestData();

            var relationship = await fixture.memberRelationshipService.GetBySid(sid);

            Assert.NotNull(relationship);
            Assert.True(relationship.Id > 0);
        }

        [Theory]
        [InlineData("INVALIDRELATIONSHIPSID")]
        [InlineData("")]
        [InlineData(null)]
        public async Task GetBySid_WithInvalidInput_ShouldThrowArgumentException(string sid)
        {
            using var fixture = new MemberRelationshipServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberRelationshipService.GetBySid(sid));
        }

        [Theory]
        [InlineData("TESTMEMBER", 1, "Nan", "Kyaw", "65", "12345678")]
        public async Task Create_WithValidInput_ShouldReturnMemberRelationship(
            string memberSid,
            int relationshipTypeId,
            string firstName,
            string lastName,
            string callingCode,
            string phoneNumber)
        {
            using var fixture = new MemberRelationshipServiceTestFixture().WithTestData();

            var request = new CreateMemberRelationshipData()
            {
                MemberSid = memberSid,
                RelationshipTypeId = relationshipTypeId,
                FirstName = firstName,
                LastName = lastName,
                CallingCode = callingCode,
                PhoneNumber = phoneNumber
            };

            var relationship = await fixture.memberRelationshipService.Create(request);

            Assert.NotNull(relationship);
            Assert.True(relationship.Id > 0);
        }

        [Theory]
        [InlineData(null, 1, "Nan", "Kyaw", "65", "12345678")]
        [InlineData("TESTMEMBER", 1, null, "Kyaw", "65", "12345678")]
        [InlineData("TESTMEMBER", 1, "Nan", null, "65", "12345678")]
        [InlineData("TESTMEMBER", 1, "Nan", "Kyaw", null, "12345678")]
        [InlineData("TESTMEMBER", 1, "Nan", "Kyaw", "65", null)]
        [InlineData("INVALIDMEMBERSID", 1, "Nan", "Kyaw", "65", "12345678")]
        [InlineData("TESTMEMBER", 5, "Nan", "Kyaw", "65", "12345678")]

        public async Task Create_WithInvalidNames_ShouldThrowArgumentException(
            string memberSid,
            int relationshipTypeId,
            string firstName,
            string lastName,
            string callingCode,
            string phoneNumber)
        {
            using var fixture = new MemberRelationshipServiceTestFixture().WithTestData();

            var request = new CreateMemberRelationshipData()
            {
                MemberSid = memberSid,
                RelationshipTypeId = relationshipTypeId,
                FirstName = firstName,
                LastName = lastName,
                CallingCode = callingCode,
                PhoneNumber = phoneNumber
            };

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberRelationshipService.Create(request));
        }

        [Theory]
        [InlineData("TESTRELATIONSHIP1", 2, "Beam", "Go", "65", "11111111")]
        [InlineData("TESTRELATIONSHIP1", 1, "test", "test", "65", "12345678")]
        public async Task Update_WithValidValues_ShouldReturnMemberRelationship(
            string sid,
            int relationshipTypeId,
            string firstName,
            string lastName,
            string callingCode,
            string phoneNumber)
        {
            using var fixture = new MemberRelationshipServiceTestFixture().WithTestData();

            var request = new UpdateMemberRelationshipData()
            {
                RelationshipTypeId = relationshipTypeId,
                FirstName = firstName,
                LastName = lastName,
                CallingCode = callingCode,
                PhoneNumber = phoneNumber
            };

            var relationship = await fixture.memberRelationshipService.Update(sid, request);

            Assert.NotNull(relationship);
            Assert.True(relationship.Id > 0);
        }

        [Theory]
        [InlineData(null, 1, "Beam", "Go", "65", "11111111")]
        [InlineData("TESTRELATIONSHIP1", 1, null, "Go", "65", "11111111")]
        [InlineData("TESTRELATIONSHIP1", 1, "Beam", null, "65", "11111111")]
        [InlineData("TESTRELATIONSHIP1", 1, "Beam", "Go", null, "11111111")]
        [InlineData("TESTRELATIONSHIP1", 1, "Beam", "Go", "65", null)]
        [InlineData("TESTRELATIONSHIP1", 1, "Beam", "Go", "65", "8iek09-38")]
        [InlineData("INVALIDRELATIONSHIPSID", 1, "Beam", "Go", "65", "11111111")]
        [InlineData("TESTRELATIONSHIP1", 5, "Beam", "Go", "65", "11111111")]
        public async Task Update_WithInvalidValues_ShouldThrowArgumentException(
            string sid,
            int relationshipTypeId,
            string firstName,
            string lastName,
            string callingCode,
            string phoneNumber)
        {
            using var fixture = new MemberRelationshipServiceTestFixture().WithTestData();

            var request = new UpdateMemberRelationshipData()
            {
                RelationshipTypeId = relationshipTypeId,
                FirstName = firstName,
                LastName = lastName,
                CallingCode = callingCode,
                PhoneNumber = phoneNumber
            };

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberRelationshipService.Update(sid, request));
        }

        [Theory]
        [InlineData("TESTRELATIONSHIP1")]
        public async Task Delete_WithValidValues_ShouldReturnNothing(string sid)
        {
            using var fixture = new MemberRelationshipServiceTestFixture().WithTestData();

            await fixture.memberRelationshipService.Delete(sid);

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberRelationshipService.GetBySid(sid));
        }

        [Theory]
        [InlineData("INVALIDRELATIONSHIPSID")]
        public async Task Delete_WithInvalidValues_ShouldThrowArgumentException(string sid)
        {
            using var fixture = new MemberRelationshipServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberRelationshipService.Delete(sid));
        }

        [Theory]
        [InlineData("TESTMEMBER")]
        public async Task ListByMemberSid_WithValidMemberSid_ShouldReturnListOfMemberRelationships(string memberSid)
        {
            using var fixture = new MemberRelationshipServiceTestFixture().WithTestData();

            var relationships = fixture.memberRelationshipService.ListByMemberSid(memberSid);

            Assert.True(await relationships.AnyAsync());
        }

        [Theory]
        [InlineData("INVALIDMEMBERSID")]
        [InlineData("")]
        [InlineData(null)]
        public async Task ListByMemberSid_WithInvalidMemberSid_ShouldThrowArgumentException(string memberSid)
        {
            using var fixture = new MemberRelationshipServiceTestFixture().WithTestData();

            var relationships = fixture.memberRelationshipService.ListByMemberSid(memberSid);

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await relationships.GetAsyncEnumerator().MoveNextAsync());
        }

        [Fact]
        public async Task ListAllTypes_ShouldReturnListOfRelationshipTypes()
        {
            using var fixture = new MemberRelationshipServiceTestFixture().WithTestData();

            var relationshipTypes = await fixture.memberRelationshipTypeService.ListAllActiveSortedByName();

            Assert.True(relationshipTypes.Any());
        }
    }
}
