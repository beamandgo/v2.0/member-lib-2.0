﻿using BeamAndGo.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BeamAndGo.Member.Test
{
    public class MemberServiceTest
    {
        [Theory]
        [InlineData(1)]
        public async Task GetById_WithValidInput_ShouldReturnMember(int id)
        {
            using var fixture = new MemberServiceTestFixture().WithTestData();

            var member = await fixture.memberService.GetById(id);

            Assert.NotNull(member);
            Assert.True(member.Id > 0);
        }

        [Theory]
        [InlineData(999)]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task GetById_WithInvalidInput_ShouldThrowArgumentException(int id)
        {
            using var fixture = new MemberServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberService.GetById(id));
        }

        [Theory]
        [InlineData("TESTMEMBER")]
        public async Task GetBySid_WithValidInput_ShouldReturnMember(string sid)
        {
            using var fixture = new MemberServiceTestFixture().WithTestData();

            var member = await fixture.memberService.GetBySid(sid);

            Assert.NotNull(member);
            Assert.True(member.Id > 0);
        }

        [Theory]
        [InlineData("INVALIDMEMBER")]
        [InlineData("")]
        [InlineData(null)]
        public async Task GetBySid_WithInvalidInput_ShouldThrowArgumentException(string sid)
        {
            using var fixture = new MemberServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberService.GetBySid(sid));
        }

        [Theory]
        [InlineData("11111111")]
        public async Task GetByPhone_WithValidNumber_ShouldReturnMember(string phone)
        {
            using var fixture = new MemberServiceTestFixture().WithTestData();

            var member = await fixture.memberService.GetByPhone(phone);

            Assert.NotNull(member);
            Assert.True(member.Id > 0);
        }

        [Theory]
        [InlineData("x")]
        [InlineData("")]
        [InlineData(null)]
        public async Task GetByPhone_WithInvalidNumber_ShouldThrowArgumentException(string phone)
        {
            using var fixture = new MemberServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberService.GetByPhone(phone));
        }

        [Theory]
        [InlineData("65", "11111111")]
        public async Task GetByCallingCodeAndPhone_WithValidCallingCodeAndNumber_ShouldReturnMember(string callingCode, string phone)
        {
            using var fixture = new MemberServiceTestFixture().WithTestData();

            var member = await fixture.memberService.GetByPhone(callingCode, phone);

            Assert.NotNull(member);
            Assert.True(member.Id > 0);
        }

        [Theory]
        [InlineData(null, "11111111")]
        [InlineData("65", null)]
        [InlineData("65", "x")]
        public async Task GetByCallingCodeAndPhone_WithInvalidValues_ShouldThrowArgumentException(string callingCode, string phone)
        {
            using var fixture = new MemberServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberService.GetByPhone(callingCode, phone));
        }

        [Theory]
        [InlineData("test@bng.com")]
        public async Task GetByEmail_WithValidEmail_ShouldReturnMember(string email)
        {
            using var fixture = new MemberServiceTestFixture().WithTestData();

            var member = await fixture.memberService.GetByEmail(email);

            Assert.NotNull(member);
            Assert.True(member.Id > 0);
        }

        [Theory]
        [InlineData("test@test*@.com")]
        [InlineData("")]
        [InlineData(null)]
        public async Task GetByEmail_WithInvalidEmail_ShouldThrowArgumentException(string email)
        {
            using var fixture = new MemberServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberService.GetByEmail(email));
        }

        [Theory]
        [InlineData("Nan", "Kyaw", "Nan", "SG", 'F', 01, 01, 2001)]
        [InlineData("Justin", "Lee", "Justin", "SG", null, null, null, null)]
        public async Task Create_WithValidInput_ShouldReturnMember(
            string firstName,
            string lastName,
            string displayName,
            string countrycode,
            char? gender,
            int? birthDay,
            int? birthMonth,
            int? birthYear)
        {
            using var fixture = new MemberServiceTestFixture().WithTestData();

            var request = new CreateMemberData()
            {
                FirstName = firstName,
                LastName = lastName,
                DisplayName = displayName,
                CountryCode = countrycode,
                Gender = gender,
                BirthDay = birthDay,
                BirthMonth = birthMonth,
                BirthYear = birthYear
            };

            var member = await fixture.memberService.Create(request);

            Assert.NotNull(member);
            Assert.True(member.Id > 0);
        }

        [Theory]
        [InlineData(null, "Kyaw", "Nan", "SG", 'F', 01, 01, 2001)]
        [InlineData("Nan", null, "Nan", "SG", 'F', 01, 01, 2001)]
        [InlineData("Nan", "Kyaw", "Nan", null, 'F', 01, 01, 2001)]
        [InlineData("Nan", "Kyaw", "Nan", "SG", 'G', 01, 01, 2001)]
        [InlineData("Nan", "Kyaw", "Nan", "SG", 'F', 0, 0, 0)]
        public async Task Create_WithInvalidNames_ShouldThrowArgumentException(
            string firstName,
            string lastName,
            string displayName,
            string countryCode,
            char? gender,
            int? birthDay,
            int? birthMonth,
            int? birthYear)
        {
            using var fixture = new MemberServiceTestFixture();

            var request = new CreateMemberData()
            {
                FirstName = firstName,
                LastName = lastName,
                DisplayName = displayName,
                CountryCode = countryCode,
                Gender = gender,
                BirthDay = birthDay,
                BirthMonth = birthMonth,
                BirthYear = birthYear
            };

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberService.Create(request));
        }

        [Theory]
        [InlineData("TESTMEMBER", "Beam", "Go", "SG", 'F', 01, 01, 2001)]
        [InlineData("TESTMEMBER", "Beam", "Go", "SG", null, null, null, null)]
        public async Task Update_WithValidValues_ShouldReturnMember(
            string sid,
            string firstName,
            string lastName,
            string countryCode,
            char? gender,
            int? birthDay,
            int? birthMonth,
            int? birthYear)
        {
            using var fixture = new MemberServiceTestFixture().WithTestData();

            var request = new UpdateMemberData()
            {
                FirstName = firstName,
                LastName = lastName,
                CountryCode = countryCode,
                Gender = gender,
                BirthDay = birthDay,
                BirthMonth = birthMonth,
                BirthYear = birthYear
            };

            var member = await fixture.memberService.Update(sid, request);

            Assert.NotNull(member);
            Assert.True(member.Id > 0);
        }

        [Theory]
        [InlineData("INVALIDMEMBERSID", "Beam", "Go", "SG", 'F', 01, 01, 2001)]
        [InlineData(null, "Beam", "Go", "SG", 'F', 01, 01, 2001)]
        [InlineData("TESTMEMBER", null, "Go", "SG", 'F', 01, 01, 2001)]
        [InlineData("TESTMEMBER", "Beam", null, "SG", 'F', 01, 01, 2001)]
        [InlineData("TESTMEMBER", "Beam", "Go", null, 'F', 01, 01, 2001)]
        [InlineData("TESTMEMBER", "Beam", "Go", "SG", 'G', 01, 01, 2001)]
        [InlineData("TESTMEMBER", "Beam", "Go", "SG", 'F', 0, 0, 0)]
        public async Task Update_WithInvalidValues_ShouldThrowArgumentException(
            string sid,
            string firstName,
            string lastName,
            string countryCode,
            char? gender,
            int? birthDay,
            int? birthMonth,
            int? birthYear)
        {
            using var fixture = new MemberServiceTestFixture().WithTestData();

            var request = new UpdateMemberData()
            {
                FirstName = firstName,
                LastName = lastName,
                CountryCode = countryCode,
                Gender = gender,
                BirthDay = birthDay,
                BirthMonth = birthMonth,
                BirthYear = birthYear
            };

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberService.Update(sid, request));
        }

        [Theory]
        [InlineData("TESTMEMBER")]
        public async Task DisableMember_WithValidInput_ShouldReturnMember(string memberSid)
        {
            using var fixture = new MemberServiceTestFixture().WithTestData();
            var member = await fixture.memberService.Disable(memberSid);

            Assert.NotNull(member);
            Assert.True(member.RecordStatus == RecordStatus.Inactive);
        }

        [Theory]
        [InlineData("TESTMEMBER999")]
        [InlineData("")]
        [InlineData(null)]
        public async Task DisableMember_WithInalidInput_ShouldThrowArgumentException(string memberSid)
        {
            using var fixture = new MemberServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async () => await fixture.memberService.Disable(memberSid));
        }

        [Theory]
        [InlineData("TESTMEMBER")]
        public async Task EnableMember_WithValidInput_ShouldReturnMember(string memberSid)
        {
            using var fixture = new MemberServiceTestFixture().WithTestData();
            var member = await fixture.memberService.Enable(memberSid);

            Assert.NotNull(member);
            Assert.True(member.RecordStatus == RecordStatus.Active);
        }

        [Theory]
        [InlineData("TESTMEMBER999")]
        [InlineData("")]
        [InlineData(null)]
        public async Task EnableMember_WithInalidInput_ShouldThrowArgumentException(string memberSid)
        {
            using var fixture = new MemberServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async () => await fixture.memberService.Enable(memberSid));
        }

        [Fact]
        public async Task ListAllMembers_WithValidInput_ShouldReturnListOfMembers()
        {
            using var fixture = new MemberServiceTestFixture().WithTestData();
            var members = await fixture.memberService.ListAll();

            Assert.True(members.Any());
        }

        [Theory]
        [InlineData("TESTMEMBER")]
        [InlineData("Beam")]
        [InlineData("Go")]
        [InlineData("11111111")]
        [InlineData("test@bng.com")]        
        public async Task SearchMember_WithValidInput_ShouldReturnListOfMembers(string search)
        {
            using var fixture = new MemberServiceTestFixture().WithTestData();
            var members = fixture.memberService.Search(search);

            Assert.True(await members.AnyAsync());
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task SearchMember_WithInvalidInput_ShouldThrowArgumentException(string search)
        {
            using var fixture = new MemberServiceTestFixture().WithTestData();

            var members = fixture.memberService.Search(search);

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await members.GetAsyncEnumerator().MoveNextAsync());
        }

        [Theory]
        [InlineData("11111111")]
        [InlineData("test@bng.com")]
        public async Task SearchExactMember_WithValidInput_ShouldReturnListOfMembers(string search)
        {
            using var fixture = new MemberServiceTestFixture().WithTestData();
            var member = await fixture.memberService.SearchExact(search);

            Assert.NotNull(member);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task SearchExactMember_WithInvalidInput_ShouldThrowArgumentException(string search)
        {
            using var fixture = new MemberServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberService.SearchExact(search));
        }

        [Theory]
        [InlineData(1)]
        public async Task GetLog_WithValidMemberId_ShouldReturnListOfMemberLogs(int id)
        {
            using var fixture = new MemberServiceTestFixture().WithTestData();
            var memberLogs = fixture.memberLogService.ListByMemberId(id);

            Assert.True(await memberLogs.AnyAsync());
        }
    }
}
