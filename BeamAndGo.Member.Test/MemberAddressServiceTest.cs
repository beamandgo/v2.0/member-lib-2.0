﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BeamAndGo.Member.Test
{
    public class MemberAddressServiceTest
    {        
        [Theory]
        [InlineData("TESTADDRESS1")]
        public async Task GetBySid_WithValidAddressSid_ShouldReturnMemberAddress(string sid)
        {
            using var fixture = new MemberAddressServiceTestFixture().WithTestData();

            var address = await fixture.memberAddressService.GetBySid(sid);

            Assert.NotNull(address);
            Assert.True(address.Id > 0);
        }

        [Theory]
        [InlineData("INVALIDAddressSID")]
        [InlineData("")]
        [InlineData(null)]
        public async Task GetBySid_WithInvalidAddressSid_ShouldThrowArgumentException(string sid)
        {
            using var fixture = new MemberAddressServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberAddressService.GetBySid(sid));
        }

        [Theory]
        [InlineData("TESTMEMBER", "Home", "West Coast Drive", "34#fy0", "SG")]
        [InlineData("TESTMEMBER", "Home", "West Coast Drive", "123456", "SG")]
        [InlineData("TESTMEMBER", "Home", "West Coast Drive", null, "SG")]
        public async Task Create_WithValidAddress_ShouldReturnMemberAddress(string memberSid, string name, string steet, string postalCode, string country)
        {
            using var fixture = new MemberAddressServiceTestFixture().WithTestData();

            var memberAddress = await fixture.memberAddressService.Create(memberSid, name, steet, postalCode, country);

            Assert.NotNull(memberAddress);
        }

        [Theory]
        [InlineData(null, "Home", "West Coast Drive", "123456", "SG")]
        [InlineData("TESTMEMBER", null, "West Coast Drive", "123456", "SG")]
        [InlineData("TESTMEMBER", "Home", null, "123456", "SG")]
        [InlineData("TESTMEMBER", "Home", "West Coast Drive", "123456", null)]
        public async Task Create_WithInvalidValues_ShouldThrowArgumentException(string memberSid, string name, string steet, string postalCode, string country)
        {
            using var fixture = new MemberAddressServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberAddressService.Create(memberSid, name, steet, postalCode, country));
        }

        [Theory]
        [InlineData("TESTADDRESS1", "Work", "Jalan Bukit Merah", "123456", "SG")]
        [InlineData("TESTADDRESS1", "Work", "Jalan Bukit Merah", null, "SG")]
        [InlineData("TESTADDRESS1", "Work", "Jalan Bukit Merah", "89JTY0#", "SG")]
        public async Task Update_WithValidValues_ShouldReturnMemberAddress(string sid, string name, string steet, string postalCode, string country)
        {
            using var fixture = new MemberAddressServiceTestFixture().WithTestData();

            var memberAddress = await fixture.memberAddressService.Update(sid, name, steet, postalCode, country);

            Assert.NotNull(memberAddress);
        }

        [Theory]
        [InlineData(null, "Work", "Jalan Bukit Merah", "123456", "SG")]
        [InlineData("TESTADDRESS1", null, "Jalan Bukit Merah", "123456", "SG")]
        [InlineData("TESTADDRESS1", "Work", null, "123456", "SG")]
        [InlineData("INVALIDAddressSID", "Work", "Jalan Bukit Merah", "123456", "SG")]
        public async Task Update_WithInvalidValues_ShouldThrowArgumentException(string sid, string name, string steet, string postalCode, string country)
        {
            using var fixture = new MemberAddressServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberAddressService.Update(sid, name, steet, postalCode, country));
        }

        [Theory]
        [InlineData("TESTADDRESS3")]
        public async Task Delete_WithValidValues_ShouldReturnNothing(string sid)
        {
            using var fixture = new MemberAddressServiceTestFixture().WithTestData();

            await fixture.memberAddressService.Delete(sid);

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberAddressService.GetBySid(sid));
        }

        [Theory]
        [InlineData("TESTADDRESS1")]
        public async Task Delete_DefaultAddress_ShouldThrowInvalidOperationException(string sid)
        {
            using var fixture = new MemberAddressServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<InvalidOperationException>(async ()
                => await fixture.memberAddressService.Delete(sid));
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task Delete_WithInvalidAddressSid_ShouldThrowArgumentException(string sid)
        {
            using var fixture = new MemberAddressServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberAddressService.Delete(sid));
        }

        [Theory]
        [InlineData("TESTADDRESS1", "Verified by Admin")]
        public async Task Verify_WithValidValues_ShouldReturnTrue(string sid, string notes)
        {
            using var fixture = new MemberAddressServiceTestFixture().WithTestData();

            await fixture.memberAddressService.Verify(sid, notes);

            var address = await fixture.memberAddressService.GetBySid(sid);
            Assert.True(address.IsVerified);   
        }

        [Theory]
        [InlineData(null, "Verified by Admin")]
        public async Task Verify_WithInvalidValues_ShouldThrowArgumentException(string sid, string notes)
        {
            using var fixture = new MemberAddressServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberAddressService.Verify(sid, notes));
        }

        [Theory]
        [InlineData("TESTADDRESS2")]
        public async Task SetDefault_WithValidValues_ShouldReturnMemberAddress(string sid)
        {
            using var fixture = new MemberAddressServiceTestFixture().WithTestData();

            await fixture.memberAddressService.SetDefault(sid);

            var address = await fixture.memberAddressService.GetBySid(sid);
            Assert.True(address.IsDefault);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public async Task SetDefault_WithInvalidAddressSid_ShouldThrowArgumentException(string sid)
        {
            using var fixture = new MemberAddressServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberAddressService.SetDefault(sid));
        }

        [Theory]
        [InlineData("TESTMEMBER")]
        public async Task ListByMemberSid_WithValidMemberSid_ShouldReturnListOfMemberaddresses(string memberSid)
        {
            using var fixture = new MemberAddressServiceTestFixture().WithTestData();

            var addresses = fixture.memberAddressService.ListByMemberSid(memberSid);

            Assert.True(await addresses.AnyAsync());
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("INVALIDMEMBERSID")]
        public async Task ListByMemberSid_WithInvalidMemberSid_ShouldThrowArgumentException(string memberSid)
        {
            using var fixture = new MemberAddressServiceTestFixture().WithTestData();

            var addresses = fixture.memberAddressService.ListByMemberSid(memberSid);

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await addresses.GetAsyncEnumerator().MoveNextAsync());
        }
    }
}
