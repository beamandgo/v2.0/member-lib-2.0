﻿using BeamAndGo.Core.Data;
using BeamAndGo.Member.Data;
using BeamAndGo.Member.Messaging;
using BeamAndGo.Member.Verification;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeamAndGo.Member.Test
{
    public class MemberPhoneServiceTestFixture : IDisposable
    {
        public IMemberPhoneService memberPhoneService;
        public IMemberPhoneVerificationService memberPhoneVerificationService;
        public IMemberLogService memberLogService;
        public IMemberRepositoryManager memberRepositoryManager;
        public IMemberSearch memberSearch;
        public IMemberService memberService;

        public MemberPhoneServiceTestFixture()
        {
            var loggerFactory = LoggerFactory.Create(builder => builder.AddConsole());

            var options = new DbContextOptionsBuilder<MemberDbContext>().UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;
            var context = new MemberDbContext(options);

            var serviceOptions = Options.Create(new MemberServiceOptions());
            var otpProvider = new DummyMemberOtpProvider(loggerFactory.CreateLogger<DummyMemberOtpProvider>());

            memberRepositoryManager = new MemberDbRepositoryManager(context);
            
            memberSearch = new MemberDbSearch(context);
            memberLogService = new MemberLogService(memberRepositoryManager);
            memberService = new MemberService(serviceOptions, memberRepositoryManager, memberSearch, memberLogService);

            memberPhoneService = new MemberPhoneService(
                memberRepositoryManager,
                memberService,
                memberLogService
            );

            memberPhoneVerificationService = new MemberPhoneVerificationService(otpProvider, memberPhoneService);
        }

        public MemberPhoneServiceTestFixture WithTestData()
        {
            memberRepositoryManager.MemberRepository.Create(new Member()
            {
                Id = 1,
                Sid = "TESTMEMBER",
                FirstName = "Beam",
                LastName = "Go",
                RecordStatus = RecordStatus.Active
            });

            memberRepositoryManager.MemberPhoneRepository.Create(new MemberPhone()
            {
                Id = 1,
                Sid = "TESTPHONE1",
                MemberId = 1,
                CallingCode = "65",
                PhoneNumber = "11111111",
                IsDefault = true,
                IsVerified = true,
                RecordStatus = RecordStatus.Active
            });

            memberRepositoryManager.MemberPhoneRepository.Create(new MemberPhone()
            {
                Id = 2,
                Sid = "TESTPHONE2",
                MemberId = 1,
                CallingCode = "65",
                PhoneNumber = "22222222",
                IsVerified = true,
                IsDefault = false,
                RecordStatus = RecordStatus.Active
            });

            memberRepositoryManager.MemberPhoneRepository.Create(new MemberPhone()
            {
                Id = 3,
                Sid = "TESTPHONE3",
                MemberId = 1,
                CallingCode = "63",
                PhoneNumber = "55555555",
                IsVerified = false,
                IsDefault = false,
                RecordStatus = RecordStatus.Active
            }); 

            memberRepositoryManager.SaveChanges();

            return this;
        }

        public void Dispose()
        {
            memberPhoneService = null;
            memberService = null;
            memberLogService = null;
            memberSearch = null;
            memberRepositoryManager.Dispose();
        }
    }
}
