﻿using BeamAndGo.Member.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;

namespace BeamAndGo.Member.Test
{
    public class MemberReferralServiceTestFixture: IDisposable
    {
        public IMemberReferralService memberReferralService;
        public IMemberRepositoryManager repositoryManager;
        public IMemberService memberService;
        public IMemberLogService memberLogService;
        public IMemberSearch memberSearch;
        public MemberReferralServiceTestFixture()
        {
            var options = new DbContextOptionsBuilder<MemberDbContext>().UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;
            var context = new MemberDbContext(options);

            var serviceOptions = Options.Create(new MemberServiceOptions());

            repositoryManager = new MemberDbRepositoryManager(context);
            memberSearch = new MemberDbSearch(context);
            memberLogService = new MemberLogService(repositoryManager);
            memberService = new MemberService(serviceOptions, repositoryManager, memberSearch, memberLogService);

            memberReferralService = new MemberReferralService(
                repositoryManager,
                memberService,
                memberLogService);
        }

        public MemberReferralServiceTestFixture WithTestData()
        {
            repositoryManager.MemberRepository.Create(new Member()
            {
                Id = 1,
                Sid = "TESTMEMBER1",
                FirstName = "Beam",
                LastName = "Go",
                ReferralCode = "TESTREFCODE"
            });

            repositoryManager.MemberRepository.Create(new Member()
            {
                Id = 2,
                Sid = "TESTMEMBER2",
                FirstName = "Nan",
                LastName = "Kyaw"
            });

            repositoryManager.MemberRepository.Create(new Member()
            {
                Id = 3,
                Sid = "TESTMEMBER3",
                FirstName = "Nan2",
                LastName = "Kyaw"
            });

            repositoryManager.MemberReferralRepository.Create(new MemberReferral()
            {
                Id = 1,
                Sid = "TESTREFERRAL1",
                ReferrerMemberId = 1,
                ReferredMemberId = 2,
                ReferralType = MemberReferralType.SalesPerson
            });

            repositoryManager.MemberReferralRepository.Create(new MemberReferral()
            {
                Id = 2,
                Sid = "TESTREFERRAL2",
                ReferrerMemberId = 1,
                ReferredMemberId = 3,
                ReferralType = MemberReferralType.SalesPerson
            });

            repositoryManager.MemberReferralRepository.Create(new MemberReferral()
            {
                Id = 3,
                Sid = "TESTREFERRAL3",
                ReferrerMemberId = 2,
                ReferredMemberId = 3,
                ReferralType = MemberReferralType.Delegate
            });

            repositoryManager.SaveChanges();

            return this;
        }

        public void Dispose()
        {
            memberReferralService = null;
            repositoryManager.Dispose();
        }
    }
}
