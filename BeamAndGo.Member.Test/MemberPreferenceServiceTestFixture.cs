﻿using BeamAndGo.Member.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeamAndGo.Member.Test
{
    public class MemberPreferenceServiceTestFixture : IDisposable
    {
        public IMemberPreferenceService memberPreferenceService;
        public IMemberLogService memberLogService;
        public IMemberPreferenceTypeService memberPreferenceTypeService;
        public IMemberRepositoryManager memberRepositoryManager;
        public IMemberSearch memberSearch;
        public IMemberService memberService;

        public MemberPreferenceServiceTestFixture()
        {
            var options = new DbContextOptionsBuilder<MemberDbContext>().UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;
            var context = new MemberDbContext(options);

            var serviceOptions = Options.Create(new MemberServiceOptions());

            memberRepositoryManager = new MemberDbRepositoryManager(context);
            memberSearch = new MemberDbSearch(context);
            memberLogService = new MemberLogService(memberRepositoryManager);
            memberPreferenceTypeService = new MemberPreferenceTypeService(memberRepositoryManager);
            memberService = new MemberService(serviceOptions, memberRepositoryManager, memberSearch, memberLogService);

            memberPreferenceService = new MemberPreferenceService(memberRepositoryManager, memberPreferenceTypeService, memberService, memberLogService);
        }

        public MemberPreferenceServiceTestFixture WithTestData()
        {
            memberRepositoryManager.MemberRepository.Create(new Member()
            {
                Id = 1,
                Sid = "TESTMEMBER",
                FirstName = "Beam",
                LastName = "Go",
                CountryCode = "SG"
            });

            memberRepositoryManager.MemberPreferenceTypeRepository.Create(new MemberPreferenceType()
            {
                Id = 1,
                Name = "IsEmailAllowed",
                Description = "test",
                DefaultValue = "false"
            });

            memberRepositoryManager.MemberPreferenceTypeRepository.Create(new MemberPreferenceType()
            {
                Id = 2,
                Name = "IsTextAllowed",
                Description = "test",
                DefaultValue = "false"
            });

            memberRepositoryManager.MemberPreferenceRepository.Create(new MemberPreference()
            {
                Id = 1,
                Sid = "TESTPREFERENCE1",
                MemberId = 1,
                PreferenceTypeId = 1,
                Value = "false"
            });

            memberRepositoryManager.MemberPreferenceRepository.Create(new MemberPreference()
            {
                Id = 2,
                Sid = "TESTPREFERENCE2",
                MemberId = 1,
                PreferenceTypeId = 2,
                Value = "false"
            });

            memberRepositoryManager.SaveChanges();

            return this;
        }

        public void Dispose()
        {
            memberPreferenceService = null;
            memberPreferenceTypeService = null;
            memberService = null;
            memberLogService = null;
            memberSearch = null;
            memberRepositoryManager.Dispose();
        }
    }
}
