﻿using BeamAndGo.Member.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeamAndGo.Member.Test
{
    public class MemberRelationshipServiceTestFixture: IDisposable
    {
        public IMemberRelationshipService memberRelationshipService;
        public IMemberLogService memberLogService;
        public IMemberRelationshipTypeService memberRelationshipTypeService;
        public IMemberRepositoryManager memberRepositoryManager;
        public IMemberSearch memberSearch;
        public IMemberService memberService;

        public MemberRelationshipServiceTestFixture()
        {
            var options = new DbContextOptionsBuilder<MemberDbContext>().UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;
            var context = new MemberDbContext(options);

            var serviceOptions = Options.Create(new MemberServiceOptions());

            memberRepositoryManager = new MemberDbRepositoryManager(context);
            memberSearch = new MemberDbSearch(context);
            memberLogService = new MemberLogService(memberRepositoryManager);
            memberRelationshipTypeService = new MemberRelationshipTypeService(memberRepositoryManager);
            memberService = new MemberService(serviceOptions, memberRepositoryManager, memberSearch, memberLogService);
          
            memberRelationshipService = new MemberRelationshipService(serviceOptions, memberRepositoryManager, memberRelationshipTypeService, memberService, memberLogService);
        }

        public MemberRelationshipServiceTestFixture WithTestData()
        {
            memberRepositoryManager.MemberRepository.Create(new Member()
            {
                Id = 1,
                Sid = "TESTMEMBER",
                FirstName = "Beam",
                LastName = "Go",
                CountryCode = "SG"
            });

            memberRepositoryManager.MemberRelationshipTypeRepository.Create(new MemberRelationshipType()
            {
                Id = 1,
                Name = "Friend",
                Gender = 'F'
            });

            memberRepositoryManager.MemberRelationshipTypeRepository.Create(new MemberRelationshipType()
            {
                Id = 2,
                Name = "Brother",
                Gender = 'M'
            });

            memberRepositoryManager.MemberRelationshipRepository.Create(new MemberRelationship()
            {
                Id = 1,
                MemberId = 1,
                Sid = "TESTRELATIONSHIP1",
                RelationshipTypeId = 1,
                FirstName = "test",
                LastName = "test",
                CallingCode = "65",
                PhoneNumber = "12345678"
            });

            memberRepositoryManager.SaveChanges();

            return this;
        }

        public void Dispose()
        {
            memberRelationshipService = null;
            memberRelationshipTypeService = null;
            memberService = null;
            memberLogService = null;
            memberSearch = null;
            memberRepositoryManager.Dispose();
        }
    }
}
