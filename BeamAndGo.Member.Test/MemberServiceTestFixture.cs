﻿using BeamAndGo.Member.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeamAndGo.Member.Test
{
    public class MemberServiceTestFixture : IDisposable
    {
        public IMemberService memberService;
        public IMemberLogService memberLogService;
        public IMemberSearch memberSearch;
        public IMemberRepositoryManager memberRepositoryManager;
       
        public MemberServiceTestFixture()
        {
            var options = new DbContextOptionsBuilder<MemberDbContext>().UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;
            var context = new MemberDbContext(options);

            var serviceOptions = Options.Create(new MemberServiceOptions());
            memberRepositoryManager = new MemberDbRepositoryManager(context);
            memberSearch = new MemberDbSearch(context);
            memberLogService = new MemberLogService(memberRepositoryManager);
            
            memberService = new MemberService(serviceOptions, memberRepositoryManager, memberSearch, memberLogService);
        }

        public MemberServiceTestFixture WithTestData()
        {
            memberRepositoryManager.MemberRepository.Create(new Member()
            {
                Id = 1,
                Sid = "TESTMEMBER",
                FirstName = "Beam",
                MiddleName = "And",
                LastName = "Go",
                Gender = 'M',
                BirthDay = 01,
                BirthMonth = 01,
                BirthYear = 2001,
                CountryCode = "PH"
            });

            memberRepositoryManager.MemberPhoneRepository.Create(new MemberPhone()
            {
                Id = 1,
                Sid = "TESTPHONE1",
                MemberId = 1,
                CallingCode = "65",
                PhoneNumber = "11111111"
            });

            memberRepositoryManager.MemberEmailRepository.Create(new MemberEmail()
            {
                Id = 1,
                Sid = "TESTEMAIL1",
                MemberId = 1,
                Email = "test@bng.com"
            });

            memberRepositoryManager.MemberLogRepository.Create(new MemberLog()
            {
                Id = 1,
                MemberId = 1,
                Meta = "test"
            });

            memberRepositoryManager.SaveChanges();

            return this;
        }

        public void Dispose()
        {
            memberService = null;
            memberSearch = null;
            memberLogService = null;
            memberRepositoryManager.Dispose();
        }
    }
}
