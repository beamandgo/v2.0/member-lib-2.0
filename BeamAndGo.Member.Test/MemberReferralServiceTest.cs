﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace BeamAndGo.Member.Test
{
    public class MemberReferralServiceTest
    {
        [Theory]
        [InlineData("TESTMEMBER2", "TESTREFCODE", MemberReferralType.Referral)]
        public async Task RegisterReferrer_WithValidValues_ShouldReturnMember(string referredMemberSid, string referralCode, MemberReferralType referralType)
        {
            using var fixture = new MemberReferralServiceTestFixture().WithTestData();

            var member = await fixture.memberReferralService.Register(referredMemberSid, referralCode, referralType);

            Assert.NotNull(member);
            Assert.NotNull(member.ReferrerMemberId);
        }

        [Theory]
        [InlineData(null, "TESTREFCODE", MemberReferralType.Referral)]
        [InlineData("TESTMEMBER2", null, MemberReferralType.Referral)]
        [InlineData("INVALIDMEMEBRSID", "TESTREFCODE", MemberReferralType.Referral)]
        [InlineData("TESTMEMBER2", "INVALIDREFCODE", MemberReferralType.Referral)]
        public async Task RegisterReferrer_WithInvalidValues_ShouldThrowArgumentException(string referredMemberSid, string referralCode, MemberReferralType referralType)
        {
            using var fixture = new MemberReferralServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async () 
                => await fixture.memberReferralService.Register(referredMemberSid, referralCode, referralType));
        }

        [Theory]
        [InlineData("TESTMEMBER3", "TESTREFCODE", MemberReferralType.SalesPerson)]
        public async Task RegisterReferrer_WithExistingReferrer_ShouldThrowInvalidOperationException(string referredMemberSid, string referralCode, MemberReferralType referralType)
        {
            using var fixture = new MemberReferralServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<InvalidOperationException>(async ()
                => await fixture.memberReferralService.Register(referredMemberSid, referralCode, referralType));
        }

        [Theory]
        [InlineData("TESTMEMBER3")]
        public async Task ListByReferredMemberSid_WithValidValues_ShouldReturnListofMemberReferral(string memberSid)
        {
            using var fixture = new MemberReferralServiceTestFixture().WithTestData();

            var referrals = fixture.memberReferralService.ListByReferredMemberSid(memberSid);

            Assert.NotNull(referrals);
            Assert.True(await referrals.AnyAsync());
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("INVALIDMEMBERSID")]
        public async Task ListByReferredMemberSid_WithInvalidValues_ShouldThrowArgumentException(string memberSid)
        {
            using var fixture = new MemberReferralServiceTestFixture().WithTestData();

            var referrals = fixture.memberReferralService.ListByReferredMemberSid(memberSid);

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await referrals.GetAsyncEnumerator().MoveNextAsync());
        }

        [Theory]
        [InlineData("TESTMEMBER3", MemberReferralType.SalesPerson)]
        public async Task ListByReferredMemberSidAndReferralType_WithValidValues_ShouldReturnListofMemberReferral(string memberSid, MemberReferralType referralType)
        {
            using var fixture = new MemberReferralServiceTestFixture().WithTestData();

            var referrals = fixture.memberReferralService.ListByReferredMemberSidAndReferralType(memberSid, referralType);

            Assert.NotNull(referrals);
            Assert.True(await referrals.AnyAsync());
        }

        [Theory]
        [InlineData("", MemberReferralType.SalesPerson)]
        [InlineData(null, MemberReferralType.SalesPerson)]
        [InlineData("INVALIDMEMBERSID", MemberReferralType.SalesPerson)]
        public async Task ListByReferredMemberSidAndReferralType_WithInvalidValues_ShouldThrowArgumentException(string memberSid, MemberReferralType referralType)
        {
            using var fixture = new MemberReferralServiceTestFixture().WithTestData();

            var referrals = fixture.memberReferralService.ListByReferredMemberSidAndReferralType(memberSid, referralType);

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await referrals.GetAsyncEnumerator().MoveNextAsync());
        }

        [Theory]
        [InlineData("TESTMEMBER1")]
        public async Task ListByReferrerMemberSid_WithValidValues_ShouldReturnListofMemberReferral(string memberSid)
        {
            using var fixture = new MemberReferralServiceTestFixture().WithTestData();

            var referrals = fixture.memberReferralService.ListByReferrerMemberSid(memberSid);

            Assert.NotNull(referrals);
            Assert.True(await referrals.AnyAsync());
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("INVALIDMEMBERSID")]
        public async Task ListByReferrerMemberSid_WithInvalidValues_ShouldThrowArgumentException(string memberSid)
        {
            using var fixture = new MemberReferralServiceTestFixture().WithTestData();

            var referrals = fixture.memberReferralService.ListByReferrerMemberSid(memberSid);

            await Assert.ThrowsAsync<ArgumentException>(async ()
                =>await referrals.GetAsyncEnumerator().MoveNextAsync());
        }

        [Theory]
        [InlineData("TESTMEMBER1", MemberReferralType.SalesPerson)]
        public async Task ListByReferrerMemberSidAndReferralType_WithValidValues_ShouldReturnListofMemberReferral(string memberSid, MemberReferralType referralType)
        {
            using var fixture = new MemberReferralServiceTestFixture().WithTestData();

            var referrals = fixture.memberReferralService.ListByReferrerMemberSidAndReferralType(memberSid,referralType);

            Assert.NotNull(referrals);
            Assert.True(await referrals.AnyAsync());
        }

        [Theory]
        [InlineData("", MemberReferralType.SalesPerson)]
        [InlineData(null, MemberReferralType.SalesPerson)]
        [InlineData("INVALIDMEMBERSID", MemberReferralType.SalesPerson)]
        public async Task ListByReferrerMemberSidAndReferralType_WithInvalidValues_ShouldThrowArgumentException(string memberSid, MemberReferralType referralType)
        {
            using var fixture = new MemberReferralServiceTestFixture().WithTestData();

            var referrals = fixture.memberReferralService.ListByReferrerMemberSidAndReferralType(memberSid, referralType);

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await referrals.GetAsyncEnumerator().MoveNextAsync());
        }
    }
}
