using System;
using System.Threading.Tasks;
using Xunit;
using BeamAndGo.Core.Data;
using BeamAndGo.Member;
using System.Linq;
using BeamAndGo.Member.Admin;
using System.Diagnostics.CodeAnalysis;

namespace BeamAndGo.Member.Test
{
    
    public class MemberAdminServiceTest : IClassFixture<MemberAdminServiceTestFixture>
    {
        [Theory]
        [InlineData("TESTADMIN1")]
        public async Task GetBySid_WithValidValues_ShouldReturnMemberAdmin(string adminSid)
        {
            using var fixture = new MemberAdminServiceTestFixture().WithTestData();

            var admin = await fixture.adminService.GetBySid(adminSid);

            Assert.NotNull(admin);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("INVALIDADMINSID")]
        public async Task GetBySid_WithInvalidValues_ShouldThrowArgumentException(string adminSid)
        {
            using var fixture = new MemberAdminServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async()
                => await fixture.adminService.GetBySid(adminSid));
        }

        [Theory]
        [InlineData("TESTMEMBER1")]
        public async Task GetByMemberSid_WithValidValues_ShouldReturnMemberAdmin(string memberSid)
        {
            using var fixture = new MemberAdminServiceTestFixture().WithTestData();

            var admin = await fixture.adminService.GetByMemberSid(memberSid);

            Assert.NotNull(admin);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("INVALIDMMEMBERSID")]
        public async Task GetByMemberSid_WithInvalidValues_ShouldThrowArgumentException(string memberSid)
        {
            using var fixture = new MemberAdminServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.adminService.GetByMemberSid(memberSid));
        }

        [Theory]
        [InlineData("TESTMEMBER2", MemberAdminAccessLevel.FullAccess, null)]
        public async Task CreateAdmin_WithValidValues_ShouldReturnMemberAdmin(string memberSid, MemberAdminAccessLevel accessLevel, DateTimeOffset? expiry)
        {
            using var fixture = new MemberAdminServiceTestFixture().WithTestData();

            var admin = await fixture.adminService.Create(memberSid, accessLevel, expiry);

            Assert.NotNull(admin);
            Assert.True(admin.Id > 0);
        }

        [Theory]
        [InlineData("", MemberAdminAccessLevel.FullAccess, null)]
        [InlineData(null, MemberAdminAccessLevel.FullAccess, null)]
        [InlineData("INVALIDMEMBERSID", MemberAdminAccessLevel.FullAccess, null)]
        public async Task CreateAdmin_WithInvalidValues_ShouldThrowArgumentException(string memberSid, MemberAdminAccessLevel accessLevel, DateTimeOffset? expiry)
        {
            using var fixture = new MemberAdminServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.adminService.Create(memberSid, accessLevel, expiry));
        }

        [Theory]
        [InlineData("TESTMEMBER1", MemberAdminAccessLevel.FullAccess, null)]
        public async Task CreateAdmin_WithExistingAdminAccount_ShouldThrowInvalidOperationException(string memberSid, MemberAdminAccessLevel accessLevel, DateTimeOffset? expiry)
        {
            using var fixture = new MemberAdminServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<InvalidOperationException>(async ()
                => await fixture.adminService.Create(memberSid, accessLevel, expiry));
        }

        [Theory]
        [InlineData("TESTADMIN1", MemberAdminAccessLevel.FullAccess, null)]
        public async Task UpdateAdmin_WithValidValues_ShouldReturnMemberAdmin(string adminSid, MemberAdminAccessLevel accessLevel, DateTimeOffset? expiry)
        {
            using var fixture = new MemberAdminServiceTestFixture().WithTestData();

            var admin = await fixture.adminService.Update(adminSid, accessLevel, expiry);

            Assert.NotNull(admin);
        }

        [Theory]
        [InlineData("", MemberAdminAccessLevel.FullAccess, null)]
        [InlineData(null, MemberAdminAccessLevel.FullAccess, null)]
        [InlineData("INVALIDADMINSID", MemberAdminAccessLevel.FullAccess, null)]
        public async Task UpdateAdmin_WithInvalidValues_ShouldThrowArgumentException(string memberSid, MemberAdminAccessLevel accessLevel, DateTimeOffset? expiry)
        {
            using var fixture = new MemberAdminServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.adminService.Update(memberSid, accessLevel, expiry));
        }

        [Theory]
        [InlineData("TESTADMIN1")]
        public async Task DeleteAdmin_WithValidValues_ShouldReturnMemberAdmin(string adminSid)
        {
            using var fixture = new MemberAdminServiceTestFixture().WithTestData();

            await fixture.adminService.Delete(adminSid);

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.adminService.GetBySid(adminSid));
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("INVALIDADMINSID")]
        public async Task DeleteAdmin_WithInvalidValues_ShouldThrowArgumentException(string adminSid)
        {
            using var fixture = new MemberAdminServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.adminService.Delete(adminSid));
        }

        [Theory]
        [InlineData("TESTADMIN1")]
        public async Task EnableAdmin_WithValidValues_ShouldReturnMemberAdmin(string adminSid)
        {
            using var fixture = new MemberAdminServiceTestFixture().WithTestData();

            var admin = await fixture.adminService.Enable(adminSid);

            Assert.NotNull(admin);
            Assert.True(admin.RecordStatus == RecordStatus.Active);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("INVALIDADMINSID")]
        public async Task EnableAdmin_WithInvalidValues_ShouldThrowArgumentException(string adminSid)
        {
            using var fixture = new MemberAdminServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.adminService.Enable(adminSid));
        }

        [Theory]
        [InlineData("TESTADMIN1")]
        public async Task DisableAdmin_WithValidValues_ShouldReturnMemberAdmin(string adminSid)
        {
            using var fixture = new MemberAdminServiceTestFixture().WithTestData();

            var admin = await fixture.adminService.Disable(adminSid);

            Assert.NotNull(admin);
            Assert.True(admin.RecordStatus == RecordStatus.Inactive);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("INVALIDADMINSID")]
        public async Task DisableAdmin_WithInvalidValues_ShouldThrowArgumentException(string adminSid)
        {
            using var fixture = new MemberAdminServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.adminService.Disable(adminSid));
        }

        [Fact]
        public async Task ListAllAdmins_WithValidValues_ShouldReturnListOfMemberAdmin()
        {
            using var fixture = new MemberAdminServiceTestFixture().WithTestData();

            var admins = await fixture.adminService.ListAll();

            Assert.NotNull(admins);
            Assert.True(admins.Any());
        }

        [Theory]
        [InlineData("TESTADMIN1")]
        public async Task SearchAdmin_WithValidValues_ShouldReturnListOfMemberAdmin(string searchString)
        {
            using var fixture = new MemberAdminServiceTestFixture().WithTestData();

            var admins = fixture.adminService.Search(searchString);

            Assert.True(await admins.AnyAsync());
        }
    }
}
