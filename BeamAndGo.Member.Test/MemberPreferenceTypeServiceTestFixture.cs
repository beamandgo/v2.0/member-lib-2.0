﻿using BeamAndGo.Member.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeamAndGo.Member.Test
{
    public class MemberPreferenceTypeServiceTestFixture : IDisposable
    {
        public IMemberPreferenceTypeService memberPreferenceTypeService;
        public IMemberRepositoryManager memberRepositoryManager;

        public MemberPreferenceTypeServiceTestFixture()
        {
            var options = new DbContextOptionsBuilder<MemberDbContext>().UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;
            var context = new MemberDbContext(options);

            var serviceOptions = Options.Create(new MemberServiceOptions());

            memberRepositoryManager = new MemberDbRepositoryManager(context);
            memberPreferenceTypeService = new MemberPreferenceTypeService(memberRepositoryManager);
        }

        public MemberPreferenceTypeServiceTestFixture WithTestData()
        {
            memberRepositoryManager.MemberPreferenceTypeRepository.Create(new MemberPreferenceType()
            {
                Id = 1,
                Name = "IsEmailAllowed",
                Description = "Allow to receive promotional emails",
                DefaultValue = "false",
                RecordStatus = Core.Data.RecordStatus.Active
            });

            memberRepositoryManager.MemberPreferenceTypeRepository.Create(new MemberPreferenceType()
            {
                Id = 2,
                Name = "IsTextAllowed",
                Description = "Allow to receive promotional SMS",
                DefaultValue = "false",
                RecordStatus = Core.Data.RecordStatus.Inactive
            });

            memberRepositoryManager.MemberPreferenceRepository.Create(new MemberPreference()
            {
                Id = 1,
                Sid = "TESTPREFERENCE1",
                MemberId = 1,
                PreferenceTypeId = 2,
                Value = "false"
            });

            memberRepositoryManager.SaveChanges();

            return this;
        }

        public void Dispose()
        {
            memberPreferenceTypeService = null;
            memberRepositoryManager.Dispose();
        }
    }
}
