﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace BeamAndGo.Member.Test
{
    public class MemberPreferenceTypeServiceTest
    {
        [Theory]
        [InlineData(1)]
        public async Task GetById_WithValidValues_ShouldReturnMemberPreferenceType(int id)
        {
            using var fixture = new MemberPreferenceTypeServiceTestFixture().WithTestData();

            var preferenceType = await fixture.memberPreferenceTypeService.GetById(id);

            Assert.NotNull(preferenceType);
            Assert.True(preferenceType.Id == 1);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task GetById_WithInvalidValues_ShouldThrowArgumentException(int id)
        {
            using var fixture = new MemberPreferenceTypeServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberPreferenceTypeService.GetById(id));
        }

        [Theory]
        [InlineData("IsEmailAllowed", "Allow to receive promotional emails", "false")]
        public async Task CreatePreferenceType_WithValidValues_ShouldReturnMemberPreferenceType(string name, string description, string defaultValue)
        {
            using var fixture = new MemberPreferenceTypeServiceTestFixture();

            var preferenceType = await fixture.memberPreferenceTypeService.Create(name, description, defaultValue);

            Assert.NotNull(preferenceType);
            Assert.True(preferenceType.Id > 0);
        }

        [Theory]
        [InlineData(null, "Allow to receive promotional emails", "false")]
        [InlineData("IsEmailAllowed", null, "false")]
        [InlineData("IsEmailAllowed", "Allow to receive promotional emails", null)]
        public async Task CreatePreferenceType_WithInvalidValues_ShouldThrowArgumentException(string name, string description, string defaultValue)
        {
            using var fixture = new MemberPreferenceTypeServiceTestFixture();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberPreferenceTypeService.Create(name, description, defaultValue));
        }

        [Theory]
        [InlineData(1, "IsTextAllowed", "Allow to receive promotional SMS", "true")]
        public async Task UpdatePreferenceType_WithValidValues_ShouldReturnMemberPreferenceType(int id, string name, string description, string defaultValue)
        {
            using var fixture = new MemberPreferenceTypeServiceTestFixture().WithTestData();

            var preferenceType = await fixture.memberPreferenceTypeService.Update(id, name, description, defaultValue);

            Assert.NotNull(preferenceType);
            Assert.True(preferenceType.Id > 0);
        }

        [Theory]
        [InlineData(0, "IsTextAllowed", "Allow to receive promotional SMS", "true")]
        [InlineData(1, null, "Allow to receive promotional SMS", "true")]
        [InlineData(1, "IsTextAllowed", null, "true")]
        [InlineData(1, "IsTextAllowed", "Allow to receive promotional SMS", null)]
        public async Task UpdatePreferenceType_WithInvalidValues_ShouldThrowArgumentException(int id, string name, string description, string defaultValue)
        {
            using var fixture = new MemberPreferenceTypeServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberPreferenceTypeService.Update(id, name, description, defaultValue));
        }

        [Theory]
        [InlineData(1)]
        public async Task DeletePreferenceType_WithValidValues_ShouldReturnNothing(int id)
        {
            using var fixture = new MemberPreferenceTypeServiceTestFixture().WithTestData();

            await fixture.memberPreferenceTypeService.Delete(id);

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberPreferenceTypeService.GetById(id));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task DeletePreferenceType_WithInvalidValues_ShouldThrowArgumentException(int id)
        {
            using var fixture = new MemberPreferenceTypeServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberPreferenceTypeService.Delete(id));
        }

        [Theory]
        [InlineData(2)]
        public async Task DeletePreferenceType_InUse_ShouldThrowInvalidOperationException(int id)
        {
            using var fixture = new MemberPreferenceTypeServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<InvalidOperationException>(async ()
                => await fixture.memberPreferenceTypeService.Delete(id));
        }

        [Theory]
        [InlineData(1)]
        public async Task EnablePreferenceType_WithValidValues_ShouldReturnMemberPreferenceType(int id)
        {
            using var fixture = new MemberPreferenceTypeServiceTestFixture().WithTestData();

            var preferenceType = await fixture.memberPreferenceTypeService.Enable(id);
            Assert.True(preferenceType.RecordStatus == Core.Data.RecordStatus.Active);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task EnablePreferenceType_WithInvalidValues_ShouldThrowArgumentException(int id)
        {
            using var fixture = new MemberPreferenceTypeServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberPreferenceTypeService.Enable(id));
        }

        [Theory]
        [InlineData(1)]
        public async Task DisablePreferenceType_WithValidValues_ShouldReturnMemberPreferenceType(int id)
        {
            using var fixture = new MemberPreferenceTypeServiceTestFixture().WithTestData();

            var preferenceType = await fixture.memberPreferenceTypeService.Disable(id);
            Assert.True(preferenceType.RecordStatus == Core.Data.RecordStatus.Inactive);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task DisablePreferenceType_WithInvalidValues_ShouldThrowArgumentException(int id)
        {
            using var fixture = new MemberPreferenceTypeServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberPreferenceTypeService.Disable(id));
        }

        [Fact]
        public async Task ListAll_ShouldReturnListofPreferenceType()
        {
            using var fixture = new MemberPreferenceTypeServiceTestFixture().WithTestData();

            var preferenceTypes = await fixture.memberPreferenceTypeService.ListAll();

            Assert.True(preferenceTypes.Any());
            Assert.True(preferenceTypes.Count() == 2);
        }

        [Fact]
        public async Task ListAllActiveSorted_ShouldReturnListofPreferenceType()
        {
            using var fixture = new MemberPreferenceTypeServiceTestFixture().WithTestData();

            var preferenceTypes = await fixture.memberPreferenceTypeService.ListAllActiveSorted();

            Assert.True(preferenceTypes.Any());
            Assert.True(preferenceTypes.Count() == 1);
        }
    }
}
