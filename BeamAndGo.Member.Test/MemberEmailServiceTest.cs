﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BeamAndGo.Member.Test
{
    public class MemberEmailServiceTest
    {

        [Theory]
        [InlineData("TESTEMAIL1")]
        public async Task GetBySid_WithValidEmailSid_ShouldReturnMemberEmail(string sid)
        {
            using var fixture = new MemberEmailServiceTestFixture().WithTestData();

            var Email = await fixture.memberEmailService.GetBySid(sid);

            Assert.NotNull(Email);
            Assert.True(Email.Id > 0);
        }

        [Theory]
        [InlineData("INVALIDEMAILSID")]
        [InlineData("")]
        [InlineData(null)]
        public async Task GetBySid_WithInvalidEmailSid_ShouldThrowArgumentException(string sid)
        {
            using var fixture = new MemberEmailServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberEmailService.GetBySid(sid));
        }

        [Theory]
        [InlineData("TESTMEMBER", "test@beamandgo.com")]
        public async Task Create_WithValidEmail_ShouldReturnMemberEmail(string memberSid, string email)
        {
            using var fixture = new MemberEmailServiceTestFixture().WithTestData();

            var memberEmail = await fixture.memberEmailService.Create(memberSid, email);

            Assert.NotNull(memberEmail);
        }

        [Theory]
        [InlineData(null, "test@beamandgo.com")]
        [InlineData("TESTMEMBER", null)]
        [InlineData("INVALIDMEMBERSID", "test@beamandgo.com")]
        [InlineData("TESTMEMBER", "te0st*te$t@bng.com")]
        public async Task Create_WithInvalidValues_ShouldThrowArgumentException(string memberSid, string email)
        {
            using var fixture = new MemberEmailServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberEmailService.Create(memberSid, email));
        }

        [Theory]
        [InlineData("TESTEMAIL2", "test1@bemandgo.com")]
        public async Task Update_WithValidValues_ShouldReturnMemberEmail(string sid, string email)
        {
            using var fixture = new MemberEmailServiceTestFixture().WithTestData();

            var memberEmail = await fixture.memberEmailService.Update(sid, email);

            Assert.NotNull(memberEmail);
        }

        [Theory]
        [InlineData(null, "test@bng.com")]
        [InlineData("TESTEMAIL2", null)]
        [InlineData("INVALIDEMAILSID", "test@bng.com")]
        [InlineData("TESTEMAIL2", "te0st*te$t@bng.com")]
        public async Task Update_WithInvalidValues_ShouldThrowArgumentException(string sid, string email)
        {
            using var fixture = new MemberEmailServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberEmailService.Update(sid, email));
        }

        [Theory]
        [InlineData("TESTEMAIL2", "Verified with verification code / link")]
        public async Task Verify_EmailAlreadyVerified_ShouldThrowInvalidOperationException(string sid, string meta)
        {
            using var fixture = new MemberEmailServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<InvalidOperationException>(async ()
                => await fixture.memberEmailService.Verify(sid, meta));
        }

        [Theory]
        [InlineData("TESTEMAIL3", "Verified with verification code / link")]
        public async Task Verify_WithValidValues_ShouldReturnMemberEmail(string sid, string meta)
        {
            using var fixture = new MemberEmailServiceTestFixture().WithTestData();

            await fixture.memberEmailService.Verify(sid, meta);

            var email = await fixture.memberEmailService.GetBySid(sid);
            Assert.True(email.IsVerified);
        }

        [Theory]
        [InlineData("TESTEMAIL3")]
        public async Task Delete_WithValidValues_ShouldReturnNothing(string sid)
        {
            using var fixture = new MemberEmailServiceTestFixture().WithTestData();

            await fixture.memberEmailService.Delete(sid);

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberEmailService.GetBySid(sid));
        }

        [Theory]
        [InlineData("TESTEMAIL1")]
        public async Task Delete_DefaultEmail_ShouldThrowInvalidOperationException(string sid)
        {
            using var fixture = new MemberEmailServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<InvalidOperationException>(async ()
                => await fixture.memberEmailService.Delete(sid));
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task Delete_WithInvalidEmailSid_ShouldThrowArgumentException(string sid)
        {
            using var fixture = new MemberEmailServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberEmailService.Delete(sid));
        }

        [Theory]
        [InlineData("TESTEMAIL2")]
        public async Task SetDefault_WithValidValues_ShouldReturnMemberEmail(string sid)
        {
            using var fixture = new MemberEmailServiceTestFixture().WithTestData();

            await fixture.memberEmailService.SetDefault(sid);

            var email = await fixture.memberEmailService.GetBySid(sid);
            Assert.True(email.IsDefault);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public async Task SetDefault_WithInvalidEmailSid_ShouldThrowArgumentException(string sid)
        {
            using var fixture = new MemberEmailServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberEmailService.SetDefault(sid));
        }

        [Theory]
        [InlineData("TESTMEMBER")]
        public async Task ListByMemberSid_WithValidMemberSid_ShouldReturnListOfMemberEmails(string memberSid)
        {
            using var fixture = new MemberEmailServiceTestFixture().WithTestData();

            var emails = fixture.memberEmailService.ListByMemberSid(memberSid);

            Assert.True(await emails.AnyAsync());
        }

        [Theory]
        [InlineData("INVALIDMEMBERSID")]
        [InlineData("")]
        [InlineData(null)]
        public async Task ListByMemberSid_WithInvalidMemberSid_ShouldThrowArgumentException(string memberSid)
        {
            using var fixture = new MemberEmailServiceTestFixture().WithTestData();

            var emails = fixture.memberEmailService.ListByMemberSid(memberSid);

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await emails.GetAsyncEnumerator().MoveNextAsync());
        }
    }
}
