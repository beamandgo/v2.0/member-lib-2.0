﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BeamAndGo.Member.Test
{
    public class MemberRelationshipTypeServiceTest
    {
        [Theory]
        [InlineData(1)]
        public async Task GetById_WithValidValues_ShouldReturnMemberRelationshipType(int id)
        {
            using var fixture = new MemberRelationshipTypeServiceTestFixture().WithTestData();

            var relationshipType = await fixture.memberRelationshipTypeService.GetById(id);

            Assert.NotNull(relationshipType);
            Assert.True(relationshipType.Id == 1);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task GetById_WithInvalidValues_ShouldThrowArgumentException(int id)
        {
            using var fixture = new MemberRelationshipTypeServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberRelationshipTypeService.GetById(id));
        }

        [Theory]
        [InlineData("Friend")]
        [InlineData("Brother", 'M')]
        public async Task CreateRelationshipType_WithValidValues_ShouldReturnMemberRelationshipType(string name, char? gender = null)
        {
            using var fixture = new MemberRelationshipTypeServiceTestFixture();

            var relationshipType = await fixture.memberRelationshipTypeService.Create(name, gender);

            Assert.NotNull(relationshipType);
            Assert.True(relationshipType.Id > 0);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public async Task CreateRelationshipType_WithInvalidValues_ShouldThrowArgumentException(string name, char? gender = null)
        {
            using var fixture = new MemberRelationshipTypeServiceTestFixture();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberRelationshipTypeService.Create(name, gender));
        }

        [Theory]
        [InlineData(1, "Father", 'M')]
        public async Task UpdateRelationshipType_WithValidValues_ShouldReturnMemberRelationshipType(int id, string name, char? gender = null)
        {
            using var fixture = new MemberRelationshipTypeServiceTestFixture().WithTestData();

            var relationshipType = await fixture.memberRelationshipTypeService.Update(id, name, gender);

            Assert.NotNull(relationshipType);
            Assert.True(relationshipType.Id > 0);
        }

        [Theory]
        [InlineData(0, "Father", 'M')]
        [InlineData(1, null, 'M')]
        public async Task UpdateRelationshipType_WithInvalidValues_ShouldThrowArgumentException(int id, string name, char? gender = null)
        {
            using var fixture = new MemberRelationshipTypeServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberRelationshipTypeService.Update(id, name, gender));
        }

        [Theory]
        [InlineData(1)]
        public async Task DeleteRelationshipType_WithValidValues_ShouldReturnNothing(int id)
        {
            using var fixture = new MemberRelationshipTypeServiceTestFixture().WithTestData();

            await fixture.memberRelationshipTypeService.Delete(id);

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberRelationshipTypeService.GetById(id));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task DeleteRelationshipType_WithInvalidValues_ShouldThrowArgumentException(int id)
        {
            using var fixture = new MemberRelationshipTypeServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberRelationshipTypeService.Delete(id));
        }

        [Theory]
        [InlineData(2)]
        public async Task DeleteRelationshipType_InUse_ShouldThrowInvalidOperationException(int id)
        {
            using var fixture = new MemberRelationshipTypeServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<InvalidOperationException>(async ()
                => await fixture.memberRelationshipTypeService.Delete(id));
        }

        [Theory]
        [InlineData(1)]
        public async Task EnableRelationshipType_WithValidValues_ShouldReturnMemberRelationshipType(int id)
        {
            using var fixture = new MemberRelationshipTypeServiceTestFixture().WithTestData();

            var relationshipType = await fixture.memberRelationshipTypeService.Enable(id);
            Assert.True(relationshipType.RecordStatus == Core.Data.RecordStatus.Active);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task EnableRelationshipType_WithInvalidValues_ShouldThrowArgumentException(int id)
        {
            using var fixture = new MemberRelationshipTypeServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberRelationshipTypeService.Enable(id));
        }

        [Theory]
        [InlineData(1)]
        public async Task DisableRelationshipType_WithValidValues_ShouldReturnMemberRelationshipType(int id)
        {
            using var fixture = new MemberRelationshipTypeServiceTestFixture().WithTestData();

            var relationshipType = await fixture.memberRelationshipTypeService.Disable(id);
            Assert.True(relationshipType.RecordStatus == Core.Data.RecordStatus.Inactive);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task DisableRelationshipType_WithInvalidValues_ShouldThrowArgumentException(int id)
        {
            using var fixture = new MemberRelationshipTypeServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberRelationshipTypeService.Disable(id));
        }

        [Fact]
        public async Task ListAll_ShouldReturnListofRelationshipType()
        {
            using var fixture = new MemberRelationshipTypeServiceTestFixture().WithTestData();

            var relationshipTypes = await fixture.memberRelationshipTypeService.ListAll();

            Assert.True(relationshipTypes.Any());
            Assert.True(relationshipTypes.Count() == 3);
        }

        [Fact]
        public async Task ListAllActiveSorted_ShouldReturnListofRelationshipType()
        {
            using var fixture = new MemberRelationshipTypeServiceTestFixture().WithTestData();

            var relationshipTypes = await fixture.memberRelationshipTypeService.ListAllActiveSortedByName();

            Assert.True(relationshipTypes.Any());
            Assert.True(relationshipTypes.Count() == 2);
        }
    }
}

