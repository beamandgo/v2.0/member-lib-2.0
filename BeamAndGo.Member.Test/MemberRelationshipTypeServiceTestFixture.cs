﻿using BeamAndGo.Member.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeamAndGo.Member.Test
{
    public class MemberRelationshipTypeServiceTestFixture : IDisposable
    {
        public IMemberRelationshipTypeService memberRelationshipTypeService;
        public IMemberRepositoryManager memberRepositoryManager;

        public MemberRelationshipTypeServiceTestFixture()
        {
            var options = new DbContextOptionsBuilder<MemberDbContext>().UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;
            var context = new MemberDbContext(options);

            var serviceOptions = Options.Create(new MemberServiceOptions());

            memberRepositoryManager = new MemberDbRepositoryManager(context);
            memberRelationshipTypeService = new MemberRelationshipTypeService(memberRepositoryManager);
        }

        public MemberRelationshipTypeServiceTestFixture WithTestData()
        {
            memberRepositoryManager.MemberRelationshipTypeRepository.Create(new MemberRelationshipType()
            {
                Id = 1,
                Name = "Friend",
                RecordStatus = Core.Data.RecordStatus.Active
            });

            memberRepositoryManager.MemberRelationshipTypeRepository.Create(new MemberRelationshipType()
            {
                Id = 2,
                Name = "Brother",
                Gender = 'M',
                RecordStatus = Core.Data.RecordStatus.Active
            });

            memberRepositoryManager.MemberRelationshipTypeRepository.Create(new MemberRelationshipType()
            {
                Id = 3,
                Name = "Sister",
                Gender = 'F',
                RecordStatus = Core.Data.RecordStatus.Inactive
            });

            memberRepositoryManager.MemberRelationshipRepository.Create(new MemberRelationship()
            {
                Id = 1,
                MemberId = 1,
                Sid = "TESTRELATIONSHIP1",
                RelationshipTypeId = 2,
                FirstName = "test",
                LastName = "test",
                CallingCode = "65",
                PhoneNumber = "12345678"
            });

            memberRepositoryManager.SaveChanges();

            return this;
        }

        public void Dispose()
        {
            memberRelationshipTypeService = null;
            memberRepositoryManager.Dispose();
        }
    }
}
