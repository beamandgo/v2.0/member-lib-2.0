﻿using BeamAndGo.Member.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using BeamAndGo.Core.Data;
using BeamAndGo.Member.Admin;
using BeamAndGo.Member.Admin.Data;
using Microsoft.Extensions.Options;

namespace BeamAndGo.Member.Test
{
    public class MemberAdminServiceTestFixture : IDisposable
    {
        public IMemberAdminService adminService;
        public IMemberAdminRepositoryManager adminRepositoryManager;
        public IMemberAdminSearch adminSearch;
        public IMemberAdminLogService adminLogService;
        public IMemberRepositoryManager memberRepositoryManager;
        public IMemberLogService memberLogService;
        public IMemberSearch memberSearch;
        public IMemberService memberService;

        public MemberAdminServiceTestFixture()
        {
            var options = new DbContextOptionsBuilder<MemberDbContext>().UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;
            var memberContext = new MemberDbContext(options);
            var adminContext = new MemberAdminDbContext(options);

            var serviceOptions = Options.Create(new MemberServiceOptions());
            memberRepositoryManager = new MemberDbRepositoryManager(memberContext);
            adminRepositoryManager = new MemberAdminDbRepositoryManager(adminContext);
            
            memberSearch = new MemberDbSearch(memberContext);
            memberLogService = new MemberLogService(memberRepositoryManager);
            memberService = new MemberService(serviceOptions, memberRepositoryManager, memberSearch, memberLogService);

            adminSearch = new MemberAdminDbSearch(adminContext);
            adminLogService = new MemberAdminLogService(adminRepositoryManager);
            adminService = new MemberAdminService(adminRepositoryManager, adminSearch, adminLogService, memberService);
        }

        public MemberAdminServiceTestFixture WithTestData()
        {
            memberRepositoryManager.MemberRepository.Create(new Member()
            {
                Id = 1,
                Sid = "TESTMEMBER1",
                FirstName = "Beam",
                LastName = "Go"
            });

            memberRepositoryManager.MemberRepository.Create(new Member()
            {
                Id = 2,
                Sid = "TESTMEMBER2",
                FirstName = "Nan",
                LastName = "Kyaw"
            });

            memberRepositoryManager.SaveChanges();

            adminRepositoryManager.MemberAdminRepository.Create(new MemberAdmin()
            {
                Id = 1,
                Sid = "TESTADMIN1",
                MemberId = 1,
                AccessLevel = MemberAdminAccessLevel.ReadOnlyMasked
            });
            
            adminRepositoryManager.SaveChanges();

            return this;
        }

        public void Dispose()
        {
            memberService = null;
            adminSearch = null;
            adminLogService = null;
            memberRepositoryManager.Dispose();
            adminRepositoryManager.Dispose();
        }
    }
}
