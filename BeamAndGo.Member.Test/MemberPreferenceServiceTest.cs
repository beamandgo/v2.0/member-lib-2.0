﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BeamAndGo.Member.Test
{
    public class MemberPreferenceServiceTest
    {
        [Theory]
        [InlineData("TESTPREFERENCE1")]
        public async Task GetBySid_WithValidPreferenceSid_ShouldReturnMemberPreference(string sid)
        {
            using var fixture = new MemberPreferenceServiceTestFixture().WithTestData();

            var preference = await fixture.memberPreferenceService.GetBySid(sid);

            Assert.NotNull(preference);
            Assert.True(preference.Id > 0);
        }

        [Theory]
        [InlineData("INVALIDPREFERENCESID")]
        [InlineData("")]
        [InlineData(null)]
        public async Task GetBySid_WithInvalidPreferenceSid_ShouldThrowArgumentException(string sid)
        {
            using var fixture = new MemberPreferenceServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberPreferenceService.GetBySid(sid));
        }

        [Theory]
        [InlineData("TESTMEMBER", 1, "true" )]
        public async Task CreateOrUpdate_WithValidValues_ShouldReturnMemberPreference(string memberSid, int preferenceTypeId, string value)
        {
            using var fixture = new MemberPreferenceServiceTestFixture().WithTestData();

            var preference = await fixture.memberPreferenceService.CreateOrUpdate(memberSid, preferenceTypeId, value);

            Assert.NotNull(preference);
            Assert.True(preference.Id > 0 && preference.Value == value);
        }

        [Theory]
        [InlineData(null, 1, "true")]
        [InlineData("TESTMEMBER", 0, "true")]
        [InlineData("TESTMEMBER", 1, null)]
        [InlineData("INVALIDMEMBERSID", 1, "true")]
        public async Task CreateOrUpdate_WithInvalidValues_ShouldThrowArgumentException(string memberSid, int preferenceTypeId, string value)
        {
            using var fixture = new MemberPreferenceServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberPreferenceService.CreateOrUpdate(memberSid, preferenceTypeId, value));
        }

        [Theory]
        [InlineData("TESTPREFERENCE1")]
        public async Task Delete_WithValidValues_ShouldReturnNothing(string sid)
        {
            using var fixture = new MemberPreferenceServiceTestFixture().WithTestData();

            await fixture.memberPreferenceService.Delete(sid);

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberPreferenceService.GetBySid(sid));
        }

        [Theory]
        [InlineData(null)]
        [InlineData("INVALIDMEMBERSID")]
        public async Task Delete_WithInvalidValues_ShouldThrowArgumentException(string sid)
        {
            using var fixture = new MemberPreferenceServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberPreferenceService.Delete(sid));
        }

        [Theory]
        [InlineData("TESTMEMBER")]
        public async Task ListByMemberSid_WithValidValues_ShouldReturnListOfMemberPreference(string memberSid)
        {
            using var fixture = new MemberPreferenceServiceTestFixture().WithTestData();

            var preferences = fixture.memberPreferenceService.ListByMemberSid(memberSid);

            Assert.True(await preferences.AnyAsync());
        }

        [Theory]
        [InlineData(null)]
        [InlineData("INVALIDMEMBERSID")]
        public async Task ListByMemberSid_WithInvalidValues_ShouldThrowArgumentException(string memberSid)
        {
            using var fixture = new MemberPreferenceServiceTestFixture().WithTestData();

            var preferences = fixture.memberPreferenceService.ListByMemberSid(memberSid);

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await preferences.GetAsyncEnumerator().MoveNextAsync());
        }

        [Theory]
        [InlineData("TESTPREFERENCE2")]
        public async Task DeleteByMemberSid_WithValidValues_ShouldReturnNothing(string memberSid)
        {
            using var fixture = new MemberPreferenceServiceTestFixture().WithTestData();

            await fixture.memberPreferenceService.Delete(memberSid);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("INVALIDMEMBERSID")]
        public async Task DeleteByMemberSid_WithInvalidValues_ShouldThrowArgumentException(string memberSid)
        {
            using var fixture = new MemberPreferenceServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberPreferenceService.Delete(memberSid));
        }

        [Fact]
        public async Task ListAllTypes_WithValidValues_ShouldReturnListOfMemberPreferenceType()
        {
            using var fixture = new MemberPreferenceServiceTestFixture().WithTestData();

            var preferenceTypes = await fixture.memberPreferenceTypeService.ListAllActiveSorted();

            Assert.True(preferenceTypes.Any());
            Assert.True(preferenceTypes.Count() == 2);
        }
    }
}
