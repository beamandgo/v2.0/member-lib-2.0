﻿using BeamAndGo.Member.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeamAndGo.Member.Test
{
    public class MemberAddressServiceTestFixture : IDisposable
    {
        public IMemberAddressService memberAddressService;
        public IMemberLogService memberLogService;
        public IMemberRepositoryManager memberRepositoryManager;
        public IMemberSearch memberSearch;
        public IMemberService memberService;
       
        public MemberAddressServiceTestFixture()
        {
            var options = new DbContextOptionsBuilder<MemberDbContext>().UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;
            var context = new MemberDbContext(options);

            var serviceOptions = Options.Create(new MemberServiceOptions());
            memberRepositoryManager = new MemberDbRepositoryManager(context);
            memberSearch = new MemberDbSearch(context);
            memberLogService = new MemberLogService(memberRepositoryManager);
            memberService = new MemberService(serviceOptions, memberRepositoryManager, memberSearch, memberLogService);

            memberAddressService = new MemberAddressService(memberRepositoryManager, memberService, memberLogService);
        }

        public MemberAddressServiceTestFixture WithTestData()
        {
            memberRepositoryManager.MemberRepository.Create(new Member()
            {
                Id = 1,
                Sid = "TESTMEMBER",
                FirstName = "Beam",
                LastName = "Go"
            });

            memberRepositoryManager.MemberAddressRepository.Create(new MemberAddress()
            {
                Id = 1,
                Sid = "TESTADDRESS1",
                MemberId = 1,
                Name = "test1",
                Street = "test1",
                PostalCode = "123456",
                CountryCode = "PH",
                IsDefault = true,
                IsVerified = true,
                RecordStatus = Core.Data.RecordStatus.Active
            });

            memberRepositoryManager.MemberAddressRepository.Create(new MemberAddress()
            {
                Id = 2,
                Sid = "TESTADDRESS2",
                MemberId = 1,
                Name = "test2",
                Street = "test2",
                PostalCode = "123456",
                CountryCode = "JP",
                IsDefault = false,
                IsVerified = true,
                RecordStatus = Core.Data.RecordStatus.Active
            });

            memberRepositoryManager.MemberAddressRepository.Create(new MemberAddress()
            {
                Id = 3,
                Sid = "TESTADDRESS3",
                MemberId = 1,
                Name = "test3",
                Street = "test3",
                PostalCode = "123456",
                CountryCode = "HK",
                IsDefault = false,
                IsVerified = false,
                RecordStatus = Core.Data.RecordStatus.Active
            });

            memberRepositoryManager.SaveChanges();

            return this;
        }

        public void Dispose()
        {
            memberAddressService = null;
            memberService = null;
            memberLogService = null;
            memberSearch = null;
            memberRepositoryManager.Dispose();
        }
    }
}
