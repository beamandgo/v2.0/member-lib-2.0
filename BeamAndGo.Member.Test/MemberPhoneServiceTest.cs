﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BeamAndGo.Member.Test
{
    public class MemberPhoneServiceTest
    {
        [Theory]
        [InlineData("TESTPHONE1")]
        public async Task GetBySid_WithValidPhoneSid_ShouldReturnMemberPhone(string phoneSid)
        {
            using var fixture = new MemberPhoneServiceTestFixture().WithTestData();

            var phone = await fixture.memberPhoneService.GetBySid(phoneSid);

            Assert.NotNull(phone);
            Assert.True(phone.Id > 0);
        }

        [Theory]
        [InlineData("INVALIDPHONESID")]
        [InlineData("")]
        [InlineData(null)]
        public async Task GetBySid_WithInvalidPhoneSid_ShouldThrowArgumentException(string phoneSid)
        {
            using var fixture = new MemberPhoneServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberPhoneService.GetBySid(phoneSid));
        }

        [Theory]
        [InlineData("TESTMEMBER", "65", "999090080")]
        public async Task Create_WithValidCountryCodeAndPhoneNumber_ShouldReturnMemberPhone(string memberSid, string callingCode, string phone)
        {
            using var fixture = new MemberPhoneServiceTestFixture().WithTestData();

            var memberPhone = await fixture.memberPhoneService.Create(memberSid, callingCode, phone);
            Assert.NotNull(memberPhone);
        }

        [Theory]
        [InlineData("TESTMEMBER", null, "12345678")]
        public async Task Create_WithInvalidCallingCode_ShouldThrowArgumentException(string memberSid, string callingCode, string number)
        {
            using var fixture = new MemberPhoneServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberPhoneService.Create(memberSid, callingCode, number));
        }

        [Theory]
        [InlineData("TESTMEMBER", "65", "x")]
        [InlineData("TESTMEMBER", "65", null)]
        public async Task Create_WithInvalidPhoneNumber_ShouldThrowArgumentException(string memberSid, string callingCode, string number)
        {
            using var fixture = new MemberPhoneServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberPhoneService.Create(memberSid, callingCode, number));
        }

        [Theory]
        [InlineData("INVALIDTESTMEMBER", "65", "12345678")]
        public async Task Create_WithInvalidMemberSid_ShouldThrowArgumentException(string memberSid, string callingCode, string number)
        {
            using var fixture = new MemberPhoneServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberPhoneService.Create(memberSid, callingCode, number));
        }

        [Theory]
        [InlineData("TESTMEMBER", "65", "11111111")]
        public async Task Create_WithExistingPhoneNumber_ShouldThrowInvalidOperationException(string memberSid, string callingCode, string number)
        {
            using var fixture = new MemberPhoneServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<InvalidOperationException>(async ()
                => await fixture.memberPhoneService.Create(memberSid, callingCode, number));
        }

        [Theory]
        [InlineData("TESTPHONE1", "65", "87654321")]
        public async Task Update_WithValidCountryCodeAndPhoneNumber_ShouldReturnMemberPhone(string sid, string callingCode, string phone)
        {
            using var fixture = new MemberPhoneServiceTestFixture().WithTestData();

            var memberPhone = await fixture.memberPhoneService.Update(sid, callingCode, phone);
            Assert.NotNull(phone);
        }

        [Theory]
        [InlineData(null, "65", "12345678")]
        [InlineData("INVALIDPHONESID", null, "12345678")]
        [InlineData("INVALIDPHONESID", "65", null)]
        [InlineData("INVALIDPHONESID", "65", "98dsj309")]
        public async Task Update_WithInvalidValues_ShouldThrowArgumentException(string sid, string callingCode, string number)
        {
            using var fixture = new MemberPhoneServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberPhoneService.Update(sid, callingCode, number));
        }

        [Theory]
        [InlineData("TESTPHONE1")]
        public async Task SendVerification_WithValidValues_ShouldReturnMemberPhone(string sid)
        {
            using var fixture = new MemberPhoneServiceTestFixture().WithTestData();

            var phone = await fixture.memberPhoneVerificationService.SendVerification(sid);
            Assert.NotNull(phone);
        }

        [Theory]
        [InlineData("INVALIDPHONESID")]
        [InlineData("")]
        [InlineData(null)]
        public async Task SendVerification_WithInvalidParams_ShouldThrowArgumentException(string phoneSid)
        {
            using var fixture = new MemberPhoneServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberPhoneVerificationService.SendVerification(phoneSid));
        }

        [Theory]
        [InlineData("TESTPHONE3", "65", "123456")]
        public async Task Verify_WithValidValues_ShouldReturnMemberPhone(string sid, string prefix, string otp)
        {
            using var fixture = new MemberPhoneServiceTestFixture().WithTestData();

            var phone = await fixture.memberPhoneVerificationService.Verify(sid, prefix, otp);
            
            Assert.True(phone);
        }

        [Theory]
        [InlineData("TESTPHONE1", "Verified with verification code / link")]
        public async Task Verify_PhoneAlreadyVerified_ShouldThrowInvalidOperationException(string sid, string meta)
        {
            using var fixture = new MemberPhoneServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<InvalidOperationException>(async ()
                => await fixture.memberPhoneService.Verify(sid, meta));
        }

        [Theory]
        [InlineData("TESTPHONE3")]
        public async Task Delete_WithValidValues_ShouldReturnNothing(string sid)
        {
            using var fixture = new MemberPhoneServiceTestFixture().WithTestData();

            await fixture.memberPhoneService.Delete(sid);

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberPhoneService.GetBySid(sid));
        }

        [Theory]
        [InlineData("TESTPHONE1")]
        public async Task Delete_DefaultPhone_ShouldThrowInvalidOperationException(string sid)
        {
            using var fixture = new MemberPhoneServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<InvalidOperationException>(async ()
                => await fixture.memberPhoneService.Delete(sid));
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task Delete_WithInvalidPhoneSid_ShouldThrowArgumentException(string sid)
        {
            using var fixture = new MemberPhoneServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberPhoneService.Delete(sid));
        }

        [Theory]
        [InlineData("TESTPHONE2")]
        public async Task SetDefault_WithValidValues_ShouldReturnMemberPhone(string sid)
        {
            using var fixture = new MemberPhoneServiceTestFixture().WithTestData();

            await fixture.memberPhoneService.SetDefault(sid);
            var phone = await fixture.memberPhoneService.GetBySid(sid);
            Assert.True(phone.IsDefault);           
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public async Task SetDefaultt_WithInvalidPhoneSid_ShouldThrowArgumentException(string sid)
        {
            using var fixture = new MemberPhoneServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.memberPhoneService.SetDefault(sid));
        }

        [Theory]
        [InlineData("TESTMEMBER")]
        public async Task ListByMemberSid_WithValidMemberSid_ShouldReturnListOfMemberPhones(string memberSid)
        {
            using var fixture = new MemberPhoneServiceTestFixture().WithTestData();

            var phones = fixture.memberPhoneService.ListByMemberSid(memberSid);

            Assert.NotNull(phones);
            Assert.True(await phones.AnyAsync());
        }

        [Theory]
        [InlineData("INVALIDMEMBERSID")]
        [InlineData("")]
        [InlineData(null)]
        public async Task ListByMemberSid_WithInvalidMemberSid_ShouldThrowArgumentException(string memberSid)
        {
            using var fixture = new MemberPhoneServiceTestFixture().WithTestData();

            var phones = fixture.memberPhoneService.ListByMemberSid(memberSid);

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await phones.GetAsyncEnumerator().MoveNextAsync());
        }
    }
}
