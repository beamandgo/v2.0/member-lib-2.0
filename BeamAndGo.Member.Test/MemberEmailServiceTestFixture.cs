﻿using BeamAndGo.Core.Data;
using BeamAndGo.Member.Data;
using BeamAndGo.Member.Invite;
using BeamAndGo.Member.Messaging;
using BeamAndGo.Member.Verification;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeamAndGo.Member.Test
{
    public class MemberEmailServiceTestFixture : IDisposable
    {
        public IMemberEmailService memberEmailService;
        public IMemberLogService memberLogService;
        public IMemberRepositoryManager memberRepositoryManager;
        public IMemberSearch memberSearch;
        public IMemberService memberService;
        public MemberEmailServiceTestFixture()
        {
            var options = new DbContextOptionsBuilder<MemberDbContext>().UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;
            var context = new MemberDbContext(options);

            var serviceOptions = Options.Create(new MemberServiceOptions());
            
            memberRepositoryManager = new MemberDbRepositoryManager(context);
            memberSearch = new MemberDbSearch(context);
            memberLogService = new MemberLogService(memberRepositoryManager);
            memberService = new MemberService(serviceOptions, memberRepositoryManager, memberSearch, memberLogService);

            memberEmailService = new MemberEmailService(memberRepositoryManager, memberService, memberLogService);
        }

        public MemberEmailServiceTestFixture WithTestData()
        {
            memberRepositoryManager.MemberRepository.Create(new Member()
            {
                Id = 1,
                Sid = "TESTMEMBER",
                FirstName = "Beam",
                LastName = "Go",
                CountryCode = "SG"
            });

            memberRepositoryManager.MemberEmailRepository.Create(new MemberEmail()
            {
                Id = 1,
                Sid = "TESTEMAIL1",
                MemberId = 1,
                Email = "test1@bng.com",
                IsVerified = true,
                IsDefault = true
            });

            memberRepositoryManager.MemberEmailRepository.Create(new MemberEmail()
            {
                Id = 2,
                Sid = "TESTEMAIL2",
                MemberId = 1,
                Email = "test2@bng.com",
                IsVerified = true,
                IsDefault = false
            });

            memberRepositoryManager.MemberEmailRepository.Create(new MemberEmail()
            {
                Id = 3,
                Sid = "TESTEMAIL3",
                MemberId = 1,
                Email = "test3@bng.com",
                IsVerified = false,
                IsDefault = false
            });

            memberRepositoryManager.SaveChanges();

            return this;
        }

        public void Dispose()
        {
            memberEmailService = null;
            memberService = null;
            memberLogService = null;
            memberSearch = null;
            memberRepositoryManager.Dispose();
        }
    }
}
