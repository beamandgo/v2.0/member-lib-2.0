﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BeamAndGo.Member.Test
{
    public class MemberInviteServiceTest
    {
        [Theory]
        [InlineData("TESTMEMBER", "bng@test.com", "https://beamandgo.com")]
        public async Task SendInviteEmail_WithValidValues(string fromMemberSid, string toEmail, string referralLink)
        {
            using var fixture = new MemberInviteServiceTestFixture().WithTestData();

            await fixture.emailInviteService.Send(fromMemberSid, toEmail, referralLink);
        }

        [Theory]
        [InlineData(null, "bng@test.com", "https://beamandgo.com")]
        [InlineData("TESTMEMBER", null, "https://beamandgo.com")]
        [InlineData("TESTMEMBER", "bng@test.com", null)]
        [InlineData("INVALIDMEMBERSID", "bng@test.com", "https://beamandgo.com")]
        public async Task SendInviteEmail_WithInvalidValues_ShouldReturnArgumentException(string fromMemberSid, string toEmail, string referralLink)
        {
            using var fixture = new MemberInviteServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.emailInviteService.Send(fromMemberSid, toEmail, referralLink));
        }

        [Theory]
        [InlineData("TESTMEMBER", "65", "123456", "https://beamandgo.com")]
        public async Task SendInviteSms_WithValidValues(string fromMemberSid, string toCallingCode, string toPhone, string referralLink)
        {
            using var fixture = new MemberInviteServiceTestFixture().WithTestData();

            await fixture.phoneInviteService.Send(fromMemberSid, toCallingCode, toPhone, referralLink);
        }

        [Theory]
        [InlineData(null, "65", "123456", "https://beamandgo.com")]
        [InlineData("TESTMEMBER", null, "123456", "https://beamandgo.com")]
        [InlineData("TESTMEMBER", "65", null, "https://beamandgo.com")]
        [InlineData("TESTMEMBER", "65", "123456", null)]
        [InlineData("INVALIDMEMBERSID", "65", "123456", "https://beamandgo.com")]
        public async Task SendInviteSms_WithInvalidValues(string fromMemberSid, string toCallingCode, string toPhone, string referralLink)
        {
            using var fixture = new MemberInviteServiceTestFixture().WithTestData();

            await Assert.ThrowsAsync<ArgumentException>(async ()
                => await fixture.phoneInviteService.Send(fromMemberSid, toCallingCode, toPhone, referralLink));
        }
    }
}
