﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Debug
{
    public class AddressDebug : Program
    {
        public static async Task Run()
        {
            // Creating new member
            var member = await MemberService.Create(new CreateMemberData
            {
                FirstName = "Beam",
                LastName = "Go",
                CountryCode = "SG"
            });

            Console.WriteLine("Adding member's address");
            var address1 = await AddressService.Create(member.Sid, "Home", "Middle Road", "123456", "SG");
            await PrintMember(member.Sid);
            PressAnyKey();

            Console.WriteLine("Updating member's default address");
            address1 = await AddressService.Update(address1.Sid, "Home", "Queensway", "654321", "SG");
            await PrintMember(member.Sid);
            PressAnyKey();

            Console.WriteLine("Deleting member's default address");
            try
            {
                await AddressService.Delete(address1.Sid);
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
            }
            await PrintMember(member.Sid);
            PressAnyKey();

            Console.WriteLine("Verifying member's default address");
            await AddressService.Verify(address1.Sid, "test");
            await PrintMember(member.Sid);
            PressAnyKey();

            Console.WriteLine("Adding member's second address");
            var address2 = await AddressService.Create(member.Sid, "Work", "Middle Road", "123456", "SG");
            await PrintMember(member.Sid);
            PressAnyKey();

            Console.WriteLine("Setting member's second email as default email");
            try
            {
                await AddressService.SetDefault(address2.Sid);
            }
            catch(InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
            }
            await PrintMember(member.Sid);
            PressAnyKey();

            Console.WriteLine("Verifying member's second email");
            await AddressService.Verify(address2.Sid, "test");
            await PrintMember(member.Sid);
            PressAnyKey();

            Console.WriteLine("Setting member's second email as default email");
            await AddressService.SetDefault(address2.Sid);
            await PrintMember(member.Sid);
            PressAnyKey();

            Console.WriteLine("Getting list of member's addresses");
            var addresses = AddressService.ListByMemberSid(member.Sid);
            await _PrintAddresses(addresses);
            PressAnyKey();
        }

        private static async Task _PrintAddresses(IAsyncEnumerable<MemberAddress> addresses)
        {
            await foreach (var address in addresses)
            {
                Console.WriteLine($"Address: ({address.Name}) {address.Street} {address.PostalCode} {address.CountryCode} {(address.IsVerified ? "(verified)" : "")} {(address.IsDefault ? "(default)" : "")}");
            }
        }
    }
}
