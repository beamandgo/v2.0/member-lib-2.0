﻿using BeamAndGo.Member.Signup;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Debug
{
    public class RelationshipDebug : Program
    {
        public static async Task Run()
        {
            // Creating relationship types
            Console.WriteLine("Create relationship type 'Friend'");
            var relationshipType1 = await RelationshipTypeService.Create("Friend");

            Console.WriteLine("Create relationship type 'Brother'");
            var relationshipType2 = await RelationshipTypeService.Create("Brother", 'M');

            Console.WriteLine("Create relationship type 'Sister'");
            var relationshipType3 = await RelationshipTypeService.Create("Sister");

            Console.WriteLine("Update relationship type 'Sister'");
            await RelationshipTypeService.Update(relationshipType3.Id, "Sister", 'F');

            Console.WriteLine("Create relationship type 'Test'");
            var relationshipType4 = await RelationshipTypeService.Create("Test");

            Console.WriteLine("Delete relationship type 'Test'");
            await RelationshipTypeService.Delete(relationshipType4.Id);
            PressAnyKey();

            Console.WriteLine("Getting list of relationship types");
            var relationshipTypes = await RelationshipTypeService.ListAll();
            foreach (var relationshipType in relationshipTypes)
            {
                await PrintRelationshipType(relationshipType.Id);
            }
            PressAnyKey();

            Console.WriteLine("Disable relationship type 'Sister'");
            await RelationshipTypeService.Disable(relationshipType3.Id);
            PressAnyKey();

            Console.WriteLine("Getting list of active relationship types");
            relationshipTypes = await RelationshipTypeService.ListAllActiveSortedByName();
            foreach (var relationshipType in relationshipTypes)
            {
                await PrintRelationshipType(relationshipType.Id);
            }
            PressAnyKey();

            Console.WriteLine("Enable relationship type 'Sister'");
            await RelationshipTypeService.Enable(relationshipType3.Id);
            PressAnyKey();

            Console.WriteLine("Signup member 1");
            var member1 = await SignupService.Signup(new SignupData
            {
                FirstName = "Beam",
                LastName = "Go",
                CallingCode = "65",
                PhoneNumber = "12345678",
                CountryCode = "SG"
            });
            await PrintMember(member1.Sid);
            PressAnyKey();

            Console.WriteLine("Member 1 adds new relationship 1");
            var relationship1 = await RelationshipService.Create(new CreateMemberRelationshipData
            {
                MemberSid = member1.Sid,
                RelationshipTypeId = relationshipType1.Id,

                FirstName = "Beam",
                LastName = "Go",
                CallingCode = "65",
                PhoneNumber = "12345678",
                CountryCode = "SG"
            });
            await PrintRelationship(relationship1.Sid);
            PressAnyKey();

            Console.WriteLine("Member 1 adds new relationship 2");
            var relationship2 = await RelationshipService.Create(new CreateMemberRelationshipData
            {
                MemberSid = member1.Sid,
                RelationshipTypeId = relationshipType1.Id,

                FirstName = "Nan",
                LastName = "Kyaw",
                CallingCode = "65",
                PhoneNumber = "12345678",
                CountryCode = "SG"
            });
            await PrintRelationship(relationship2.Sid);
            PressAnyKey();

            Console.WriteLine("Member 1 updates relationship 2");
            relationship2 = await RelationshipService.Update(relationship2.Sid, new UpdateMemberRelationshipData
            {
                RelationshipTypeId = relationshipType3.Id,

                FirstName = "Nan",
                LastName = "Kyaw",
                Gender = 'F',
                BirthDay = 1,
                BirthMonth = 1,
                BirthYear = 2000,
                CallingCode = "95",
                PhoneNumber = "87654321",
                Email = "bng@test.com",
                Street = "test",
                PostalCode = "1234B",
                CountryCode = "MM"
            });
            await PrintRelationship(relationship2.Sid);
            PressAnyKey();

            Console.WriteLine("Getting list of member 1's relationships");
            var relationships = RelationshipService.ListByMemberSid(member1.Sid);
            await PrintRelationships(relationships);
            PressAnyKey();

            Console.WriteLine("Delete relationship type 1");
            try
            {
                await RelationshipTypeService.Delete(relationshipType1.Id);
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
            }
            PressAnyKey();

            Console.WriteLine("Disable relationship type 1");
            await RelationshipTypeService.Disable(relationshipType1.Id);
            PressAnyKey();

            Console.WriteLine("Member 1 deletes relationship 1");
            await RelationshipService.Delete(relationship1.Sid);
            PressAnyKey();

            Console.WriteLine("Delete relationship type 1");
            await RelationshipTypeService.Delete(relationshipType1.Id);
            PressAnyKey();                       

            Console.WriteLine("Getting list of member 1's relationships");
            relationships = RelationshipService.ListByMemberSid(member1.Sid);
            await PrintRelationships(relationships);
            PressAnyKey();
        }

        public static async Task PrintRelationshipType(int id)
        {
            var relationshipType = await RelationshipTypeService.GetById(id);
            Console.WriteLine($"ID      : {relationshipType.Id}");                
            Console.WriteLine($"Name    : {relationshipType.Name}");
            Console.WriteLine($"Gender  : {relationshipType.Gender}");
        }

        public static async Task PrintRelationship(string sid)
        {
            var relationship = await RelationshipService.GetBySid(sid);
            Console.WriteLine($"ID                  : {relationship.Id}");
            Console.WriteLine($"SID                 : {relationship.Sid}");
            Console.WriteLine($"Relationship Type Id: {relationship.RelationshipTypeId}");
            Console.WriteLine($"Relationship Type   : {relationship.RelationshipType.Name} ({relationship.RelationshipType.Gender})");
            Console.WriteLine($"Name                : {relationship.FullName()}");
            Console.WriteLine($"Gender              : {relationship.Gender}");
            Console.WriteLine($"DOB                 : {relationship.BirthYear}/{relationship.BirthMonth}/{relationship.BirthDay}");
            Console.WriteLine($"Phone               : {relationship.CallingCode} {relationship.PhoneNumber}");
            Console.WriteLine($"Email               : {relationship.Email}");
            Console.WriteLine($"Address             : {relationship.Street} {relationship.PostalCode} {relationship.CountryCode}");
        }

        public static async Task PrintRelationships(IAsyncEnumerable<MemberRelationship> relationships)
        {
            await foreach(var relationship in relationships)
            {
                Console.WriteLine($"ID                  : {relationship.Id}");
                Console.WriteLine($"SID                 : {relationship.Sid}");
                Console.WriteLine($"Relationship Type Id: {relationship.RelationshipTypeId}");
                Console.WriteLine($"Relationship Type   : {relationship.RelationshipType.Name} ({relationship.RelationshipType.Gender})");
                Console.WriteLine($"Name                : {relationship.FullName()}");
                Console.WriteLine($"Gender              : {relationship.Gender}");
                Console.WriteLine($"DOB                 : {relationship.BirthYear}/{relationship.BirthMonth}/{relationship.BirthDay}");
                Console.WriteLine($"Phone               : {relationship.CallingCode} {relationship.PhoneNumber}");
                Console.WriteLine($"Email               : {relationship.Email}");
                Console.WriteLine($"Address             : {relationship.Street} {relationship.PostalCode} {relationship.CountryCode}");
            }
        }
    }
}
