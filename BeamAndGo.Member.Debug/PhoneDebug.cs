﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Debug
{
    public class PhoneDebug : Program
    {
        public static async Task Run()
        {
            // Creating new member
            var member = await MemberService.Create(new CreateMemberData
            {
                FirstName = "Beam",
                LastName = "Go",
                CountryCode = "SG"
            });

            Console.WriteLine("Adding member's phone number");
            var phone1 = await PhoneService.Create(member.Sid, "65", "12345678");
            await PrintMember(member.Sid);
            PressAnyKey();

            Console.WriteLine("Updating member's default phone");
            try
            {
                phone1 = await PhoneService.Update(phone1.Sid, "65", "11111111");
            }
            catch(InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
            }            
            await PrintMember(member.Sid);
            PressAnyKey();

            Console.WriteLine("Deleting member's default phone");
            try
            {
                await PhoneService.Delete(phone1.Sid);
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
            }
            await PrintMember(member.Sid);
            PressAnyKey();

            Console.WriteLine("Verifying member's default phone");
            await PhoneService.Verify(phone1.Sid, "Verified with OTP");
            await PrintMember(member.Sid);
            PressAnyKey();

            Console.WriteLine("Adding member's second phone number");
            var phone2 = await PhoneService.Create(member.Sid, "66", "87654321");
            await PrintMember(member.Sid);
            PressAnyKey();

            Console.WriteLine("Setting member's second phone as default phone");
            try
            {
                await PhoneService.SetDefault(phone2.Sid);
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
            }
            await PrintMember(member.Sid);
            PressAnyKey();

            Console.WriteLine("Verifying member's second phone");
            await PhoneService.Verify(phone2.Sid, "Verified with OTP");
            await PrintMember(member.Sid);
            PressAnyKey();

            Console.WriteLine("Setting member's second phone as default phone");
            await PhoneService.SetDefault(phone2.Sid);            
            await PrintMember(member.Sid);
            PressAnyKey();

            Console.WriteLine("Getting list of member's phones");
            var phones = PhoneService.ListByMemberSid(member.Sid);
            await _PrintPhones(phones);
            PressAnyKey();
        }

        private static async Task _PrintPhones(IAsyncEnumerable<MemberPhone> phones)
        {
            await foreach (var phone in phones)
            {
                Console.WriteLine($"Phone  : ({phone.Identifier}) +{phone.CallingCode} {phone.PhoneNumber} {(phone.IsVerified ? "(verified)" : "")} {(phone.IsDefault ? "(default)" : "")}");
            }
        }
    }
}
