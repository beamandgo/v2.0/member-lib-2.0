﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Debug
{
    public class EmailDebug : Program
    {
        public static async Task Run()
        {
            // Creating new member
            var member = await MemberService.Create(new CreateMemberData
            {
                FirstName = "Beam",
                LastName = "Go",
                CountryCode = "SG"
            });

            Console.WriteLine("Adding member's email address");
            var email1 = await EmailService.Create(member.Sid, "bng@test.com");
            await PrintMember(member.Sid);
            PressAnyKey();

            Console.WriteLine("Updating member's default email");
            try
            {
                email1 = await EmailService.Update(email1.Sid, "beamandgo@test.com");
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
            }
            await PrintMember(member.Sid);
            PressAnyKey();

            Console.WriteLine("Deleting member's default email");
            try
            {
                await EmailService.Delete(email1.Sid);
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
            }
            await PrintMember(member.Sid);
            PressAnyKey();

            Console.WriteLine("Verifying member's default email");
            await EmailService.Verify(email1.Sid, "Verified with verificaion code");
            await PrintMember(member.Sid);
            PressAnyKey();

            Console.WriteLine("Adding member's second email");
            var email2 = await EmailService.Create(member.Sid, "bng2@test.com");
            await PrintMember(member.Sid);
            PressAnyKey();

            Console.WriteLine("Setting member's second email as default email");
            try
            {
                await EmailService.SetDefault(email2.Sid);
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
            }
            await PrintMember(member.Sid);
            PressAnyKey();

            Console.WriteLine("Verifying member's second email");
            await EmailService.Verify(email2.Sid, "Verified with verificaion code");
            await PrintMember(member.Sid);
            PressAnyKey();

            Console.WriteLine("Setting member's second email as default email");
            await EmailService.SetDefault(email2.Sid);
            await PrintMember(member.Sid);
            PressAnyKey();

            Console.WriteLine("Getting list of member's emails");
            var emails = EmailService.ListByMemberSid(member.Sid);
            await _PrintEmails(emails);
            PressAnyKey();
        }

        private static async Task _PrintEmails(IAsyncEnumerable<MemberEmail> emails)
        {
            await foreach (var email in emails)
            {
                Console.WriteLine($"Email  : ({email.Identifier}) <{email.Email}> {(email.IsVerified ? "(verified)" : "")} {(email.IsDefault ? "(default)" : "")}");
            }
        }
    }
}
