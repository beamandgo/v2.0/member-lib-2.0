﻿using BeamAndGo.Member.Signup;
using System;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Debug
{
    public class VerificationDebug : Program
    {
        public static async Task Run()
        {
            // Creating new member
            var member = await MemberService.Create(new CreateMemberData
            {
                FirstName = "Beam",
                LastName = "Go",
                CountryCode = "SG"
            });

            // Add phone
            Console.WriteLine("Adding member's phone number");
            var phone1 = await PhoneService.Create(member.Sid, "65", "12345678");
            PressAnyKey();

            // Add email
            Console.WriteLine("Adding member's email address");
            var email1 = await EmailService.Create(member.Sid, "bng@test.com");
            PressAnyKey();

            Console.WriteLine("Verifying member's phone");
            await PhoneVerificationService.SendVerification(phone1.Sid);
            await PhoneVerificationService.Verify(phone1.Sid, "test", "123456");
            PressAnyKey();

            Console.WriteLine("Verifying member's default email");            
            await EmailVerificationService.Send(email1.Sid, "https://beamandgo.com");
            PressAnyKey();
            await EmailVerificationService.Verify(Console.ReadLine());            
            PressAnyKey();

            await PrintMember(member.Sid);
        }
    }
}
