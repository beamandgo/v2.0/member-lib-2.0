﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using BeamAndGo.Member.Data;
using BeamAndGo.Member.Signup;
using BeamAndGo.Member.Invite;
using BeamAndGo.Member.Messaging;
using BeamAndGo.Member.Verification;
using BeamAndGo.Member.Verification.Data;
using BeamAndGo.Member.Admin;
using BeamAndGo.Member.Admin.Data;
using System.Collections.Generic;
using StackExchange.Redis;

namespace BeamAndGo.Member.Debug
{
    public class Program
    {
        // Logging services
        public static IMemberLogService MemberLogService;
        public static IMemberAdminLogService AdminLogService;

        // Lookup type services
        public static IMemberRelationshipTypeService RelationshipTypeService;
        public static IMemberPreferenceTypeService PreferenceTypeService;

        // Main services
        public static IMemberService MemberService;
        public static IMemberPhoneService PhoneService;
        public static IMemberEmailService EmailService;
        public static IMemberAddressService AddressService;
        public static IMemberReferralService ReferralService;
        public static IMemberRelationshipService RelationshipService;
        public static IMemberPreferenceService PreferenceService;
        public static IMemberSignupService SignupService;
        public static IMemberEmailVerificationService EmailVerificationService;
        public static IMemberPhoneVerificationService PhoneVerificationService;
        public static IMemberEmailInviteService EmailInviteService;
        public static IMemberPhoneInviteService PhoneInviteService;
        public static IMemberAdminService AdminService;

        private static void Setup()
        {
            // Console logger
            var loggerFactory = LoggerFactory.Create(builder => builder.AddConsole());

            // In-memory database
            var options = new DbContextOptionsBuilder<MemberDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking).Options;

            // Local database
            //var options = new DbContextOptionsBuilder<MemberDbContext>()
            //    .UseMySql("server=servername;database=Member;user=user;password=password;compress=true;sslmode=none;treattinyasboolean=false;guidformat=none")
            //    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking).Options;

            // Repo setup
            var context = new MemberDbContext(options);
            var adminContext = new MemberAdminDbContext(options);
            var repositoryManager = new MemberDbRepositoryManager(context);
            var adminRepositoryManager = new MemberAdminDbRepositoryManager(adminContext);

            var redisConnectionString = Environment.GetEnvironmentVariable("REDIS");
            var emailVerificationTokenRepository = new MemberEmailVerificationTokenRedisRepository(ConnectionMultiplexer.Connect(redisConnectionString));

            // Options
            var memberServiceOptions = Options.Create(new MemberServiceOptions());
            var signupServiceOptions = Options.Create(new MemberSignupServiceOptions());
            var verificationServiceOptions = Options.Create(new MemberVerificationServiceOptions());

            // Formatters
            var emailVerificationFormatter = new DefaultEmailVerificationFormatter();
            var emailInviteFormatter = new BeamAndGoMemberEmailInviteFormatter();
            var phoneInviteFormatter = new BeamAndGoMemberPhoneInviteFormatter();

            // Messaging proviers
            var otpProvider = new DummyMemberOtpProvider(loggerFactory.CreateLogger<DummyMemberOtpProvider>());
            var mailMessagingProvider = new DummyMemberMailMessagingProvider(loggerFactory.CreateLogger<DummyMemberMailMessagingProvider>());
            var textMessagingProvider = new DummyMemberTextMessagingProvider(loggerFactory.CreateLogger<DummyMemberTextMessagingProvider>());

            // Standalone Repos
            var memberSearch = new MemberDbSearch(context);
            var adminSearch = new MemberAdminDbSearch(adminContext);

            // Logging services
            MemberLogService = new MemberLogService(repositoryManager);
            AdminLogService = new MemberAdminLogService(adminRepositoryManager);

            // Lookup type services
            RelationshipTypeService = new MemberRelationshipTypeService(repositoryManager);
            PreferenceTypeService = new MemberPreferenceTypeService(repositoryManager);

            // Main services
            MemberService = new MemberService(memberServiceOptions, repositoryManager, memberSearch, MemberLogService);
            PhoneService = new MemberPhoneService(repositoryManager, MemberService, MemberLogService);
            EmailService = new MemberEmailService(repositoryManager, MemberService, MemberLogService);
            AddressService = new MemberAddressService(repositoryManager, MemberService, MemberLogService);
            ReferralService = new MemberReferralService(repositoryManager, MemberService, MemberLogService);
            RelationshipService = new MemberRelationshipService(memberServiceOptions, repositoryManager, RelationshipTypeService, MemberService, MemberLogService);
            PreferenceService = new MemberPreferenceService(repositoryManager, PreferenceTypeService, MemberService, MemberLogService);
            SignupService = new MemberSignupService(signupServiceOptions, MemberService, PhoneService, EmailService, AddressService, ReferralService, PreferenceService, MemberLogService);
            EmailVerificationService = new MemberEmailVerificationService(verificationServiceOptions, emailVerificationTokenRepository, emailVerificationFormatter, mailMessagingProvider, EmailService, MemberService);
            PhoneVerificationService = new MemberPhoneVerificationService(otpProvider, PhoneService);
            EmailInviteService = new MemberEmailInviteService(emailInviteFormatter, mailMessagingProvider, EmailService, MemberService);
            PhoneInviteService = new MemberPhoneInviteService(phoneInviteFormatter, textMessagingProvider, PhoneService, MemberService);
            AdminService = new MemberAdminService(adminRepositoryManager, adminSearch, AdminLogService, MemberService);
        }

        public static async Task Main(string[] args)
        {
            Setup();

            if (args.Length < 1)
            {
                Console.WriteLine("Usage: dotnet run [module]");
                return;
            }

            switch (args[0].ToUpper())
            {
                case "MEMBER": await MemberDebug.Run(); return;
                case "PHONE": await PhoneDebug.Run(); return;
                case "EMAIL": await EmailDebug.Run(); return;
                case "ADDRESS": await AddressDebug.Run(); return;
                case "RELATIONSHIP": await RelationshipDebug.Run(); return;
                case "REFERRAL": await ReferralDebug.Run(); return;
                case "PREFERENCE": await PreferenceDebug.Run(); return;
                case "VERIFY": await VerificationDebug.Run(); return;
                case "INVITE": await InviteDebug.Run(); return;
                case "ADMIN": await AdminDebug.Run(); return;
                default: Console.WriteLine("Invalid module"); return;
            }
        }

        public static void PressAnyKey()
        {
            Console.WriteLine("Press any key...");
            Console.ReadKey(true);
        }

        public static async Task PrintMember(string sid)
        {
            var member = await MemberService.GetBySid(sid, true);
            Console.WriteLine($"ID     : {member.Id}");
            Console.WriteLine($"SID    : {member.Sid}");
            Console.WriteLine($"Name   : {member.FullName()}");
            Console.WriteLine($"Gender : {member.Gender}");
            Console.WriteLine($"DOB    : {member.BirthYear}/{member.BirthMonth}/{member.BirthDay}");
            Console.WriteLine($"Country: {member.CountryCode}");

            foreach (var phone in member.Phones)
            {
                Console.WriteLine($"Phone  : ({phone.Identifier}) +{phone.CallingCode} {phone.PhoneNumber} {(phone.IsVerified ? "(verified)" : "")} {(phone.IsDefault ? "(default)" : "")}");
            }

            foreach (var email in member.Emails)
            {
                Console.WriteLine($"Email  : ({email.Identifier}) <{email.Email}> {(email.IsVerified ? "(verified)" : "")} {(email.IsDefault ? "(default)" : "")}");
            }

            foreach (var address in member.Addresses)
            {
                Console.WriteLine($"Address: ({address.Name}) {address.Street} {address.PostalCode} {address.CountryCode} {(address.IsVerified ? "(verified)" : "")} {(address.IsDefault ? "(default)" : "")}");
            }
        }

        public static async Task PrintAdmin(string sid)
        {
            var admin = await AdminService.GetBySid(sid);
            Console.WriteLine($"Admin ID    : {admin.Id}");
            Console.WriteLine($"Admin SID   : {admin.Sid}");
            Console.WriteLine($"Member ID   : {admin.MemberId}");
            Console.WriteLine($"Access Level: {admin.AccessLevel}");
            Console.WriteLine($"Expiry Date : {admin.Expiry}");
        }

        public static async Task PrintMemberLogs(int id)
        {
            var logs = MemberLogService.ListByMemberId(id);
            await foreach (var l in logs) Console.WriteLine(l.Meta);
        }

        public static async Task PrintAdminLogs(int id)
        {
            var logs = AdminLogService.ListByAdminId(id);
            await foreach (var l in logs) Console.WriteLine(l.Meta);
        }        
    }
}
