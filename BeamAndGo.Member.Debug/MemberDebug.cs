﻿using System;
using System.Threading.Tasks;
using BeamAndGo.Member.Signup;

namespace BeamAndGo.Member.Debug
{
    public class MemberDebug : Program
    {
        public static async Task Run()
        {
            Console.WriteLine("Signup member 1");
            var member1 = await SignupService.Signup(new SignupData
            {
                FirstName = "Beam",
                LastName = "Go",
                CallingCode = "65",
                PhoneNumber = "12345678",
                CountryCode = "SG"
            });
            await PrintMember(member1.Sid);
            PressAnyKey();

            Console.WriteLine("Signup member 2");
            var member2 = await SignupService.Signup(new SignupData
            {
                FirstName = "Beam2",
                LastName = "Go",
                CallingCode = "65",
                PhoneNumber = "87654321",
                Email = "bng@test.com",
                Street = "test",
                PostalCode = "123456",
                CountryCode = "SG"
            });
            await PrintMember(member2.Sid);
            PressAnyKey();

            Console.WriteLine("Updating member 1");
            var update1 = new UpdateMemberData
            {
                FirstName = "Beam",
                LastName = "Go",
                MiddleName = "And",
                DisplayName = "BNG",
                Gender = 'M',
                BirthDay = 1,
                BirthMonth = 1,
                BirthYear = 2000,
                CountryCode = "SG"
            };
            member1 = await MemberService.Update(member1.Sid, update1);
            await PrintMember(member1.Sid);
            PressAnyKey();

            Console.WriteLine("Disabling member 1");
            await MemberService.Disable(member1.Sid);
            PressAnyKey();

            Console.WriteLine("Enabling member 1");
            await MemberService.Enable(member1.Sid);
            PressAnyKey();

            Console.WriteLine("Reviewing member 1 logs");
            await PrintMemberLogs(member1.Id);

            Console.WriteLine("Total number of members");
            var count = await MemberService.Count();
            Console.WriteLine(count);
            PressAnyKey();

            Console.WriteLine("Get by calling code and phone number");
            var member = await MemberService.GetByPhone("65", "12345678");
            await PrintMember(member.Sid);
            PressAnyKey();

            Console.WriteLine("Get by email");
            member = await MemberService.GetByEmail("bng@test.com");
            await PrintMember(member.Sid);
            PressAnyKey();

            Console.WriteLine("List of members");
            var memberList = await MemberService.ListAll();
            foreach (var m in memberList) await PrintMember(m.Sid);

            Console.WriteLine("Searching members");
            Console.Write("Search: ");
            var members = MemberService.Search(Console.ReadLine());
            await foreach (var m in members) await PrintMember(m.Sid);

            Console.WriteLine("Searching member by email or phone");
            Console.Write("Search: ");
            member = await MemberService.SearchExact(Console.ReadLine());
            await PrintMember(member.Sid);
            PressAnyKey();
        }
    }
}
