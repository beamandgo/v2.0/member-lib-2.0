﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Debug
{
    public class PreferenceDebug : Program
    {
        public static async Task Run()
        {
            // Creating new member
            var member = await MemberService.Create(new CreateMemberData
            {
                FirstName = "Beam",
                LastName = "Go",
                CountryCode = "SG"
            });

            var preferenceType1 = await PreferenceTypeService.Create("IsEmailAllowed", "Allow to recieve promotional emails", "false");
            var preferenceType2 = await PreferenceTypeService.Create("IsTextAllowed", "Allow to recieve promotional SMS", "false");
            var preferenceType3 = await PreferenceTypeService.Create("IsPostMailAllowed", "Allow to recieve promotional mails", "false");

            Console.WriteLine("Delete preference type 3");
            await PreferenceTypeService.Delete(preferenceType3.Id);
            PressAnyKey();

            Console.WriteLine("Getting list of member's default preferences");
            var preferences = PreferenceService.ListWithDefaultByMemberSid(member.Sid);
            Console.WriteLine(await preferences.CountAsync());
            PressAnyKey();

            Console.WriteLine("Member creates preferences");
            var preference1 = await PreferenceService.CreateOrUpdate(member.Sid, preferenceType1.Id, "true");
            var preference2 = await PreferenceService.CreateOrUpdate(member.Sid, preferenceType2.Id, "true");
            PressAnyKey();

            Console.WriteLine("Getting member's preference 1");
            preference1 = await PreferenceService.GetBySid(preference1.Sid);
            await _PrintMemberPreference(preference1.Sid);
            PressAnyKey();

            Console.WriteLine("Getting list of member's preferences");
            preferences = PreferenceService.ListByMemberSid(member.Sid);
            Console.WriteLine(await preferences.CountAsync());
            await foreach (var preference in preferences)
            {
                await _PrintMemberPreference(preference.Sid);
            }
            PressAnyKey();            

            Console.WriteLine("Delete preference type 1");
            try
            {
                await PreferenceTypeService.Delete(preferenceType1.Id);
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
            }
            PressAnyKey();

            Console.WriteLine("Deletes member's preference 1");
            await PreferenceService.Delete(preference1.Sid);
            PressAnyKey();

            Console.WriteLine("Disable preference type 1");
            await PreferenceTypeService.Disable(preferenceType1.Id);
            PressAnyKey();

            Console.WriteLine("Delete preference type 1");
            await PreferenceTypeService.Delete(preferenceType1.Id);
            PressAnyKey();

            Console.WriteLine("Getting list of member's preferences");
            preferences = PreferenceService.ListByMemberSid(member.Sid);
            await foreach (var preference in preferences)
            {
                await _PrintMemberPreference(preference.Sid);
            }
            PressAnyKey();
        }
        private static async Task _PrintMemberPreference(string sid)
        {
            var preference = await PreferenceService.GetBySid(sid);

            Console.WriteLine($"ID                  : {preference.Id}");
            Console.WriteLine($"SID                 : {preference.Sid}");
            Console.WriteLine($"Member ID           : {preference.MemberId}");
            Console.WriteLine($"PreferenceType ID   : {preference.PreferenceTypeId}");
            Console.WriteLine($"PreferenceType Name : {preference.PreferenceType.Name} ({preference.PreferenceType.Description})");
            Console.WriteLine($"Value               : {preference.Value}");
        }
    }
}
