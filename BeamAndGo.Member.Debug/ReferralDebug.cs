﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Debug
{
    public class ReferralDebug : Program
    {
        public static async Task Run()
        {
            // Creating referrer member
            var member1 = await MemberService.Create(new CreateMemberData
            {
                FirstName = "Beam",
                LastName = "Go",
                CountryCode = "SG"
            });

            // Creating referred member 1
            var member2 = await MemberService.Create(new CreateMemberData
            {
                FirstName = "Beam2",
                LastName = "Go",
                CountryCode = "SG"
            });

            // Creating referred member 2
            var member3 = await MemberService.Create(new CreateMemberData
            {
                FirstName = "Beam3",
                LastName = "Go",
                CountryCode = "SG"
            });

            Console.WriteLine("Member 2 adds Member 1 as Referral");
            var referral = await ReferralService.Register(member2.Sid, member1.ReferralCode, MemberReferralType.Referral);
            PressAnyKey();

            Console.WriteLine("Member 2 adds Member 3 as Referral");
            try
            {
                await ReferralService.Register(member2.Sid, member3.ReferralCode, MemberReferralType.Referral);
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
            }
            PressAnyKey();

            Console.WriteLine("Member 2 adds Member 3 as Deligate");
            await ReferralService.Register(member2.Sid, member3.ReferralCode, MemberReferralType.Delegate);
            PressAnyKey();

            Console.WriteLine("Member 3 adds Member 1 as Referrer");
            await ReferralService.Register(member3.Sid, member1.ReferralCode, MemberReferralType.Referral);
            PressAnyKey();

            Console.WriteLine("List Referrals of Member 1");
            var referrals = ReferralService.ListByReferrerMemberSid(member1.Sid);
            await _PrintReferrals(referrals);
            PressAnyKey();

            Console.WriteLine("List Referrals of Member 3");
            referrals = ReferralService.ListByReferrerMemberSidAndReferralType(member3.Sid, MemberReferralType.Delegate);
            await _PrintReferrals(referrals);
            PressAnyKey();

            Console.WriteLine("List Referrals of Member 2");
            referrals = ReferralService.ListByReferredMemberSid(member2.Sid);
            await _PrintReferrals(referrals);
            PressAnyKey();

            Console.WriteLine("List Referrals of Member 2");
            referrals = ReferralService.ListByReferredMemberSidAndReferralType(member2.Sid, MemberReferralType.Referral);
            await _PrintReferrals(referrals);
            PressAnyKey();
        }

        private static async Task _PrintReferrals(IAsyncEnumerable<MemberReferral> referrals)
        {
            await foreach (var referral in referrals)
            {
                Console.WriteLine($"ID              : {referral.Id}");
                Console.WriteLine($"SID             : {referral.Sid}");
                Console.WriteLine($"Referral Type   : {referral.ReferralType}");
                Console.WriteLine($"Referrer ID     : {referral.ReferrerMemberId}");
                Console.WriteLine($"Referrer Name   : {referral.Referrer.FirstName} {referral.Referrer.LastName}");
                Console.WriteLine($"Referee ID      : {referral.ReferredMemberId}");
                Console.WriteLine($"Referee Name    : {referral.Referred.FirstName} {referral.Referred.LastName}");
                Console.WriteLine();
            }
        }
    }
}
