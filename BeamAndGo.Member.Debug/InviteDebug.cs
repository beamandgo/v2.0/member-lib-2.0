﻿using System;
using System.Threading.Tasks;

namespace BeamAndGo.Member.Debug
{
    public class InviteDebug : Program
    {
        public static async Task Run()
        {
            // Creating new member
            var member = await MemberService.Create(new CreateMemberData
            {
                FirstName = "Beam",
                LastName = "Go",
                CountryCode = "SG"
            });

            Console.WriteLine("Member sends SMS invitation");
            await PhoneInviteService.Send(member.Sid, "65", "12345678", "https://beamandgo.com");
            PressAnyKey();

            Console.WriteLine("Member sends email invitation");
            await EmailInviteService.Send(member.Sid, "bng@test.com", "https://beamandgo.com");
        }
    }
}
