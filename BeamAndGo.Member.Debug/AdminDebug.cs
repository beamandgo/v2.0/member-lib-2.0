﻿using System;
using System.Threading.Tasks;
using BeamAndGo.Member.Admin;

namespace BeamAndGo.Member.Debug
{
    public class AdminDebug : Program
    {
        public static async Task Run()
        {
            Console.WriteLine("Creating new admin");

            var member = await MemberService.Create(new CreateMemberData
            {
                FirstName = "Beam",
                LastName = "Go",
                CountryCode = "SG"
            });
            await PrintMember(member.Sid);
            PressAnyKey();

            var admin = await AdminService.Create(member.Sid, MemberAdminAccessLevel.FullAccess, null);
            await PrintAdmin(admin.Sid);
            PressAnyKey();

            Console.WriteLine("Updating admin's access level");
            admin = await AdminService.Update(admin.Sid, MemberAdminAccessLevel.SuperAdmin, null);
            await PrintAdmin(admin.Sid);
            PressAnyKey();

            Console.WriteLine("Get by Member SID");
            admin = await AdminService.GetByMemberSid(member.Sid);
            await PrintAdmin(admin.Sid);
            PressAnyKey();

            Console.WriteLine("Getting list of admins");
            var admins = await AdminService.ListAll();
            foreach (var a in admins) await PrintAdmin(a.Sid);
            PressAnyKey();

            Console.WriteLine("Search admins");
            Console.Write("Search: ");
            var adminsSearch = AdminService.Search(Console.ReadLine());
            await foreach (var a in adminsSearch) await PrintAdmin(a.Sid);
            PressAnyKey();

            Console.WriteLine("Disabling admin");
            await AdminService.Disable(admin.Sid);
            PressAnyKey();

            Console.WriteLine("Enabling admin");
            await AdminService.Enable(admin.Sid);
            PressAnyKey();

            Console.WriteLine("Deleting admin");
            await AdminService.Delete(admin.Sid);
            PressAnyKey();

            Console.WriteLine("Reviewing admin logs");
            await PrintAdminLogs(admin.Id);
            PressAnyKey();

            Console.WriteLine("Total number of admins");
            Console.WriteLine(await AdminService.Count());
            PressAnyKey();
        }
    }
}
